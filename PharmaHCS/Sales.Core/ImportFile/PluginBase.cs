﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Core.ImportFile
{
    public abstract class PluginBase
    {
        public List<PluginRecord> FileRecords { get; protected set; }
        public PluginBase()
        {
            FileRecords = new List<PluginRecord>();
        }
        public abstract void ProcessData(PluginInput[] files);
        public abstract ValidationStatus ValidateFiles();
    }
}
