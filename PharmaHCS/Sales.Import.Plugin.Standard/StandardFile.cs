﻿using Sales.Core.ImportFile;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using OfficeOpenXml;

namespace Sales.Import.Plugin.Standard
{
    /// <summary>
    /// Contains 1 file with xlsx extension and 
    /// </summary>
    public class StandardFile : StandardExcelImportFile
    {
        public StandardFile() : base()
        {
            HeaderColumns.AddRange(new string[]{
                "BranchCode",
                "CustomerCode",
                "CustomerName",
                "CustomerAddress",
                "ProductCode",
                "ProductName",
                "Qty",
                "Price",
                "SalesDate",
                "Bonus",
                "InvoiceNumber"
            });
        }

        protected override void FillRecord(List<string> columns,int rowIndex, ExcelRange inputRow)
        {
            DateTime? sd = null;
            if (inputRow[rowIndex, columns.IndexOf("SalesDate") + 1].Value != null)
                sd = DateTime.TryParse(inputRow[rowIndex, columns.IndexOf("SalesDate") + 1].Value.ToString(), out DateTime ss) ? ss : (DateTime?)null;

            decimal? qty = null;
            if (inputRow[rowIndex, columns.IndexOf("Qty") + 1].Value != null)
                qty = Decimal.TryParse(inputRow[rowIndex, columns.IndexOf("Qty") + 1].Value.ToString(), out decimal q) ? q : (decimal?)null;

            FileRecords.Add(new PluginRecord
            {
                DistributorBranchCode = inputRow[rowIndex, columns.IndexOf("BranchCode") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("BranchCode") + 1].Value.ToString(),
                CustomerCode = inputRow[rowIndex, columns.IndexOf("CustomerCode") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("CustomerCode") + 1].Value.ToString(),
                CustomerName = inputRow[rowIndex, columns.IndexOf("CustomerName") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("CustomerName") + 1].Value.ToString(),
                CustomerAddress = inputRow[rowIndex, columns.IndexOf("CustomerAddress") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("CustomerAddress") + 1].Value.ToString(),
                DosageFormCode = inputRow[rowIndex, columns.IndexOf("ProductCode") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("ProductCode") + 1].Value.ToString(),
                DosageFormName = inputRow[rowIndex, columns.IndexOf("ProductName") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("ProductName") + 1].Value.ToString(),
                FileQuantity = inputRow[rowIndex, columns.IndexOf("Qty") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("Qty") + 1].Value.ToString(),
                FileAmount = inputRow[rowIndex, columns.IndexOf("Price") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("Price") + 1].Value.ToString(),
                SalesDate = sd,
                InvoiceNumber = inputRow[rowIndex, columns.IndexOf("InvoiceNumber") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("InvoiceNumber") + 1].Value.ToString(),
                Quantity = qty,
                InvalidQty = !qty.HasValue
            });
        }
    }

    public abstract class StandardExcelImportFile : PluginBase
    {
        public List<string> HeaderColumns { get; set; }
        public int HeaderStartRow { get; set; }
        public StandardExcelImportFile() : base()
        {
            HeaderColumns = new List<string>();
            HeaderStartRow = 1;
            Status = ValidationStatus.Succeeded;
        }
        private ValidationStatus Status { get; set; }

        public override void ProcessData(PluginInput[] files)
        {
            if (HeaderColumns.Count < 1) return;
            ///for every file check header
            ///and append the data into FileRecords
            foreach (var file in files)
                ProcessFile(file);
        }

        private void ProcessFile(PluginInput file)
        {
            using (ExcelPackage package = new ExcelPackage(file.FileStream)) {
                foreach (var sheet in package.Workbook.Worksheets) {
                    var columns = from firstRowCell in sheet.Cells[HeaderStartRow, 1, HeaderStartRow, sheet.Dimension.End.Column]
                                  select (HeaderStartRow > 0 ? firstRowCell.Value.ToString() : $"Column {firstRowCell.Start.Column}");
                    /*
                     * validate columns
                     */
                    var foundColumns = from hc in HeaderColumns
                                       join fc in columns on hc equals fc
                                       select hc;
                    if (foundColumns.Count() != HeaderColumns.Count) { Status = ValidationStatus.InvalidFormat ; return; }
                    /*
                     * if everything ok, put data into FileRecords
                    */
                    for (int rowIndex = HeaderStartRow+1; rowIndex <= sheet.Dimension.End.Row; rowIndex++) {
                        var inputRow = sheet.Cells[rowIndex, 1, rowIndex, sheet.Dimension.End.Column];

                        FillRecord(columns.ToList(), rowIndex,inputRow);
                    }
                }
            }
        }

        protected abstract void FillRecord(List<string> columns,int rowIndex, ExcelRange inputRow);

        public override ValidationStatus ValidateFiles()
        {
            return Status;
        }
    }
}
