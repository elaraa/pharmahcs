﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.App
{
    public static class DropDownList
    {
        public static IEnumerable<SelectListItem> ToDropDownList<TModel>(this IQueryable<TModel> list, Func<TModel, SelectListItem> selector) {

            return list.Select(selector).ToList();
        }
        public static IEnumerable<SelectListItem> ToDropDownList<TModel>(this List<TModel> list, Func<TModel, SelectListItem> selector)
        {
            return list.Select(selector);
        }
    }
}