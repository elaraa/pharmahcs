﻿using Kendo.Mvc.UI.Fluent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Web.App
{
    public static class TreeListExtension
    {
        /// <summary>
        /// Adds the defualt property options for any TreeList {Filterable, sortable, pageable, selectable, resizable, reorderable}
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="builder">kendo TreeList</param>
        /// <returns>Kendo TreeList after adding the properties</returns>
        public static TreeListBuilder<T> AddDefaultOptions<T>(this TreeListBuilder<T> builder) where T : class
        {
            builder
                    
                    .Filterable()
                    .Sortable()
                    .Resizable(true)
                    .Selectable(selectable => selectable.Mode(Kendo.Mvc.UI.TreeListSelectionMode.Single).Type(Kendo.Mvc.UI.TreeListSelectionType.Row))
                    //.Events(events => events.Change("GridActions.dgChange"))
                    .Reorderable(true)
                    .Pageable(p => p.PageSize(20).PageSizes(true))
                    .Toolbar(t=>t.Excel())
                    .Excel(e=>e.FileName("exported.xlsx").Filterable(true))
                   ;
            return builder;
        }
        public static TreeListColumnFactory<T> AddColumn<T, Value>(this TreeListColumnFactory<T> builder, Expression<Func<T, Value>> expression) where T : class
        {
            if (expression.ReturnType == typeof(System.DateTime) || expression.ReturnType == typeof(System.DateTime?))
                builder.Add().Field(expression).Format("{0:dd MMM yy}");
            //if (expression.Type == typeof(bool) || expression.Type == typeof(bool?))
            //    builder.Add().Field(expression).Template(BooleanColumnTemplate(builder.Column.Member));
            else
                builder.Add().Field(expression);
            return builder;
        }
        public static TreeListColumnFactory<T> AddAuditColumns<T>(this TreeListColumnFactory<T> builder) where T : class
        {
            builder.Add().Field("LastUpdate").Format("{0:dd MMM yy hh:mm}");
            builder.Add().Field("UpdatedByLogonName").Title("Updated By");
            return builder;
        }
       
       public static TreeListColumnFactory<T> AddActionColumns<T>(this TreeListColumnFactory<T> builder, string columnId
           ,string controller, string treeName = "Grid"
           , string editUrl = null, string deleteUrl = null) where T : class
        {
        
            var requestContext = HttpContext.Current.Request.RequestContext;
            var urlHelper = new System.Web.Mvc.UrlHelper(requestContext);

            var defaultEditUrl = urlHelper.Action("Edit", controller);
            var editNdx = defaultEditUrl.IndexOf("Edit");
            var lastSlashNdx = defaultEditUrl.LastIndexOf('/');
            if (lastSlashNdx > editNdx)
                defaultEditUrl = defaultEditUrl.Remove(lastSlashNdx);


            editUrl = string.IsNullOrEmpty(editUrl) || string.IsNullOrWhiteSpace(editUrl) ? string.Format("{0}/", defaultEditUrl) : string.Format("{0}/", editUrl);
            deleteUrl = string.IsNullOrEmpty(deleteUrl) || string.IsNullOrWhiteSpace(deleteUrl) ? urlHelper.Action("Delete", controller) : deleteUrl;

            builder.Add().Field("")
                .Title("")
                .Filterable(false)
                .Template(string.Format(
                        "<a style='text-decoration:none' class=\"glyphicon glyphicon-pencil pencil \"  href=\"{1}#={0}#\"></a> &nbsp;<a style='text-decoration:none' class=\"glyphicon glyphicon-trash trashIcon\"   onclick=\"DeleteRecord('{3}','kendoTreeList','#={0}#','{2}')\"></a> ",
                        columnId, editUrl, deleteUrl,
                        treeName
                        )
                    );
            return builder;
        }
        

        public static TreeListAjaxDataSourceBuilder<T> AddDefaultSettings<T>(this TreeListAjaxDataSourceBuilder<T> builder, Action<DataSourceTreeListModelDescriptorFactory<T>> model, Action<CrudOperationBuilder> read = null) where T : class
        {
            string controller = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString();

            builder
                .Batch(true)
                .ServerOperation(false)
                .Model(model)
                .Read(read ?? (r => r.Action("ReadList", controller)));
            return builder;
        }
    }
}