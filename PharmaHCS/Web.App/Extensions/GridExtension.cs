﻿using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Web.App
{
    public static class GridExtension
    {
        /// <summary>
        /// Adds the defualt property options for any grid {Filterable, sortable, pageable, selectable, resizable, reorderable}
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="builder">kendo Grid</param>
        /// <returns>Kendo grid after adding the properties</returns>
        public static GridBuilder<T> AddDefaultOptions<T>(this GridBuilder<T> builder, bool hideToolbar = false) where T : class
        {
            builder
                    .Pageable(p => p.Refresh(true).PageSizes(true).ButtonCount(5))
                    .Filterable()
                   //.Pageable(KendoUISettings.GridPagingSettings)
                   //.Filterable(KendoUISettings.GridFilterationSettings)
                   .Sortable()
                    .AllowCopy(true)
                   .Selectable(selectable => selectable.Mode(GridSelectionMode.Single).Type(GridSelectionType.Row))
                   //.Events(events => events.Change("GridActions.dgChange"))
                   .Resizable(resize => resize.Columns(true))
                   .Reorderable(reorder => reorder.Columns(true))
                   .Excel(e=>e.AllPages(true).FileName("export.xlsx").Filterable(true))
                   //.Pdf(p=>p.AllPages().Author("PharmaHCS").AvoidLinks().Creator("PharmaHCS").FileName("export.pdf"))
                   //.ToolBar(t=>t.Excel())
                   //.Groupable()
                   //.Mobile(MobileMode.Auto)
                   ;
            if (!hideToolbar) builder.ToolBar(t => t.Excel());
            builder.Columns(cols => cols.ColumnsContainer.Columns.ToList().ForEach(c =>
            {
                //c.HeaderHtmlAttributes["class"] = "grid-header";
                c.HeaderHtmlAttributes["title"] = c.Title;
                //c.Media = ("(min-width: 450px)");
            }));

            return builder;
        }
        /// <summary>
        /// Adds the pixelWidth property to column and by default adds 450 in media
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="builder"></param>
        /// <param name="pixelWidth"></param>
        /// <returns></returns>
        public static GridBoundColumnBuilder<T> AddDefaultOptions<T>(this GridBoundColumnBuilder<T> builder, int pixelWidth) where T : class
        {
            return AddDefaultOptions(builder, "450", pixelWidth);
        }
        public static GridBoundColumnBuilder<T> AddDefaultOptionsAndViewInMobile<T>(this GridBoundColumnBuilder<T> builder) where T : class
        {
            return AddDefaultOptions(builder, 0);
        }
        /// <summary>
        /// Adds properties to column {Filterable, media, width}
        /// adjusts column view if boolean type or datetime type
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="builder">Kendo Grid column property</param>
        /// <param name="mediaMinWidth">media min width</param>
        /// <param name="pixelWidth">width</param>
        /// <returns>Same Kendo Grid column property after adding the properties</returns>
        public static GridBoundColumnBuilder<T> AddDefaultOptions<T>(this GridBoundColumnBuilder<T> builder, string mediaMinWidth = "950", int pixelWidth = 0) where T : class
        {
            if (builder.Column.MemberType == typeof(System.DateTime) || builder.Column.MemberType == typeof(System.DateTime?))
                builder.Format("{0:dd MMM yy}");
            if (builder.Column.MemberType == typeof(bool) || builder.Column.MemberType == typeof(bool?))
                builder.ClientTemplate(BooleanColumnTemplate(builder.Column.Member));

            builder.Filterable(ftb => ftb.Multi(true).Search(true));
            if (!string.IsNullOrEmpty(mediaMinWidth) && !string.IsNullOrWhiteSpace(mediaMinWidth))
                builder.Media(string.Format("(min-width: {0}px)", mediaMinWidth));
            if (pixelWidth > 0)
                builder.Width(pixelWidth);
            return builder;
        }
        /// <summary>
        /// Add the Mobile view template column to kendo grid
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="builder">Kendo grid column</param>
        /// <param name="callback">javascript callback function should be in the form of "#=resColTemplate(data)#" </param>
        /// <returns>Kendo grid column after adding the column</returns>
        public static GridTemplateColumnBuilder<T> AddDefaultMobileTemplate<T>(this GridTemplateColumnBuilder<T> builder, string callback = "#=resColTemplate(data)#") where T : class
        {
            return builder
                .ClientTemplate(callback)
                .Title("Items")
                .Media("(max-width: 450px)");
        }
        /// <summary>
        /// Add the default properties of kendo datasource {pagesize, batch, ajax, model, read function}
        /// </summary>
        /// <typeparam name="T">Model type</typeparam>
        /// <param name="builder">kendo datasource</param>
        /// <param name="model"></param>
        /// <param name="read">read action</param>
        /// <returns>kendo datasource after adding the properties</returns>
        public static DataSourceBuilder<T> AddDefaultSettings<T>(this DataSourceBuilder<T> builder, Action<DataSourceModelDescriptorFactory<T>> model, Action<CrudOperationBuilder> read = null,bool serverOperation = true, Action<CrudOperationBuilder> create = null, Action<CrudOperationBuilder> update = null) where T : class
        {
            string controller = System.Web.HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString();

            builder
                .Ajax()
                .Batch(true)
                .ServerOperation(serverOperation)
                .PageSize(10)
                .Model(model)
                .Read(read ?? (r => r.Action("ReadList", controller)))
                .Create(create?? (r=>r.Action("CreateListRow",controller)))
                .Update(update ?? (r => r.Action("UpdateListRow", controller)));
                
            return builder;
        }
        /// <summary>
        /// Add 2 columns for audit (LastUpdate, UpdatedByLogonName).
        /// if those columns are not in the model it will produce error in run time.
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="builder">Columns property of kendo grid</param>
        /// <returns>the same columns property of kendo grid after adding the 2 columns</returns>
        public static GridColumnFactory<T> AddAuditColumns<T>(this GridColumnFactory<T> builder) where T : class
        {
            builder.Bound("LastUpdate").AddDefaultOptions("1050").Format("{0:dd MMM yy hh:mm tt}"); ;
            builder.Bound("UpdatedByLogonName").AddDefaultOptions("1050").Title("Updated By");
            return builder;
        }
        public static GridColumnFactory<T> AddActionColumns<T>(this GridColumnFactory<T> builder, string columnId, params ListActionColumn[] columns) where T : class
        {
            if (columns == null || columns.Length < 1) return builder;

            var sb = new List<string>();
            foreach (var column in columns)
                sb.Add(column.ToString().Replace("$GRIDID$", builder.Container.Name));

            builder.Bound(columnId)
                    .ClientTemplate(string.Join("&nbsp;", sb.ToArray())).Filterable(false)
                    .Title("");
            return builder;
        }
        public static GridColumnFactory<T> AddActionColumns<T>(this GridColumnFactory<T> builder, string columnId, string editUrl = null, string deleteUrl = null) where T : class
        {

            var requestContext = System.Web.HttpContext.Current.Request.RequestContext;
            var controller = builder.Container.ViewContext.RouteData.Values["controller"].ToString();
            var urlHelper = new System.Web.Mvc.UrlHelper(requestContext);

            var defaultEditUrl = urlHelper.Action("Edit", controller);
            var editNdx = defaultEditUrl.IndexOf("Edit");
            var lastSlashNdx = defaultEditUrl.LastIndexOf('/');
            if (lastSlashNdx > editNdx)
                defaultEditUrl = defaultEditUrl.Remove(lastSlashNdx);


            editUrl = string.IsNullOrEmpty(editUrl) || string.IsNullOrWhiteSpace(editUrl) ? string.Format("{0}/", defaultEditUrl) : string.Format("{0}/", editUrl);
            deleteUrl = string.IsNullOrEmpty(deleteUrl) || string.IsNullOrWhiteSpace(deleteUrl) ? urlHelper.Action("Delete", controller) : deleteUrl;

            
            return AddActionColumns(builder,columnId,
                ListActionColumn.EditColumn(columnId,editUrl), 
                ListActionColumn.DeleteColumn(columnId, deleteUrl));
        }
        //public static GridColumnFactory<T> AddActionColumns<T>(this GridColumnFactory<T> builder, string columnId, string editUrl = null, string deleteUrl = null) where T : class
        //{
           
        //    var requestContext = System.Web.HttpContext.Current.Request.RequestContext;
        //    var controller =  builder.Container.ViewContext.RouteData.Values["controller"].ToString();
        //    var urlHelper = new System.Web.Mvc.UrlHelper(requestContext);

        //    var defaultEditUrl = urlHelper.Action("Edit", controller);
        //    var editNdx = defaultEditUrl.IndexOf("Edit");
        //    var lastSlashNdx = defaultEditUrl.LastIndexOf('/');
        //    if (lastSlashNdx>editNdx)
        //        defaultEditUrl = defaultEditUrl.Remove(lastSlashNdx);


        //    editUrl = string.IsNullOrEmpty(editUrl) || string.IsNullOrWhiteSpace(editUrl) ? string.Format("{0}/", defaultEditUrl) : string.Format("{0}/",editUrl);
        //    deleteUrl = string.IsNullOrEmpty(deleteUrl) || string.IsNullOrWhiteSpace(deleteUrl) ? urlHelper.Action("Delete", controller) : deleteUrl;
            
        //    builder.Bound(columnId)
        //            .ClientTemplate(string.Format(
        //                "<a style='text-decoration:none' class=\"glyphicon glyphicon-pencil pencil \"  href=\"{1}#={0}#\"></a> &nbsp;<a style='text-decoration:none' class=\"glyphicon glyphicon-trash trashIcon\"   onclick=\"DeleteGridRecord('{3}','#={0}#','{2}')\"></a> ",
        //                columnId, editUrl, deleteUrl,
        //                builder.Container.Name
        //                )
        //            ).Filterable(false)
        //            .Title("");
        //    return builder;
        //}

        private static string BooleanColumnTemplate(string memberName)
        {
            var urlHelper = new System.Web.Mvc.UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);

            return string.Format("$$ if( {0} )|[ $$ " +
                  "<img src='{1}' height=20 ></img>" +
                  "$$]| else |[$$" +
                  "<i class='fa fa-times-circle text-muted small-thin'></i>" +
                  "$$]|$$", memberName,
                  urlHelper.Content("~/Images/chk_green.png"))
                  .Replace("$$", "#")
                  .Replace("]|", "}")
                  .Replace("|[", "{");
        }
    }
    public class ListActionColumn
    {
        private const string _template = "<a id =\"{3}\" style='text-decoration:none' class=\"{1}\"  href=\"{0}\" {2}></a>";
        private const string _onClickTemplate = "<a id =\"{2}\" style='text-decoration:none' class=\"{0} \" {1}></a>";
        public string Url { get; set; }
        public string Icon { get; set; }
        public string OnClick { get; set; }
        public string Id { get; set; }
        public override string ToString()
        {
            if (OnClick != null)
                return string.Format(_onClickTemplate, Icon, string.IsNullOrEmpty(OnClick) ? "" : "onclick=\"" + OnClick + "\"", Id);
            else
                return string.Format(_template, Url, Icon, string.IsNullOrEmpty(OnClick) ? "" : "onclick=\"" + OnClick + "\"", Id);
        }
        public static ListActionColumn EditColumn(string columnId, string editUrl = null)
        {
            var requestContext = System.Web.HttpContext.Current.Request.RequestContext;
            var urlHelper = new System.Web.Mvc.UrlHelper(requestContext);

            var defaultEditUrl = urlHelper.Action("Edit");
            var editNdx = defaultEditUrl.IndexOf("Edit");
            var lastSlashNdx = defaultEditUrl.LastIndexOf('/');
            if (lastSlashNdx > editNdx)
                defaultEditUrl = defaultEditUrl.Remove(lastSlashNdx);


            editUrl = string.IsNullOrEmpty(editUrl) || string.IsNullOrWhiteSpace(editUrl) ? string.Format("{0}/", defaultEditUrl) : string.Format("{0}/", editUrl);

            return new ListActionColumn
            {
                Url = string.Format("{0}#={1}#", editUrl, columnId),
                Icon = "glyphicon glyphicon-pencil pencil"
            };
        }
        public static ListActionColumn DeleteColumn(string columnId, string deleteUrl)
        {
            return new ListActionColumn
            {
                Icon = "glyphicon glyphicon-trash trashIcon",
                OnClick = string.Format("DeleteGridRecord('$GRIDID$','#={0}#','{1}')", columnId, deleteUrl)

            };
        }

        public static ListActionColumn GenericSwALColumn(string columnId, string Url, string Icon, bool swal = false, string buttonType = "generic", string sucessMessageHeader = "", string sucessMessage = ""
           , string failMessageHeader = "", string failMessage = "", string title = "", string text = "", string type = "", string confirmButtonText = "",
             string cancelButtonText = "", string safe = "")
        {

            return new ListActionColumn
            {
                Icon = Icon,
                OnClick = swal ? string.Format("RecordAction('$GRIDID$','#={0}#','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}')", columnId, Url, swal,
                  sucessMessageHeader, sucessMessage, failMessageHeader, failMessage, title, text, type, confirmButtonText, cancelButtonText, safe) :
                   null,
                Url = swal ? null : string.Format("{0}#={1}#", string.Format("{0}/", Url), columnId),
                Id = string.Format("{0}#={1}#", buttonType + "_", columnId)
            };
        }
    }
}