﻿function onSelect(e) {
    for (var i = 0; i < e.files.length; i++) {
        var file = e.files[i].rawFile;
        if (file) {
            var reader = new FileReader();
            reader.onloadend = function () {
                $('#fileImg')[0].src = (this.result);
            };
            reader.readAsDataURL(file);
        }
    }
}