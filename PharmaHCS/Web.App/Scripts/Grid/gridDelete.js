﻿
function reloadControl(targetGrid, gridType) {

    var grid = $(targetGrid).data(gridType);
    grid.dataSource.read();
    grid.refresh();
}

function reloadGrid(targetGrid) {

    return reloadControl(targetGrid, "kendoGrid");
}

function DeleteRecord(gridId, gridType, recordId, deleteUrl) {
    if (confirm('Are you sure you want to delete the selected record?')) {
        $.ajax({
            url: deleteUrl,
            data: { id: recordId },
            async: false,
            type: "POST",
            success: function (data) {
                if ($.trim(data)) {
                    swal("Done", "Record deleted Successfully!", "success");
                    reloadControl("#" + gridId, gridType);
                }
                else {
                    swal("Error", "Record is not deleted !!!", "error");
                }
            },
            error: function (errorThrown) {
                swal("Error", "There is an error while deleting!", "error");
            }
        })
    }
}

function DeleteGridRecord(gridId, recordId, deleteUrl) {
    return DeleteRecord(gridId,"kendoGrid", recordId, deleteUrl);
}

