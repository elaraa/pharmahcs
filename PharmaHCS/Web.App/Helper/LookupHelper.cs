﻿using DB.ORM.DB;
using Kendo.Mvc.UI;
using Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.App.Controllers;
using static Services.Helper.EnumHelpers;

namespace Web.App
{
    public class LookupHelper
    {
        private int UserId { get; set; }
        public LookupHelper(BaseController controller)
        {
            UserId = controller.UserId;
        }
        public IQueryable<TModel> GetMyData<TModel>() where TModel : class, new()
        {
            return new BaseService<TModel>(UserId).Get(null, true);
        }
        public IEnumerable<SelectListItem> GetMyDropDownListData<TModel>(Func<TModel, SelectListItem> selector) where TModel : class, new()
        {
            return GetMyData<TModel>().ToDropDownList(selector);
        }
        public static IEnumerable<SelectListItem> GetMyCountries(BaseController controller) {
            return new BaseService<DB.ORM.DB.Country>(controller.UserId).Get(null, true)
                .ToDropDownList(s=>new SelectListItem { Text = s.CountryName, Value = s.CountryId.ToString()});
        }
        public static IEnumerable<SelectListItem> GetMyPotentials(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.Potential>(controller.UserId).Get(null, true)
                .ToDropDownList(s => new SelectListItem { Text = s.PotentialName, Value = s.PotentialId.ToString() });
        }
        public static IEnumerable<SelectListItem> GetMyTerritories(BaseController controller, string[] selectedItems = null)
        {
            //var tr = new BaseService<DB.ORM.DB.Territory>(controller.UserId).Get(null, true).ToList();
            //var values =  TerritoryLoadChilddren(null, ref tr, selectedItems);
            //return values.Items;
            return new BaseService<DB.ORM.DB.Territory>(controller.UserId).Get(w=>w.Depth==4, true)
                .ToDropDownList(s=> new SelectListItem { Text=s.TerritoryCode+"|"+s.TerritoryName, Value=s.TerritoryId.ToString() });
        }
        //private static OEntity LoadChildren<IEntity, OEntity>(IEntity entity, ref List<IEntity> entities
        //    , string[] selectedMenuItems = null
        //    ) {
        //}
                
        private static DropDownTreeItemModel TerritoryLoadChilddren(DB.ORM.DB.Territory territory, ref List<DB.ORM.DB.Territory> territories, string[] selectedMenuItems = null)
        {
            var terrList = territory == null ? territories.Where(w => w.ParentTerritoryId == null) : territories.Where(w => w.ParentTerritoryId == territory.TerritoryId);
            var TerrItem = new DropDownTreeItemModel
            {
                HasChildren = terrList.Count() > 0,
                Text = territory == null ? string.Empty : territory.TerritoryName,
                Id = territory == null ? string.Empty : territory.TerritoryId.ToString(),
                Expanded = false
            };

            TerrItem.Checked = selectedMenuItems == null ? false : selectedMenuItems.Contains(TerrItem.Id);

            if (TerrItem.HasChildren)
                foreach (var item in terrList)
                    TerrItem.Items.Add(TerritoryLoadChilddren(item, ref territories, selectedMenuItems));

            return TerrItem;
        }


        internal static IEnumerable<SelectListItem> GetMyDepartments(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.Department>(controller.UserId).Get(null, true)
               .ToDropDownList(s => new SelectListItem { Text = s.DepartmentName, Value = s.DepartmentId.ToString() });
        }
        public static IEnumerable<SelectListItem> GetMyAccountCategories(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.AccountCategory>(controller.UserId).Get(null, true)
                .ToDropDownList(s => new SelectListItem { Text = s.CategoryName, Value = s.CategoryId.ToString() });
        }
        public static IEnumerable<SelectListItem> GetMyAccountTypes(BaseController controller)
        {
            System.Linq.Expressions.Expression<Func<DB.ORM.DB.AccountType, bool>> where = null;

            if (controller is SalesImportController)
                where = w => w.IncludeInSales == true;
            return new BaseService<DB.ORM.DB.AccountType>(controller.UserId).Get(where, true)
                .ToDropDownList(s => new SelectListItem { Text = s.AccountTypeName, Value = s.AccountTypeId.ToString() });
        }
        public static IEnumerable<SelectListItem> GetMyJobTitles(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.JobTitle>(controller.UserId).Get(null, true)
                .ToDropDownList(s => new SelectListItem { Text = s.JobTitleName, Value = s.JobTitleId.ToString() });
        }

        public static IEnumerable<SelectListItem> GetMyProducts(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.Product>(controller.UserId).Get(null, true)
                .ToDropDownList(s => new SelectListItem { Text = s.ProductName, Value = s.ProductId.ToString() });
        }

        public static IEnumerable<SelectListItem> GetMyDosageForms(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.ProductDosageForm>(controller.UserId).Get(null, true)
                .ToDropDownList(s => new SelectListItem { Text = s.DosageFormName, Value = s.DosageFormId.ToString() });
        }


        public static IEnumerable<SelectListItem> GetMyDistributors(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.Distributor>(controller.UserId).Get(null, true)
                .ToDropDownList(s => new SelectListItem { Text = s.DistributorName, Value = s.DistributorId.ToString() });
        }


        public static IEnumerable<SelectListItem> GetMyAccounts(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.Account>(controller.UserId).Get(null, true)
                .ToDropDownList(s => new SelectListItem { Text = s.AccountName, Value = s.AccountId.ToString() });
        }
        public static IEnumerable<SelectListItem> GetMyUnits(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.DosageFormUnit>(controller.UserId).Get(null, true)
                .ToDropDownList(s => new SelectListItem { Text = s.UnitName, Value = s.UnitId.ToString() });
        }

        public static IEnumerable<SelectListItem> GetMyTeams(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.vw_Team>(controller.UserId).Get(null, true)
                .ToDropDownList(s => new SelectListItem { Text = s.TeamName, Value = s.TeamId.ToString() });
        }

        public static IEnumerable<SelectListItem> GetMyProfiles(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.TeamProfile>(controller.UserId).Get(null, true)
                .ToDropDownList(s => new SelectListItem { Text = s.ProfileName, Value = s.ProfileId.ToString() });
        }

        public static IEnumerable<SelectListItem> GetMyProfiles(BaseController controller,int id)
        {
            using(DBModel db=new DBModel())
            {
                var profilesIDs = db.fn_Security_GetProfilesForEmployee(id, DateTime.Now).Select(a => a.ProfileId);
                return new BaseService<DB.ORM.DB.TeamProfile>(controller.UserId).Get(a => profilesIDs.Contains(a.ProfileId), true)
                .ToDropDownList(s => new SelectListItem { Text = s.ProfileName, Value = s.ProfileId.ToString() });
            }

        }
        public static IEnumerable<SelectListItem> GetMyMedicalRep(BaseController controller)
        { 
            using (DBModel db = new DBModel())
            {
                var profemployeeIDs = db.fn_GetmyMedicalRep(controller.UserId, DateTime.Now.Date);
                //return new BaseService<DB.ORM.DB.Employee>(controller.UserId).Get(a => profemployeeIDs.Contains(a.EmployeeId), true)
                //.ToDropDownList(s => new SelectListItem { Text = s.EmployeeName, Value = s.EmployeeId.ToString() });
                return profemployeeIDs.ToDropDownList(s => new SelectListItem { Text = s.EmployeeName, Value = s.employeeid.ToString() });
            }

        }

        public static IEnumerable<SelectListItem> GetMyEmployees(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.Employee>(controller.UserId).Get(null, true)
                .ToDropDownList(s => new SelectListItem { Text = s.EmployeeName, Value = s.EmployeeId.ToString() });
        }
        public static IEnumerable<SelectListItem> GetTimeDefinitions(BaseController controller)
        {
            var defaultPredicate = (System.Linq.Expressions.Expression<Func<DB.ORM.DB.TimeDefinition, bool>>)(w=> w.Year <= DateTime.Now.Year);
            var targetPredicate = (System.Linq.Expressions.Expression<Func<DB.ORM.DB.TimeDefinition, bool>>)(w => w.Year >= DateTime.Now.Year-1 && w.Year <= DateTime.Now.Year + 1);
            var nextMonth = DateTime.Now.AddMonths(1);
            var repTargetPredicate = (System.Linq.Expressions.Expression<Func<DB.ORM.DB.TimeDefinition, bool>>)(w => System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(nextMonth) >= System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(w.FromDate) && System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(nextMonth) <= System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(w.ToDate));

            System.Linq.Expressions.Expression<Func<DB.ORM.DB.TimeDefinition, bool>> appliedPredicate = null;
            if (controller as RepTargetVisitController != null)
                appliedPredicate = repTargetPredicate;
            else if ((controller as TargetProfileController) == null)
                appliedPredicate = defaultPredicate;
            else appliedPredicate = targetPredicate;


            return new BaseService<DB.ORM.DB.TimeDefinition>(controller.UserId).Get(appliedPredicate, true)
                .ToDropDownList(s => new SelectListItem { Text = s.PeriodName, Value = s.TimeDefinitionId.ToString() });
        }
        

        public static IEnumerable<SelectListItem> GetMySalesPlugins(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.ImportPlugin>(controller.UserId).Get(null, true)
                .ToDropDownList(s => new SelectListItem { Text = s.PluginName, Value = s.PluginId.ToString() });
        }
        public static IEnumerable<SelectListItem> GetMyRoles(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.SystemRole>(controller.UserId).Get(null, true)
                .ToDropDownList(s => new SelectListItem { Text = s.RoleName, Value = s.SystemRoleId.ToString() });
        }
public static IEnumerable<SelectListItem> GetTerritoryLevels(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.Territory>(controller.UserId).Get(null, true)
                .Select(s => s.Depth).Distinct().OrderBy(s=>s)
                .ToDropDownList(s => new SelectListItem { Text = s.ToString(), Value = s.ToString() });
        }
        public static IEnumerable<TreeViewItemModel> GetAllMenus(BaseController controller, string[] selectedMenuItems = null) {
            var menus = new BaseService<DB.ORM.DB.SystemMenu>(controller.UserId).Get(null, true).ToList();
            if (menus == null) return null;
            var items = MenuLoadChilddren(null,ref menus, selectedMenuItems);
            return items.Items;
        }
        private static TreeViewItemModel MenuLoadChilddren(DB.ORM.DB.SystemMenu menu, ref List<DB.ORM.DB.SystemMenu> menus, string[] selectedMenuItems = null)
        {
            var menuList = menu == null ? menus.Where(w => w.ParentMenuId == null) : menus.Where(w => w.ParentMenuId == menu.MenuId);
            var menuItem = new TreeViewItemModel
            {
                HasChildren = menuList.Count() > 0,
                Text = menu==null?string.Empty: menu.MenuName,
                Id = menu == null ? string.Empty : menu.MenuId.ToString(),
                Expanded = false
            };
            
            menuItem.Checked = selectedMenuItems==null?false: selectedMenuItems.Contains(menuItem.Id);

            if (menuItem.HasChildren)
                foreach (var item in menuList.OrderBy(o => o.SortOrder))
                    menuItem.Items.Add(MenuLoadChilddren(item, ref menus, selectedMenuItems));

            return menuItem;
        }
        public static IEnumerable<SelectListItem> GetGenders(BaseController controller) {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = "M", Text = "Male" },
                new SelectListItem { Value = "F", Text = "Female" }
            };
        }
        public static IEnumerable<SelectListItem> GetMaritalStatuses(BaseController controller)
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = "S", Text = "Single" },
                new SelectListItem { Value = "M", Text = "Married" }
            };
        }

        public static IEnumerable<SelectListItem> GetProductPriceCritria()
        {
            return (Enum.GetValues(typeof(ProductPriceCritria)).Cast<int>().Select(e => new SelectListItem() 
                                   { Text = ((ProductPriceCritria)e).DisplayName(), Value = e.ToString() })).ToList();
        }
    }
}