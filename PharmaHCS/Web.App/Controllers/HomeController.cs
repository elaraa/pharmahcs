﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class HomeController : BaseController
    {
        //public override ActionResult Index()
        //{
        //    ///https://dottutorials.net/dynamic-user-defined-dashboards-asp-net-core-tutorial/
        //    return View();
        //}

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}