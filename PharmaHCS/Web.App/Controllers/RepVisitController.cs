﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.CRM;
using System;
using System.Linq;
using System.Web.Mvc;
using Web.App.Models;

namespace Web.App.Controllers
{
    public class RepVisitController : BaseControllerList<vw_VisitsPerDate, vw_VisitsPerDate, DateTime>
    {
        public RepVisitController() : base()
        {
            ViewNamePlural = PH.RepVisit.ViewNamePlural;
            ViewNameSingular = PH.RepVisit.ViewNameSingular;
            ApplySecurity = false;
        }
        protected override void InitTemplateServices()
        {
            ListService = new RepVisit(UserId);
            ActionService = new RepVisit(UserId);
        }
        public ActionResult Edit(DateTime id)
        {
            FillViewBag(false);
            ViewBag.VisitsList = (ActionService as RepVisit).ReadVisitsList(id).ToList();
            ViewBag.GKey = new GMapsSettings(UserId).GoogleMapsKey;
            return View((ActionService as RepVisit).GetByDate(id));
        }
        protected override void FillViewBag(bool isCreate)
        {
            if (isCreate)
                ViewBag.Accounts = (ActionService as RepVisit).GetAccounts()
                    .ToDropDownList(s => new SelectListItem { Text = string.Format("{0}|{1} - {2}", s.AccountCode, s.AccountName, s.Address), Value = s.AccountId.ToString() });
            base.FillViewBag(isCreate);
        }
        public override ActionResult Create()
        {
            FillViewBag(true);

            return View(new VMVisit
            {
                VisitDate = DateTime.Now
            });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VMVisit model) {
            FillViewBag(true);
            return ModelState.IsValid && (ActionService as RepVisit).Add(model.Model(Request.Browser)) ? RedirectToAction("Index") : (ActionResult)View(model);
        }
        public JsonResult ReadDoctors(int accountId) => Json((ActionService as RepVisit)
                .GetAccountDoctors(accountId)
                .ToDropDownList(s => new SelectListItem { Text = s.DoctorName, Value = s.DoctorId.ToString() })
                , JsonRequestBehavior.AllowGet);
        public JsonResult ReadCoaches() => Json((ActionService as RepVisit).GetCoaches().ToDropDownList(s => new SelectListItem { Text = s.EmployeeName, Value = s.EmployeeId.ToString() }), JsonRequestBehavior.AllowGet);
        public ActionResult ReadPresentations() => Json((ActionService as RepVisit).GetPresentations(), JsonRequestBehavior.AllowGet);
        public ActionResult ReadVisitsList([DataSourceRequest] DataSourceRequest request, DateTime date) => Json((ActionService as RepVisit).ReadVisitsList(date).ToDataSourceResult(request));
        public ActionResult DeleteVisit(Guid id) => Json((ActionService as RepVisit).DeleteVisit(id));
    }
}