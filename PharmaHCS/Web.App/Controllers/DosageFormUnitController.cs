﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class DosageFormUnitController : BaseControllerTemplate<vw_DosageFormUnit, DosageFormUnit, int>
    {

        public DosageFormUnitController() : base()
        {
            ViewNamePlural = PH.DosageFormUnit.ViewNamePlural;
            ViewNameSingular = PH.DosageFormUnit.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<DosageFormUnit, object>>[]
                {
                    x => x.UnitCode,
                    x => x.UnitName,                
                    x => x.CountryId

                };
        }

        protected override Expression<Func<DosageFormUnit, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.UnitId == id.Value) : (Expression<Func<DosageFormUnit, bool>>)(x => 1 == 0);
        }

        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            //
    
            ViewBag.Countries = LookupHelper.GetMyCountries(this);
        }



    }
}