﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class CeilingDosageController : BaseControllerTemplate<vw_CeilingDosageAccount, CeilingDosageAccount, int>
    {
         
        public CeilingDosageController() : base()
        {
            ViewNamePlural =  PH.CeilingDosage.ViewNamePlural;
            ViewNameSingular = PH.CeilingDosage.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<CeilingDosageAccount, object>>[]
                {
                    x=>x.AccountId,
                    x=>x.DosageFormId,
                    x => x.CeilingQty,
                    x => x.FromTimeId,
                    x => x.ToTimeId

                };
        }
        protected override void InitTemplateServices()
        {
            ActionService = new CeilingDosageService(UserId);
            ListService = new BaseService<vw_CeilingDosageAccount>(UserId);
        }
        protected override Expression<Func<CeilingDosageAccount, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.CeilingDosageAccountId == id.Value) : (Expression<Func<CeilingDosageAccount, bool>>)(x => 1 == 0);
        }

        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            ViewBag.TimeDef = LookupHelper.GetTimeDefinitions(this);
            ViewBag.DosageForms = LookupHelper.GetMyDosageForms(this);
            ViewBag.Accounts = LookupHelper.GetMyAccounts(this);
            
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Create(CeilingDosageAccount model)
        {
            FillViewBag(true);
            var result = (ActionService as CeilingDosageService).IsOverlapped(model);
            return AddOrUpdateModel(model, null, "Index", null, "Create", result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Edit(CeilingDosageAccount model)
        {
            FillViewBag(false);
            var result = (ActionService as CeilingDosageService).IsOverlapped(model);
            return AddOrUpdateModel(model, PropertiesToUpdate, "Index", null, "Edit", result);
        }



      


    }
}