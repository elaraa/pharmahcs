﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class EmployeeController : BaseControllerTemplate<vw_Employee, Employee, int>
    {
       // EmployeeService Service = new EmployeeService(UserId);
        public EmployeeController() : base()
        {
            ViewNamePlural = PH.Employee.ViewNamePlural;
            ViewNameSingular = PH.Employee.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<Employee, object>>[]
                {
                    x=>x.EmployeeCode,
                    x => x.EmployeeName,
                    x => x.NationalIDNumber,
                    x => x.NationalIDNumber,
                    x => x.OrganizationLevel,
                    x => x.BirthDate,
                    x => x.MaritalStatus,
                    x => x.Gender,
                    x => x.HireDate,
                    x => x.LoginId,
                    x => x.Password,
                    x => x.Picture,
                    x => x.JobTitleId,
                    x => x.DepartmentId,
                    x => x.Email,
                    x => x.Mobile,
                    x => x.Active,
                    x => x.CountryId

            };
        }
        protected override void InitTemplateServices()
        {
            ActionService = new EmployeeService(UserId);
            ListService = new BaseService<vw_Employee>(UserId);
        }
        protected override Expression<Func<Employee, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.EmployeeId == id.Value) : (Expression<Func<Employee, bool>>)(x => 1 == 0);
        }

        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            //
            ViewBag.JobTitles = LookupHelper.GetMyJobTitles(this);
            ViewBag.Departments = LookupHelper.GetMyDepartments(this);
            ViewBag.Countries = LookupHelper.GetMyCountries(this);
            ViewBag.Genders = LookupHelper.GetGenders(this);
            ViewBag.MaritalStatuses = LookupHelper.GetMaritalStatuses(this);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Edit(Employee model)
        {
            FillViewBag(false);
            if (ModelState.IsValid)
            {
                if ((ActionService as EmployeeService).edit(model))
                    return View("Index");
            }
            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Create(Employee model) 
        {
            FillViewBag(false);
            if (ModelState.IsValid)
            {
                if ((ActionService as EmployeeService).create(model))
                    return View("Index");
            }
            return View(model);
        }


        #region Employee Profile

        //List
        public ActionResult ReadEmployeeProfileList([DataSourceRequest] DataSourceRequest request, int EmployeeId)
        {
            return Json(new BaseService<vw_EmployeeProfile>(UserId).Get(w => w.EmployeeId == EmployeeId, true).ToDataSourceResult(request));
        }
        private void FillViewBag_EmployeeProfile(bool isCreate)
        {
            ViewBag.Title = string.Format("{1} {0}", "Employee Profile", isCreate ? "Create" : "Edit");
            ViewBag.EntityName = "Employee Profile";

            ViewBag.Employees = LookupHelper.GetMyEmployees(this);
            ViewBag.Profiles = LookupHelper.GetMyProfiles(this);


        }
        //create
        public ActionResult CreateEmployeeProfile(int employeeId)
        {
            FillViewBag_EmployeeProfile(true);
            ViewBag.Profiles = LookupHelper.GetMyProfiles(this, employeeId);
            var model = new EmployeeProfile { EmployeeId = employeeId , FromDate = DateTime.Now};
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEmployeeProfile(EmployeeProfile employeeProfile)
        {
            FillViewBag_EmployeeProfile(true);
            return CreateChildEntity<EmployeeProfile>(employeeProfile, "Edit", new { id = employeeProfile.EmployeeId }
            , "CreateEmployeeProfile");
        }
        //edit
        public ActionResult EditEmployeeProfile(int id)
        {
            FillViewBag_EmployeeProfile(false);
            return EditChildEntity<int, EmployeeProfile>(id, x => x.EmployeeProfileId == id, "EditEmployeeProfile");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEmployeeProfile(EmployeeProfile employeeProfile)
        {
            FillViewBag_EmployeeProfile(false);
            return EditChildEntity<EmployeeProfile>(employeeProfile, new Expression<Func<EmployeeProfile, object>>[]
                {
                    x => x.EmployeeId,
                     x => x.ProfileId,
                      x => x.FromDate,
                       x => x.ToDate,
                }
            , "Edit", new { id = employeeProfile.EmployeeId }, "EditEmployeeProfile");
        }
        //delete
        [HttpPost]
        public virtual JsonResult DeleteEmployeeProfile(int? id)
        {
            return DeleteChildEntity<int, EmployeeProfile>(id, x => x.EmployeeProfileId == id);
        }

        #endregion



        #region Employee Account

        //List
        public ActionResult ReadEmployeeAccountList([DataSourceRequest] DataSourceRequest request, int EmployeeId)
        {
            return Json(new BaseService<vw_EmployeeAccount>(UserId).Get(w => w.EmployeeId == EmployeeId, true).ToDataSourceResult(request));
        }

        #endregion



        #region Employee Territory

        //List
        public ActionResult ReadEmployeeTerritoryList([DataSourceRequest] DataSourceRequest request, int EmployeeId)
        {
            return Json(new BaseService<vw_EmployeeTerritory>(UserId).Get(w => w.EmployeeId == EmployeeId, true).ToDataSourceResult(request));
        }

        #endregion



        #region Employee History

        //List
        public ActionResult ReadEmployeeHistoryList([DataSourceRequest] DataSourceRequest request, int EmployeeId)
        {
            return Json(new BaseService<vw_EmployeeHistory>(UserId).Get(w => w.EmployeeId == EmployeeId, true).ToDataSourceResult(request));
        }
        private void FillViewBag_EmployeeHistory(bool isCreate)
        {
            ViewBag.Title = string.Format("{1} {0}", "Employee Profile", isCreate ? "Create" : "Edit");
            ViewBag.EntityName = "Employee Profile";

            ViewBag.Employees = LookupHelper.GetMyEmployees(this);
            ViewBag.JobTitles = LookupHelper.GetMyJobTitles(this);
            ViewBag.Departments = LookupHelper.GetMyDepartments(this);
            ViewBag.Teams = LookupHelper.GetMyTeams(this);
            ViewBag.Countries = LookupHelper.GetMyCountries(this);

        }
        //create
        public ActionResult CreateEmployeeHistory(int employeeId)
        {
            FillViewBag_EmployeeHistory(true);
            var model = new EmployeeHistory { EmployeeId = employeeId , FromDate = DateTime.Now};
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEmployeeHistory(EmployeeHistory employeeHistory)
        {
            FillViewBag_EmployeeHistory(true);
            //return CreateChildEntity<EmployeeHistory>(employeeHistory, "Edit", new { id = employeeHistory.EmployeeId }
            //, "CreateEmployeeHistory");
            var result = (ActionService as EmployeeService).IsOverlapped(employeeHistory);
            return AddOrUpdateModel(employeeHistory, null, "Edit", new { id = employeeHistory.EmployeeId }, "CreateEmployeeHistory", result);
        }
        //edit
        public ActionResult EditEmployeeHistory(int id)
        {
            FillViewBag_EmployeeHistory(false);
            return EditChildEntity<int, EmployeeHistory>(id, x => x.EmployeeHistoryId == id, "EditEmployeeHistory");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEmployeeHistory(EmployeeHistory employeeHistory)
        {
            FillViewBag_EmployeeHistory(false);
            var active = new BaseService<Employee>(UserId).GetOne(a => a.EmployeeId == employeeHistory.EmployeeId).Active;
            if (active)
            {
                //    return EditChildEntity<EmployeeHistory>(employeeHistory, new Expression<Func<EmployeeHistory, object>>[]
                //    {
                //            x => x.EmployeeId,
                //            x => x.JobTitleId,
                //            x => x.DepartmentId,
                //            x => x.ManagerId,
                //            x => x.OrganizationLevel,
                //            x => x.FromDate,
                //            x => x.ToDate,
                //            x => x.CountryId,

                //    }
                //, "Edit", new { id = employeeHistory.EmployeeId }, "EditEmployeeHistory");
                var result = (ActionService as EmployeeService).IsOverlapped(employeeHistory);
                return AddOrUpdateModel(employeeHistory, new Expression<Func<EmployeeHistory, object>>[]
                {
                    x => x.EmployeeId,
                    x => x.JobTitleId,
                    x => x.DepartmentId,
                    x => x.ManagerId,
                    x => x.OrganizationLevel,
                    x => x.FromDate,
                    x => x.ToDate,
                    x => x.CountryId,

                }, "Edit", new { id = employeeHistory.EmployeeId }, "EditEmployeeHistory", result);
            }
            else
            {
                ShowError(PH.Employee.ErrorUpdateHistory);
                return View(employeeHistory);

            }

        }
        //delete
        [HttpPost]
        public virtual JsonResult DeleteEmployeeHistory(int? id)
        {
            return DeleteChildEntity<int, EmployeeHistory>(id, x => x.EmployeeHistoryId == id);
        }

        #endregion

    }
}