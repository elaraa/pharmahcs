﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class SpecialtyController : BaseControllerTemplate<vw_Specialty, Specialty, int>
    {
        public SpecialtyController() : base()
        {
            ViewNamePlural = PH.Specialty.ViewNamePlural;
            ViewNameSingular = PH.Specialty.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<Specialty, object>>[]
                {
                    x=>x.SpecialtyCode,
                    x => x.SpecialtyName,
                    x => x.CountryId
                };
        }

        protected override Expression<Func<Specialty, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.SpecialtyId == id.Value) : (Expression<Func<Specialty, bool>>)(x => 1 == 0);
        }
    }
}