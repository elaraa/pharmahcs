﻿using Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.App.Models;

namespace Web.App.Controllers
{
    public class MyController : BaseController
    {
        public MyController():base()
        {
            ApplySecurity = false;
        }
        // GET: My Profile
        public override ActionResult Index()
        {
            return RedirectToAction("Profile");
        }
        public ActionResult Profile() {

            return View(Reload());
        }
        MProfile Reload()
        {
            ViewBag.Genders = LookupHelper.GetGenders(this);
            ViewBag.MaritalStatuses = LookupHelper.GetMaritalStatuses(this);
            ViewBag.Layout = Layout;

            var profile = new MyProfileService(UserId).MyProfile;
            profile.Pic = string.IsNullOrEmpty(profile.Pic) ? Url.Content("~/Images/defaultUserIcon.png") : Url.Content(string.Format("~/uploads/employee/{0}/{1}", UserId, profile.Pic));
            return profile;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Profile(MProfile model)
        {
            var err = "";
            if (!ModelState.IsValid) {
                err += GetModelStateErrors(ModelState);
                if (!string.IsNullOrEmpty(err))
                    ShowError(err);
                return View(model);
            }
            var result = new MyProfileService(UserId).SaveBasicData(model.Mobile, model.Gender, model.MaritalStatus);
            if (result.Success)
                ShowSuccess("Data Saved.");
            else
                ShowError(result.Message);
            return View(Reload());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeImage(HttpPostedFileBase Pic)
        {
            if (Pic != null && Pic.ContentLength >= 1)
            {
                if (Pic.SaveEmployeePic(UserId) && new MyProfileService(UserId).SavePicture(Pic.FileName))
                {
                    ShowSuccess(PH.MyProfile.ShowSuccess, true);
                    Session["pic"] = Pic.FileName;
                }
                else
                    ShowError(PH.MyProfile.ShowErrorSaving, true);
            }
            else ShowError(PH.MyProfile.ShowErrorUpload, true);
            return RedirectToAction("Profile");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePass(VMChangePassword model)
        {
            if (!new MyProfileService(UserId).SavePassword(model.CurrentPassword, model.NewPassword))
                ShowError(PH.MyProfile.ShowErrorSaving, true);
            return RedirectToAction("Profile");
        }
    }
}