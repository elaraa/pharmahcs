﻿using DB.ORM.DB;
using Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class AccountController : BaseControllerTemplate<vw_Account, Account, int>
    {
        public AccountController() : base()
        {
            ViewNamePlural = PH.Account.ViewNamePlural;
            ViewNameSingular = PH.Account.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<Account, object>>[]
                {
                    x=>x.AccountName,
                    x => x.AccountTypeId,
                    x => x.AccountCode,
                    x => x.Address,
                    x => x.TerritoryId,
                    x => x.PotentialId,
                    x => x.Active,
                    x => x.Approved,
                    x => x.CategoryId,
                    x => x.CountryId

                };
        }

        protected override Expression<Func<Account, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.AccountId == id.Value) : (Expression<Func<Account, bool>>)(x => 1 == 0);
        }

        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            //
            ViewBag.AccountTypes = LookupHelper.GetMyAccountTypes(this);
            ViewBag.Territories = LookupHelper.GetMyTerritories(this);
            ViewBag.Potentials = LookupHelper.GetMyPotentials(this);
            ViewBag.Categories = LookupHelper.GetMyAccountCategories(this);
            ViewBag.Countries = LookupHelper.GetMyCountries(this);
        }

    }
}