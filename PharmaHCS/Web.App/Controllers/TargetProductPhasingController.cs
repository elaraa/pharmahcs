using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Linq.Expressions; 
using System.Web; 
using System.Web.Mvc;
using Web.App.Models;
using Web.Helpers.File;

namespace Web.App.Controllers 
{ 
    public class TargetProductPhasingController : BaseControllerList<vw_TargetProductPhasing, Target, int>
    {
        public TargetProductPhasingController()
        {
            ViewNameSingular = ViewNamePlural = PH.TargetAnalyzer.ViewNameTargetProduct;
        }
        protected override Expression<Func<Target, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.TargetId == id.Value) : (Expression<Func<Target, bool>>)(x => 1 == 0);
        }
        protected override void InitTemplateServices()
        {
            ListService = new BaseService<vw_TargetProductPhasing>(UserId);
            ActionService = new TargetService(UserId);
        } 
        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            ViewBag.Countries = LookupHelper.GetMyCountries(this);
        }
        public FileResult DownloadTemplate(int year, int countryId)
        {
            var fileName = string.Format("Product Phasing_{0}.xlsx", year);
            var table = (ActionService as TargetService).ExportProductPhasing(year, countryId);

            return new FileContentResult(
                 FileHelper.DatatableToBytes(table, "ProductPhase")
                , MimeMapping.GetMimeMapping(fileName))
            {
                FileDownloadName = fileName
            };
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VMMarketShareImport model)
        {
            FillViewBag(true);
            if (ModelState.IsValid && model.File.ContentLength > 0)
            {
                //get data
                //pass it to service
                var result = (ActionService as TargetService)
                       .ImportProductPhasing(model.Year, model.CountryId, FileHelper.GetDateSetFromExcel(model.File.InputStream));
                //if succeeded return to list
                //save file
                if (result.Succeeded)
                {
                    ShowSuccess(PH.TargetAnalyzer.UploadedsuccessfullyCode, true);
                    model.File.SaveTargetProductFile(model.Year);
                    return RedirectToAction("Index");
                }
                else
                    ShowError(
                        string.Format(PH.TargetAnalyzer.ErrorUploading
                        , string.Join("<br/>",result.ErrorList)
                        , result.ErrorList.Count));
            }
            return View(model);
        }
        public ActionResult Edit(int id)
        {
            FillViewBag(false);
            return View(ActionService.GetOne(w => w.TargetId == id));
        }
        public ActionResult ReadDetailsList([DataSourceRequest] DataSourceRequest request, int year, int countryId)
        {
            return Json(new BaseService<vw_targetPhasing_Details>(UserId)
                .Get(w => w.Year == year && w.CountryId == countryId, true).ToDataSourceResult(request));
        }
    } 
}