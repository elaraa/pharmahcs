﻿using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using System;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class BaseControllerList<LModel, AModel, AModelDto, IDType> : BaseController
        where LModel : class, new()
        where AModel : class, new()
        where AModelDto : class, new()
        where IDType : struct
    {
        protected BaseService<LModel> ListService { get; set; }
        protected BaseService<AModelDto> ActionService { get; set; }
        public BaseControllerList() : base()
        {
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            InitTemplateServices();
            base.OnActionExecuting(filterContext);
        }
        protected virtual void InitTemplateServices()
        {
            ListService = new BaseService<LModel>(UserId);
            ActionService = new BaseService<AModelDto>(UserId);
        }
        public virtual ActionResult ReadList([DataSourceRequest] DataSourceRequest request)
        {
            return Json(ListService.Get(null, true).ToDataSourceResult(request));
        }

        public virtual ActionResult Create()
        {
            FillViewBag(true);
            return View();
        }

        protected virtual Expression<Func<AModel, bool>> GetOneExpression(IDType? id)
        {
            return null;
        }

        protected Expression<Func<AModel, object>>[] PropertiesToUpdate
        {
            get;
            set;
        }

        [HttpPost]
        public virtual JsonResult Delete(IDType? id)
        {
            if (id == null)
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest));

            AModel model = ActionService.GetOne(GetOneExpression(id));
            if (model == null)
                return Json(HttpNotFound());

            return Json(ActionService.Delete(GetOneExpression(id)) > 0);
        }

    }
    public class BaseControllerTemplateDto<LModel, AModel, AModelDto, IDType> : BaseControllerList<LModel, AModel, AModelDto, IDType>
        where LModel : class, new()
        where AModel : class, new()
        where AModelDto : class, new()
        where IDType : struct
    {
        public BaseControllerTemplateDto() : base()
        {
            //InitTemplateServices();
        }

        public ActionResult Edit(IDType? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            FillViewBag(false);
            var model = Mapper.Map<AModelDto>(ActionService.GetOne(GetOneExpression(id)));
            if (model == null)
                return HttpNotFound();

            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AModelDto model)
        {
            return AddOrUpdate(model);
        }
        public override ActionResult Create()
        {
            FillViewBag(true);
            return View(new AModelDto { });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AModelDto model)
        {
            return AddOrUpdate(model, true);
        }

        private ActionResult AddOrUpdate(AModelDto model, bool isCreate = false, string successView = "Index")
        {
            string modelErrors = "";
            RunResult result;
            if (ModelState.IsValid)
            {
                if (ActionService == null)
                    result = new BaseService<AModel>(UserId).AddOrUpdate(Mapper.Map<AModel>(model), isCreate);
                else
                    result = ActionService.AddOrUpdate(model, isCreate);

                if (result.Succeeded)
                {
                    ShowSuccess(result.SuccessDesc, true);
                    return RedirectToAction(successView);
                }

                ShowError(result.ErrorDesc);
            }
            modelErrors += GetModelStateErrors(ModelState);
            if (!string.IsNullOrEmpty(modelErrors))
                ShowError(modelErrors);
            FillViewBag(isCreate);
            return View(model);
        }
    }
}