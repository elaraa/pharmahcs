﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using System;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class BaseControllerList<LModel, AModel, IDType> : BaseController
        where LModel : class, new()
        where AModel : class, new()
        where IDType : struct
    {
        protected BaseService<LModel> ListService { get; set; }
        protected BaseService<AModel> ActionService { get; set; }
        public BaseControllerList() : base()
        {
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            InitTemplateServices();
            base.OnActionExecuting(filterContext);
        }
        protected virtual void InitTemplateServices()
        {
            ListService = new BaseService<LModel>(UserId);
            ActionService = new BaseService<AModel>(UserId);
        }
        public virtual ActionResult ReadList([DataSourceRequest] DataSourceRequest request)
        {
            return Json(ListService.Get(null, true).ToDataSourceResult(request));
        }

        public virtual ActionResult Create()
        {
            FillViewBag(true);
            return View();
        }
        
        protected virtual Expression<Func<AModel, bool>> GetOneExpression(IDType? id)
        {
            return null;
        }

        protected Expression<Func<AModel, object>>[] PropertiesToUpdate
        {
            get;
            set;
        }
        
        [HttpPost]
        public virtual JsonResult Delete(IDType? id)
        {
            if (id == null)
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest));

            AModel model = ActionService.GetOne(GetOneExpression(id));
            if (model == null)
                return Json(HttpNotFound());

            return Json(ActionService.Delete(GetOneExpression(id)) > 0);
        }

    }

    /// <summary>
    /// Controller for Entity Models that will carry on basic functions of controller like navigating to index, create, edit and delete functions
    /// </summary>
    /// <typeparam name="LModel"> List Model [to be used in list]</typeparam>
    /// <typeparam name="AModel">Actions Model [to be used in create, edit, delete]</typeparam>
    public class BaseControllerTemplate<LModel, AModel, IDType> : BaseControllerList<LModel, AModel, IDType>
        where LModel : class, new()
        where AModel : class, new()
        where IDType : struct
    {
        public BaseControllerTemplate() : base()
        {
            //InitTemplateServices();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create(AModel model)
        {
            FillViewBag(false);
            return CreateChildEntity(model, "Index", null, "Create", ActionService);
        }
        public virtual ActionResult Edit(IDType? id)
        {
            FillViewBag(false);
            return EditChildEntity(id, GetOneExpression(id), "Edit", ActionService);
        }
     
        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit(AModel model)
        {
            FillViewBag(false);
            return EditChildEntity(model, PropertiesToUpdate, "Index", null, "Edit", ActionService);
        }
    }
}