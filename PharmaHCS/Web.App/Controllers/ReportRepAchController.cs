﻿using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class ReportRepAchController : BaseReportController
    {

        public override ActionResult Index()
        {
            ViewBag.Time = LookupHelper.GetTimeDefinitions(this);
            ViewBag.Teams = LookupHelper.GetMyTeams(this);
            ViewBag.ReportName = PH.Reports.ViewNameRepAchievement;

            return base.Index();
        }
        public ReportRepAchController()
        {
            ViewNamePlural = ViewNameSingular = PH.Reports.ViewNameRepAchievement;
            //ReportName = "BU+Achievement";

        }


    }
}