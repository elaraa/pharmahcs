﻿using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class ReportPMAchProductController : BaseReportController
    {

        public override ActionResult Index()
        {
            ViewBag.Time = LookupHelper.GetTimeDefinitions(this);
            ViewBag.Teams = LookupHelper.GetMyTeams(this);
            ViewBag.ReportName = PH.Reports.ViewNamePMAchievementProduct;

            return base.Index();
        }
        public ReportPMAchProductController()
        {
            ViewNamePlural = ViewNameSingular = PH.Reports.ViewNamePMAchievementProduct;
            //ReportName = "BU+Achievement";

        }


    }
}