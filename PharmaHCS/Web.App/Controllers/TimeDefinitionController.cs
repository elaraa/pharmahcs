﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class TimeDefinitionController : BaseControllerTemplate<vw_TimeDefinition, TimeDefinition, int>
    {
        public TimeDefinitionController() : base()
        {
            ViewNamePlural = PH.TimeDefinition.ViewNamePlural;
            ViewNameSingular = PH.TimeDefinition.ViewNamePlural;

            PropertiesToUpdate = new Expression<Func<TimeDefinition, object>>[]
                {
                    x=>x.PeriodName,
                    x => x.FromDate,
                    x => x.ToDate,
                    x => x.QuarterName,
                    x => x.Year,
                    x => x.CountryId

                };
        }
        protected override void FillViewBag(bool isCreate)
        {
            ViewBag.Countries = LookupHelper.GetMyCountries(this);
            base.FillViewBag(isCreate);
        }
        protected override Expression<Func<TimeDefinition, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.TimeDefinitionId == id.Value) : (Expression<Func<TimeDefinition, bool>>)(x => 1 == 0);
        }
    }
}