﻿using DB.ORM.DB;
using Services.Business;
using System;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using Web.App.Models;

namespace Web.App.Controllers
{
    public class UserController : BaseControllerList<vw_User, Employee,int>
    {
        public UserController()
        {
            ViewNamePlural = PH.User.ViewNamePlural;
            ViewNameSingular = PH.User.ViewNameSingular;
        }
        void FillViewBag() {
            ViewBag.Title = string.Format("{1} {0}", ViewNameSingular, "Edit");
            ViewBag.EntityName = ViewNameSingular;
            ViewBag.Roles = LookupHelper.GetMyRoles(this);
        }
        public ActionResult Edit(int? id) {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            FillViewBag();
            var emp = ActionService.GetOne(w=>w.EmployeeId == id);
            if (emp == null)
                return HttpNotFound();
            return View(new VMUser {
                UserId = emp.EmployeeId,
                LoginName = emp.LoginId,
                PictureUrl = string.IsNullOrEmpty(emp.Picture)?"~/images/defaultUserIcon.png":string.Format("~/Uploads/Employee/{0}/{1}", emp.EmployeeId,emp.Picture),
                Name = emp.EmployeeName
            });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VMUser model)
        {
            var err = "";
            if (ModelState.IsValid) {
                var emp = ActionService.GetOne(w => w.EmployeeId == model.UserId);
                if (!string.IsNullOrEmpty(model.Password) && !string.IsNullOrEmpty(model.LoginName))
                {
                    emp.Password = LoginService.HashString( model.Password);
                    emp.LoginId = model.LoginName;
                }
                else {
                    if (string.IsNullOrEmpty(model.LoginName)) { emp.LoginId = null; emp.Password = null; }
                    if (!string.IsNullOrEmpty(model.Password) && string.IsNullOrEmpty(model.LoginName)) { err=(PH.User.ErrorLogin); }
                }
                if (model.PictureFile != null && model.PictureFile.ContentLength > 0){
                    model.PictureFile.SaveEmployeePic(emp.EmployeeId);
                    emp.Picture = model.PictureFile.FileName;
                }
                if (ActionService.Update(emp, new Expression<Func<Employee, object>>[]{
                    x=>x.LoginId,
                    x => x.Password,
                    x => x.Picture,
                    x => x.RoleId
                }))
                    return RedirectToAction("Index");
                else
                    err += PH.User.ErrorSaveData;
            }
            err += GetModelStateErrors(ModelState);
            if (!string.IsNullOrEmpty(err))
                ShowError(err);
            FillViewBag();
            return View(model);
            
        }

    }
}