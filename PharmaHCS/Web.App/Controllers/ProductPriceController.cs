﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class ProductPriceController : BaseControllerTemplate<vw_ProductPrice, ProductPrice, int>
    {

        public ProductPriceController() : base()
        {
            ViewNamePlural = PH.Product.ViewNamePluralProductPrice;
            ViewNameSingular = PH.Product.ViewNameSingularProductPrice;
            
            PropertiesToUpdate = new Expression<Func<ProductPrice, object>>[]
                {
                    x=>x.ProductId,
                    x => x.DosageFormId,
                    x => x.DistributorId,
                    x => x.AccountCategoryId,
                    x => x.TerritoryId,
                    x => x.AccountId,
                    x => x.FromDate,
                    x => x.ToDate,
                    x => x.Price,
                };
        }

        protected override void InitTemplateServices()
        {
            ActionService = new ProductPriceService(UserId);
            ListService = new BaseService<vw_ProductPrice>(UserId);
        }
        protected override Expression<Func<ProductPrice, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.ProductPriceId == id.Value) : (Expression<Func<ProductPrice, bool>>)(x => 1 == 0);
        }

        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            //
            ViewBag.Products = LookupHelper.GetMyProducts(this);
            ViewBag.PriceCritria = LookupHelper.GetProductPriceCritria();
            ViewBag.DosageForms = LookupHelper.GetMyDosageForms(this);
            ViewBag.Distributors = LookupHelper.GetMyDistributors(this);
            ViewBag.Territories = LookupHelper.GetMyTerritories(this);
        }

        public override ActionResult ReadList([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new ProductPriceService(UserId).GetProductPrices().ToDataSourceResult(request));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Create(ProductPrice model)
        {
            FillViewBag(true);
            var result = (ActionService as ProductPriceService).IsOverlapped(model);
            return AddOrUpdateModel(model, null, "Index", null, "Create", result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Edit(ProductPrice model)
        {
            FillViewBag(false);
            var result = (ActionService as ProductPriceService).IsOverlapped(model);
            return AddOrUpdateModel(model, PropertiesToUpdate, "Index", null, "Edit", result);
        }

    }
}