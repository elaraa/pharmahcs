﻿using DB.ORM.DB;
using Services.Business;
using Services.Business.CRM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class VisitController : BaseControllerList<vw_VisitsPerDate, vw_VisitsPerDate, DateTime>
    {
        public VisitController() : base()
        {
            ViewNamePlural = PH.Visit.ViewNamePlural;
            ViewNameSingular = PH.Visit.ViewNameSingular;
        }
        protected override void InitTemplateServices()
        {
            ListService = new VisitService(UserId);
            ActionService = new VisitService(UserId);
        }
        public ActionResult Edit(string id)
        {
            var splits = id.Split('_');
            if(splits.Count() < 2) new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var empId = Convert.ToInt32( splits[0]);
            var date = Convert.ToDateTime(splits[1]);

            FillViewBag(false);
            ViewBag.VisitsList = (ActionService as VisitService).ReadVisitsList(date, empId).ToList();
            ViewBag.GKey = new GMapsSettings(UserId).GoogleMapsKey;
            return View((ActionService as VisitService).GetByDate(date,empId));
        }
        public JsonResult ApproveVisit(Guid id) {
            return Json((ActionService as VisitService).ApproveRejectVisit(id, true));
        }
        public JsonResult RejectVisit(Guid id) {
            return Json((ActionService as VisitService).ApproveRejectVisit(id, false));
        }
    }
}