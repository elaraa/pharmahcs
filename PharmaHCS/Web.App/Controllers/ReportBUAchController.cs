﻿using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class ReportBUAchController : BaseReportController
    {
        public override ActionResult Index()
        {
            ViewBag.Time = LookupHelper.GetTimeDefinitions(this);
            ViewBag.Teams = LookupHelper.GetMyTeams(this);
            ViewBag.ReportName = PH.Reports.ViewNamePluralBUAchievement;

            return base.Index();
        }
        public ReportBUAchController()
        {
            ViewNamePlural= ViewNameSingular = PH.Reports.ViewNamePluralBUAchievement;
            //ReportName = "BU+Achievement";

        }
    }
}