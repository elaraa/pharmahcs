﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business.Sales;
using System.Linq;
using System.Web.Mvc;


namespace Web.App.Controllers
{
    public class TargetAnalyzerController : BaseController
    {
        public TargetAnalyzerController()
        {
            ViewNameSingular = ViewNamePlural = PH.TargetAnalyzer.ViewNameTarget;
        }
        public override ActionResult Index()
        {
            ViewBag.PageType = PH.TargetAnalyzer.Dashboard;
            ViewBag.Title = PH.TargetAnalyzer.targetAnalyzer;

            return View(new TargetAnalyzer(UserId).GetSummary());
        }
        public ActionResult _GetProductAnalysis_Table([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new TargetAnalyzer(UserId).GetProductAnalysis().ToDataSourceResult(request));
        }
        public ActionResult _GetProductAnalysisChart()
        {
            return Json(new TargetAnalyzer(UserId).GetProductAnalysis());
        }

        public ActionResult _GetTerritoryAnalysis_Table([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new TargetAnalyzer(UserId).GetTerritoryAnalysis().ToDataSourceResult(request));
        }
        public ActionResult _GetTerritoryAnalysisChart(string qtype = "q", int top = 10)
        {
            var data = new TargetAnalyzer(UserId).GetTerritoryAnalysis();
            if (qtype.ToLower() == "q")
                return Json(data.OrderByDescending(o => o.TerritoryTarget).Take(top));
            return Json(data.OrderByDescending(o => o.TerritoryTarget_Qty).Take(top));
        }


        public ActionResult _GetProfileAnalysis_Table([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new TargetAnalyzer(UserId).GetProfileAnalysis().ToDataSourceResult(request));
        }
    }
}