﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class MovementLogsController : BaseController
    {
        public MovementLogsController() :base()
        {
             ViewNamePlural = "Movement Logs";
            ViewNameSingular = "Movement Log";
        }
        // GET: InMarketSalesAnalyzer
        public override ActionResult Index()
        {
            ViewBag.MedicalRep = LookupHelper.GetMyMedicalRep(this);
           
            return View(new MovementDto { Date=DateTime.Now});
        }

        [HttpPost]
        public ActionResult GetVisits(int? id,DateTime date)
        {
           // var res = new MovementLogService(UserId).getVisits(id.Value,date);
            return Json(new MovementLogService(UserId).getVisits(id.Value, date));
        }
       
    }
    

    public class MovementDto
    {
        public int EmployeeId { get; set; } 
        public DateTime Date { get; set; }
    }
}