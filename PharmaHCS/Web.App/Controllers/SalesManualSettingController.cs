﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class SalesManualSettingController : BaseController
    {
        public SalesManualSettingController()
        {
            ViewNamePlural = ViewNameSingular = PH.SalesManualSetting.ViewNamePlural;
        }
        public override ActionResult Index()
        {
            ViewBag.Time = LookupHelper.GetTimeDefinitions(this);

            return base.Index();
        }
        [HttpPost]
        public ActionResult EmployeeStructureRun()
        {

            var result = new Services.Business.Sales.AdminManualRun(UserId).ExecuteEmpStructure();
            if (result.Succeeded)
                ShowSuccess(PH.SalesManualSetting.SuccessfullCode, true);
            else
                ShowError(string.Format(PH.SalesManualSetting.ErrorCode, result.ErrorDesc), true);

            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult CeilingCalcRun(int timeId) {

            var result = new Services.Business.Sales.AdminManualRun(UserId).ExecuteCeilingCalc(timeId);
            if (result.Succeeded)
                ShowSuccess(PH.SalesManualSetting.SuccessfullCode, true);
            else
                ShowError(string.Format(PH.SalesManualSetting.ErrorCode, result.ErrorDesc), true);

            return RedirectToAction("Index");
        }
    }
}