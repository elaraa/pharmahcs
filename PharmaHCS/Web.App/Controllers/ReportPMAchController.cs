﻿using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class ReportPMAchController : BaseReportController
    {

        public override ActionResult Index()
        {
            ViewBag.Time = LookupHelper.GetTimeDefinitions(this);
            ViewBag.Teams = LookupHelper.GetMyTeams(this);
            ViewBag.ReportName = PH.Reports.ViewNamePMAchievement;

            return base.Index();
        }
        public ReportPMAchController()
        {
            ViewNamePlural = ViewNameSingular = PH.Reports.ViewNamePMAchievement;
            //ReportName = "BU+Achievement";

        }


    }
}