﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class TeamController : BaseControllerTemplate<vw_Team, Team, int>
    {
        public TeamController() : base()
        {
           
            ViewNamePlural = PH.Team.ViewNamePlural;
            ViewNameSingular = PH.Team.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<Team, object>>[]
                {
                    x=>x.TeamCode,
                    x => x.TeamName,
                    x => x.CountryId
                };
        }
        protected override void InitTemplateServices()
        {
            ActionService = new TeamService(UserId);
            ListService = new BaseService<vw_Team>(UserId);
        }
        protected override Expression<Func<Team, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.TeamId == id.Value) : (Expression<Func<Team, bool>>)(x => 1 == 0);
        }
        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            ViewBag.Countries = LookupHelper.GetMyCountries(this);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Create(Team model)
        {
            FillViewBag(true);
            var result = (ActionService as TeamService).IsOverlapped(model);
            return AddOrUpdateModel(model, null, "Index", null, "Create", result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Edit(Team model)
        {
            FillViewBag(false);
            var result = (ActionService as TeamService).IsOverlapped(model);
            return AddOrUpdateModel(model, PropertiesToUpdate, "Index", null, "Edit", result);
        }

        #region Team Profile

        //List
        public ActionResult ReadTeamProfileList([DataSourceRequest] DataSourceRequest request, int TeamId)
        {
            return Json(new BaseService<vw_TeamProfile>(UserId).Get(w => w.TeamId == TeamId, true).ToDataSourceResult(request));
        }
        private void FillViewBag_TeamProfile(bool isCreate)
        {
            ViewBag.Title = string.Format("{1} {0}", "Team Profile", isCreate ? "Create" : "Edit");
            ViewBag.EntityName = "Team Profile";

            ViewBag.Teams = LookupHelper.GetMyTeams(this);
            
        }
        //create
        public ActionResult CreateTeamProfile(int teamId)
        {
            FillViewBag_TeamProfile(true);
            var model = new TeamProfile { TeamId = teamId };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTeamProfile(TeamProfile teamProfile)
        {
            FillViewBag_TeamProfile(true);
            return CreateChildEntity<TeamProfile>(teamProfile, "Edit", new { id = teamProfile.TeamId}
            , "CreateTeamProfile");
        }
        //edit
        public ActionResult EditTeamProfile(int id)
        {
            FillViewBag_TeamProfile(false);
            return EditChildEntity<int, TeamProfile>(id, x => x.ProfileId == id, "EditTeamProfile");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTeamProfile(TeamProfile teamProfile)
        {
            FillViewBag_TeamProfile(false);
            return EditChildEntity<TeamProfile>(teamProfile, new Expression<Func<TeamProfile, object>>[]
                {   
                    x => x.TeamId,
                     x => x.ProfileCode,
                      x => x.ProfileName,
                       x => x.Active,

              
                }
            , "Edit", new { id = teamProfile.TeamId }, "EditTeamProfile");
        }
        //delete
        [HttpPost]
        public virtual JsonResult DeleteTeamProfile(int? id)
        {
            return DeleteChildEntity<int, TeamProfile>(id, x => x.ProfileId == id);
        }

        #endregion




        #region Team Sales Product

        //List
        public ActionResult ReadTeamSalesProductList([DataSourceRequest] DataSourceRequest request, int TeamId)
        {
            return Json(new BaseService<vw_TeamSalesProduct>(UserId).Get(w => w.TeamId == TeamId, true).ToDataSourceResult(request));
        }
        private void FillViewBag_TeamSalesProduct(bool isCreate)
        {
            ViewBag.Title = string.Format("{1} {0}", "Team Sales Product", isCreate ? "Create" : "Edit");
            ViewBag.EntityName = "Team Sales Product";

            ViewBag.Teams = LookupHelper.GetMyTeams(this);
            ViewBag.Products = LookupHelper.GetMyProducts(this);
 
        }
        //create
        public ActionResult CreateTeamSalesProduct(int teamId)
        {
            FillViewBag_TeamSalesProduct(true);
            var model = new TeamSalesProduct { TeamId = teamId, FromDate = DateTime.Now };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTeamSalesProduct(TeamSalesProduct teamSalesProduct)
        {
            FillViewBag_TeamSalesProduct(true);

            var result = (ActionService as TeamService).IsOverlapped(teamSalesProduct);
            return AddOrUpdateModel(teamSalesProduct, null, "Edit", new { id = teamSalesProduct.TeamId }
                                  , "CreateTeamSalesProduct", result);
        }
        //edit
        public ActionResult EditTeamSalesProduct(int id)
        {
            FillViewBag_TeamSalesProduct(false);
            return EditChildEntity<int, TeamSalesProduct>(id, x => x.TeamSalesProductId == id, "EditTeamSalesProduct");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTeamSalesProduct(TeamSalesProduct teamSalesProduct)
        {
            FillViewBag_TeamSalesProduct(false);
            var result = (ActionService as TeamService).IsOverlapped(teamSalesProduct);

            return AddOrUpdateModel(teamSalesProduct, 
                new Expression<Func<TeamSalesProduct, object>>[] { x => x.TeamId, x => x.ProductId, x => x.FromDate,  x => x.ToDate },
                "Edit", new { id = teamSalesProduct.TeamId }, "EditTeamSalesProduct", result);
        }
        //delete
        [HttpPost]
        public virtual JsonResult DeleteTeamSalesProduct(int? id)
        {
            return DeleteChildEntity<int, TeamSalesProduct>(id, x => x.TeamSalesProductId == id);
        }

        #endregion



    }
}