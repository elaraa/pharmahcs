﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.App.Models
{
    public class VMMarketShareImport
    {
        [Required]
        public int Year { get; set; }
        public int Level { get; set; }
        [Display(Name = "Country")]
        [Required]
        public int CountryId { get; set; }
        [Required]
        public HttpPostedFileBase File { get; set; }
    }
}