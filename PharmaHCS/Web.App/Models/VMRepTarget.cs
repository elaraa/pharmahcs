﻿using System.ComponentModel.DataAnnotations;

namespace Web.App.Models
{
    public class VMRepTarget
    {
        [Required]
        public int TimeDefinitionId { get; set; }
        [Required]
        public int CreateOption { get; set; }
        public int RepId { get; set; }
    }
}