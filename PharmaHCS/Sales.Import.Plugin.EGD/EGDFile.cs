﻿using Sales.Core.ImportFile;
using System;
using System.IO.Compression;
using System.Linq;
using System.Text;
using TinyCsvParser;

namespace Sales.Import.Plugin.EGD
{
    public class EGDFile : PluginBase
    {
        public ValidationStatus Status { get; set; }
        public override void ProcessData(PluginInput[] files)
        {
            ///1. Unzip File
            ///2. check if the 2 file names exists [1002.txt, Total_branches.txt]
            ///3. read the file 1002.txt
            ///4. read the file Total_branches.txt
            ///return
            Status = ValidationStatus.InvalidFormat;
            var file1002Exist = false; var fileTotal_branchesExist = false; 
            foreach (var file in files)
            {
                if (file.FileName.ToLower().Contains(".zip"))
                    using (var archive = new ZipArchive(file.FileStream, ZipArchiveMode.Read))
                    {
                        foreach (ZipArchiveEntry entry in archive.Entries)
                        {
                            if (entry.FullName.Contains("1002.txt"))
                            {
                                var csvParserOptions = new CsvParserOptions(true, '\t');
                                var csvMapper = new Csv1002Mapping();
                                var csvParser = new CsvParser<C1002File>(csvParserOptions, csvMapper);
                                var enc = Encoding.GetEncoding("windows-1256");
                                var result = csvParser
                                    .ReadFromStream(entry.Open(), enc)
                                    .ToList();

                                decimal? qty = null; decimal? returns = null;
                                DateTime? date = null;
                                foreach (var item in result)
                                {
                                    qty = Decimal.TryParse(item.Result.QUANTIY, out decimal q) ? q : (decimal?)null;
                                    returns = Decimal.TryParse(item.Result.compute_0009, out q) ? q : (decimal?)null;
                                    date = DateTime.TryParse(item.Result.DATE_SWITCH, out DateTime dateTime) ? dateTime : (DateTime?)null;


                                    var custCode = item.Result.DESTINATION_BRANCH_CODE1.Trim();
                                    if (custCode.Length > 0 && custCode[0] == '0') custCode = custCode.Substring(1);

                                    var record = new PluginRecord
                                    {
                                        CustomerCode = item.Result.DESTINATION_BRANCH_CODE.Trim() + custCode,//item.Result.DESTINATION_BRANCH_CODE1.Trim(),
                                        CustomerName = item.Result.NAME,
                                        CustomerAddress = item.Result.ADDRESS,
                                        CustomerTerritory = item.Result.ADDRESS,
                                        DosageFormCode = item.Result.ITEM_CODE,
                                        DosageFormName = item.Result.ITEM_NAME,
                                        InvoiceNumber = item.Result.INVOICE_NO,
                                        SalesDate = date,
                                        DistributorBranchCode = item.Result.DESTINATION_BRANCH_CODE1
                                    };

                                    if (item.Result.STATUS == "1") {
                                        if (qty.HasValue && qty > 0)
                                        {
                                            record.Quantity = qty;
                                            record.InvalidQty = !qty.HasValue;
                                            record.FileQuantity = item.Result.QUANTIY;
                                            FileRecords.Add(record);
                                        }
                                        if (returns.HasValue && returns > 0)
                                        {
                                            record.Quantity = -1 * returns;
                                            record.InvalidQty = !returns.HasValue;
                                            record.FileQuantity = item.Result.compute_0009;
                                            FileRecords.Add(record);
                                        }
                                    }

                                    
                                }

                                file1002Exist = true;
                            }
                            else if (entry.FullName.Contains("Total_branches.txt"))
                            {

                                //Console.WriteLine("Total_branches.txt");
                                //Console.ReadKey();
                                var csvParserOptions = new CsvParserOptions(true, '\t');
                                var csvMapper = new CsvTotal_branchesMapping();
                                var csvParser = new CsvParser<CTotal_branchesFile>(csvParserOptions, csvMapper);
                                var enc = Encoding.GetEncoding("windows-1256");
                                var result = csvParser
                                    .ReadFromStream(entry.Open(), enc)
                                    .ToList();

                                decimal? qty = null; decimal? returns = null;
                                DateTime? date = null;
                                foreach (var item in result)
                                {
                                    qty = Decimal.TryParse(item.Result.OUT_CUST, out decimal q) ? q : (decimal?)null;
                                    returns = Decimal.TryParse(item.Result.RET_CUST, out q) ? q : (decimal?)null;
                                    date = DateTime.TryParse(item.Result.DATE_TRANS, out DateTime dateTime) ? dateTime : (DateTime?)null;

                                    var custCode = item.Result.CUSTOMER_CODE.Trim();
                                    if (custCode.Length > 0 && custCode[0] == '0') custCode = custCode.Substring(1);


                                    var record = new PluginRecord
                                    {
                                        CustomerCode = item.Result.BRANCH_CODE.Trim() + custCode,
                                        CustomerName = item.Result.CUSTOMER_NAME,
                                        CustomerAddress = item.Result.CUSTOMER_ADDRESS,
                                        CustomerTerritory = item.Result.GOV_NAME + "->" + item.Result.DISTRICT_NAME,
                                        DosageFormCode = item.Result.ITEM_CODE,
                                        DosageFormName = item.Result.ITEM_NAME,
                                        SalesDate = date,
                                        DistributorBranchCode = item.Result.BRANCH_CODE,
                                        DistributorBranchName = item.Result.BRANCH_NAME,
                                        InvoiceNumber = item.Result.FIELD2,
                                    };

                                    if (item.Result.STATUS == "1") {
                                        if (qty.HasValue && qty > 0)
                                        {
                                            record.Quantity = qty;
                                            record.InvalidQty = !qty.HasValue;
                                            record.FileQuantity = item.Result.OUT_CUST;
                                            FileRecords.Add(record);
                                        }
                                        if (returns.HasValue && returns > 0)
                                        {
                                            record.Quantity = -1 * returns /*1 return, 0 sales*/;
                                            record.InvalidQty = !returns.HasValue;
                                            record.FileQuantity = item.Result.RET_CUST;
                                            FileRecords.Add(record);
                                        }
                                    }
                                    
                                }
                                fileTotal_branchesExist = true;
                            }

                        }
                    }
                if (file.FileName.ToLower().Contains(".xls")) {//Manual file
                        ///read the format
                        ///insert records
                }


            }
            Status = fileTotal_branchesExist || file1002Exist ? ValidationStatus.Succeeded : ValidationStatus.InvalidFormat;
        }

        public override ValidationStatus ValidateFiles()
        {
            return Status;
        }
    }
}
