﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyCsvParser.Mapping;

namespace Sales.Import.Plugin.EGD
{
    class CsvTotal_branchesMapping : CsvMapping<CTotal_branchesFile>
    {
        public CsvTotal_branchesMapping()
            : base()
        {
            MapProperty(0, x => x.DATE_TRANS);
            MapProperty(1, x => x.ITEM_CODE);
            MapProperty(2, x => x.ITEM_NAME);
            MapProperty(3, x => x.STATUS);
            MapProperty(4, x => x.VENDOR_CODE);
            MapProperty(5, x => x.CUSTOMER_NAME);
            MapProperty(6, x => x.GOV_NAME);
            MapProperty(7, x => x.DISTRICT_NAME);
            MapProperty(8, x => x.RET_CUST);
            MapProperty(9, x => x.OUT_CUST);
            MapProperty(10, x => x.BRANCH_CODE);
            MapProperty(11, x => x.CUSTOMER_CODE);
            MapProperty(12, x => x.CUSTOMER_ADDRESS);
            MapProperty(13, x => x.EXPIRY_DATE);
            MapProperty(14, x => x.TRANS_SERIAL);
            MapProperty(15, x => x.BATCH_NO);
            MapProperty(16, x => x.INV_NO);
            MapProperty(17, x => x.AREA_NO);
            MapProperty(18, x => x.DISTRICT_NO);
            MapProperty(19, x => x.FIELD1);
            MapProperty(20, x => x.BRANCH_NAME);
            MapProperty(21, x => x.FIELD2);
            MapProperty(22, x => x.FIELD3);
            MapProperty(23, x => x.FIELD4);
            MapProperty(24, x => x.REPRSENT_ID);
            MapProperty(25, x => x.FIELD5);

        }
    }
    public class CTotal_branchesFile
    {
        public string DATE_TRANS { get; set; }
        public string ITEM_CODE { get; set; }
        public string ITEM_NAME { get; set; }
        public string STATUS { get; set; }
        public string VENDOR_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string GOV_NAME { get; set; }
        public string DISTRICT_NAME { get; set; }
        public string RET_CUST { get; set; }
        public string OUT_CUST { get; set; }
        public string BRANCH_CODE { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_ADDRESS { get; set; }
        public string EXPIRY_DATE { get; set; }
        public string TRANS_SERIAL { get; set; }
        public string BATCH_NO { get; set; }
        public string INV_NO { get; set; }
        public string AREA_NO { get; set; }
        public string DISTRICT_NO { get; set; }
        public string FIELD1 { get; set; }
        public string BRANCH_NAME { get; set; }
        public string FIELD2 { get; set; }
        public string FIELD3 { get; set; }
        public string FIELD4 { get; set; }
        public string REPRSENT_ID { get; set; }
        public string FIELD5 { get; set; }


    }
}
