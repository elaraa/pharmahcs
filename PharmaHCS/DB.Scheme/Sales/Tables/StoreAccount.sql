﻿CREATE TABLE [Sales].[StoreAccount] (
    [StoreAccountId]   INT      IDENTITY (1, 1) NOT NULL,
    [AccountId]        INT      NOT NULL,
    [FromTimeId]       INT      NOT NULL,
    [ToTimeId]         INT      NULL,
    [CreationDate]     DATETIME CONSTRAINT [DF_StoreAccount_CreationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME NULL,
    [CreatedById]      INT      NULL,
    [LastModifiedById] INT      NULL,
    CONSTRAINT [PK_StoreAccount] PRIMARY KEY CLUSTERED ([StoreAccountId] ASC),
    CONSTRAINT [FK_StoreAccount_Account] FOREIGN KEY ([AccountId]) REFERENCES [Shared].[Account] ([AccountId]),
    CONSTRAINT [FK_StoreAccount_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_StoreAccount_FromTimeDefinition] FOREIGN KEY ([FromTimeId]) REFERENCES [Shared].[TimeDefinition] ([TimeDefinitionId]),
    CONSTRAINT [FK_StoreAccount_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_StoreAccount_ToTimeDefinition] FOREIGN KEY ([ToTimeId]) REFERENCES [Shared].[TimeDefinition] ([TimeDefinitionId])
);


GO
ALTER TABLE [Sales].[StoreAccount] NOCHECK CONSTRAINT [FK_StoreAccount_Account];


GO
ALTER TABLE [Sales].[StoreAccount] NOCHECK CONSTRAINT [FK_StoreAccount_CreatedBy];


GO
ALTER TABLE [Sales].[StoreAccount] NOCHECK CONSTRAINT [FK_StoreAccount_FromTimeDefinition];


GO
ALTER TABLE [Sales].[StoreAccount] NOCHECK CONSTRAINT [FK_StoreAccount_LastModifiedBy];


GO
ALTER TABLE [Sales].[StoreAccount] NOCHECK CONSTRAINT [FK_StoreAccount_ToTimeDefinition];



