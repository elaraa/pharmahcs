﻿CREATE TABLE [Sales].[CeilingDosageAccount] (
    [CeilingDosageAccountId] INT             IDENTITY (1, 1) NOT NULL,
    [DosageFormId]           INT             NOT NULL,
    [AccountId]              INT             NULL,
    [FromTimeId]             INT             NOT NULL,
    [ToTimeId]               INT             NULL,
    [CeilingQty]             DECIMAL (18, 6) NOT NULL,
    [CreationDate]           DATETIME        CONSTRAINT [DF_CeilingDosageAccount_CreationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]       DATETIME        NULL,
    [CreatedById]            INT             NULL,
    [LastModifiedById]       INT             NULL,
    CONSTRAINT [PK_CeilingDosageAccount] PRIMARY KEY CLUSTERED ([CeilingDosageAccountId] ASC),
    CONSTRAINT [FK_CeilingDosageAccount_Account] FOREIGN KEY ([AccountId]) REFERENCES [Shared].[Account] ([AccountId]),
    CONSTRAINT [FK_CeilingDosageAccount_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_CeilingDosageAccount_FromTimeDefinition] FOREIGN KEY ([FromTimeId]) REFERENCES [Shared].[TimeDefinition] ([TimeDefinitionId]),
    CONSTRAINT [FK_CeilingDosageAccount_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_CeilingDosageAccount_ProductDosageForm] FOREIGN KEY ([DosageFormId]) REFERENCES [Shared].[ProductDosageForm] ([DosageFormId]),
    CONSTRAINT [FK_CeilingDosageAccount_ToTimeDefinition] FOREIGN KEY ([ToTimeId]) REFERENCES [Shared].[TimeDefinition] ([TimeDefinitionId])
);


GO
ALTER TABLE [Sales].[CeilingDosageAccount] NOCHECK CONSTRAINT [FK_CeilingDosageAccount_Account];


GO
ALTER TABLE [Sales].[CeilingDosageAccount] NOCHECK CONSTRAINT [FK_CeilingDosageAccount_CreatedBy];


GO
ALTER TABLE [Sales].[CeilingDosageAccount] NOCHECK CONSTRAINT [FK_CeilingDosageAccount_FromTimeDefinition];


GO
ALTER TABLE [Sales].[CeilingDosageAccount] NOCHECK CONSTRAINT [FK_CeilingDosageAccount_LastModifiedBy];


GO
ALTER TABLE [Sales].[CeilingDosageAccount] NOCHECK CONSTRAINT [FK_CeilingDosageAccount_ProductDosageForm];


GO
ALTER TABLE [Sales].[CeilingDosageAccount] NOCHECK CONSTRAINT [FK_CeilingDosageAccount_ToTimeDefinition];



