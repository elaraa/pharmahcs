﻿CREATE TABLE [Sales].[CeilingAccountException] (
    [CeilingAccountExceptionId] INT      IDENTITY (1, 1) NOT NULL,
    [AccountId]                 INT      NOT NULL,
    [ProductId]                 INT      NULL,
    [FromTimeId]                INT      NOT NULL,
    [ToTimeId]                  INT      NULL,
    [CreationDate]              DATETIME CONSTRAINT [DF_CeilingAccountException_CreationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]          DATETIME NULL,
    [CreatedById]               INT      NULL,
    [LastModifiedById]          INT      NULL,
    CONSTRAINT [PK_CeilingAccountException] PRIMARY KEY CLUSTERED ([CeilingAccountExceptionId] ASC),
    CONSTRAINT [FK_CeilingAccountException_Account] FOREIGN KEY ([AccountId]) REFERENCES [Shared].[Account] ([AccountId]),
    CONSTRAINT [FK_CeilingAccountException_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_CeilingAccountException_FromTimeDefinition] FOREIGN KEY ([FromTimeId]) REFERENCES [Shared].[TimeDefinition] ([TimeDefinitionId]),
    CONSTRAINT [FK_CeilingAccountException_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_CeilingAccountException_ToTimeDefinition] FOREIGN KEY ([ToTimeId]) REFERENCES [Shared].[TimeDefinition] ([TimeDefinitionId])
);


GO
ALTER TABLE [Sales].[CeilingAccountException] NOCHECK CONSTRAINT [FK_CeilingAccountException_Account];


GO
ALTER TABLE [Sales].[CeilingAccountException] NOCHECK CONSTRAINT [FK_CeilingAccountException_CreatedBy];


GO
ALTER TABLE [Sales].[CeilingAccountException] NOCHECK CONSTRAINT [FK_CeilingAccountException_FromTimeDefinition];


GO
ALTER TABLE [Sales].[CeilingAccountException] NOCHECK CONSTRAINT [FK_CeilingAccountException_LastModifiedBy];


GO
ALTER TABLE [Sales].[CeilingAccountException] NOCHECK CONSTRAINT [FK_CeilingAccountException_ToTimeDefinition];





