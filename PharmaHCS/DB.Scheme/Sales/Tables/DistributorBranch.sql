﻿CREATE TABLE [Sales].[DistributorBranch] (
    [BranchId]         INT            IDENTITY (1, 1) NOT NULL,
    [DistributorId]    INT            NOT NULL,
    [BranchName]       NVARCHAR (MAX) NOT NULL,
    [BranchCode]       NVARCHAR (50)  NULL,
    [CreationDate]     DATETIME       CONSTRAINT [DistributorBranch_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_DistributorBranch] PRIMARY KEY CLUSTERED ([BranchId] ASC),
    CONSTRAINT [FK_DistributorBranch_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_DistributorBranch_Distributor] FOREIGN KEY ([DistributorId]) REFERENCES [Sales].[Distributor] ([DistributorId]),
    CONSTRAINT [FK_DistributorBranch_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [Sales].[DistributorBranch] NOCHECK CONSTRAINT [FK_DistributorBranch_CreatedBy];


GO
ALTER TABLE [Sales].[DistributorBranch] NOCHECK CONSTRAINT [FK_DistributorBranch_Distributor];


GO
ALTER TABLE [Sales].[DistributorBranch] NOCHECK CONSTRAINT [FK_DistributorBranch_LastModifiedBy];







