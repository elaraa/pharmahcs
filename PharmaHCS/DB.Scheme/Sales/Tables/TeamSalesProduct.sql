﻿CREATE TABLE [Sales].[TeamSalesProduct] (
    [TeamSalesProductId] INT      IDENTITY (1, 1) NOT NULL,
    [TeamId]             INT      NOT NULL,
    [ProductId]          INT      NOT NULL,
    [FromDate]           DATE     NOT NULL,
    [ToDate]             DATE     NULL,
    [CreationDate]       DATETIME CONSTRAINT [TeamSalesProduct_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]   DATETIME NULL,
    [CreatedById]        INT      NULL,
    [LastModifiedById]   INT      NULL,
    CONSTRAINT [PK_TeamProduct] PRIMARY KEY CLUSTERED ([TeamSalesProductId] ASC),
    CONSTRAINT [FK_TeamProduct_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_TeamProduct_Product] FOREIGN KEY ([ProductId]) REFERENCES [Shared].[Product] ([ProductId]),
    CONSTRAINT [FK_TeamProduct_Team] FOREIGN KEY ([TeamId]) REFERENCES [Shared].[Team] ([TeamId]),
    CONSTRAINT [FK_TeamSalesProduct_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [Sales].[TeamSalesProduct] NOCHECK CONSTRAINT [FK_TeamProduct_LastModifiedBy];


GO
ALTER TABLE [Sales].[TeamSalesProduct] NOCHECK CONSTRAINT [FK_TeamProduct_Product];


GO
ALTER TABLE [Sales].[TeamSalesProduct] NOCHECK CONSTRAINT [FK_TeamProduct_Team];


GO
ALTER TABLE [Sales].[TeamSalesProduct] NOCHECK CONSTRAINT [FK_TeamSalesProduct_CreatedBy];



