﻿CREATE TABLE [Sales].[MarketShareTerritory] (
    [MarketShareTerritoryId] INT            IDENTITY (1, 1) NOT NULL,
    [MarketShareId]          INT            NOT NULL,
    [TerritoryId]            INT            NOT NULL,
    [CategoryId]             INT            NULL,
    [MarketShare]            DECIMAL (8, 5) CONSTRAINT [DF_MarketShareTerritory_MarketShare] DEFAULT ((0)) NOT NULL,
    [CreationDate]           DATETIME       CONSTRAINT [DF_MarketShareTerritory_CreationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]       DATETIME       NULL,
    [CreatedById]            INT            NULL,
    [LastModifiedById]       INT            NULL,
    CONSTRAINT [PK_MarketShareTerritory] PRIMARY KEY CLUSTERED ([MarketShareTerritoryId] ASC),
    CONSTRAINT [FK_MarketShareTerritory_AccountCategory] FOREIGN KEY ([CategoryId]) REFERENCES [Shared].[AccountCategory] ([CategoryId]),
    CONSTRAINT [FK_MarketShareTerritory_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_MarketShareTerritory_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_MarketShareTerritory_MarketShare] FOREIGN KEY ([MarketShareId]) REFERENCES [Sales].[MarketShare] ([MarketShareId]),
    CONSTRAINT [FK_MarketShareTerritory_Territory] FOREIGN KEY ([TerritoryId]) REFERENCES [Shared].[Territory] ([TerritoryId])
);


GO
ALTER TABLE [Sales].[MarketShareTerritory] NOCHECK CONSTRAINT [FK_MarketShareTerritory_AccountCategory];


GO
ALTER TABLE [Sales].[MarketShareTerritory] NOCHECK CONSTRAINT [FK_MarketShareTerritory_CreatedBy];


GO
ALTER TABLE [Sales].[MarketShareTerritory] NOCHECK CONSTRAINT [FK_MarketShareTerritory_LastModifiedBy];


GO
ALTER TABLE [Sales].[MarketShareTerritory] NOCHECK CONSTRAINT [FK_MarketShareTerritory_MarketShare];


GO
ALTER TABLE [Sales].[MarketShareTerritory] NOCHECK CONSTRAINT [FK_MarketShareTerritory_Territory];





