﻿CREATE TABLE [Sales].[ImportedFileRecord] (
    [FileRecordId]          INT             IDENTITY (1, 1) NOT NULL,
    [FileId]                INT             NOT NULL,
    [DistributorBranchId]   INT             NULL,
    [RawRecord]             NVARCHAR (MAX)  COLLATE Latin1_General_CI_AS NULL,
    [CustomerCode]          NVARCHAR (MAX)  NULL,
    [CustomerName]          NVARCHAR (MAX)  NULL,
    [CustomerAddress]       NVARCHAR (MAX)  NULL,
    [CustomerTerritory]     NVARCHAR (MAX)  NULL,
    [DosageFormCode]        NVARCHAR (MAX)  NULL,
    [DosageFormName]        NVARCHAR (MAX)  NULL,
    [FileQuantity]          NVARCHAR (MAX)  NULL,
    [FileAmount]            NVARCHAR (MAX)  NULL,
    [InvoiceNumber]         NVARCHAR (MAX)  NULL,
    [SalesDate]             DATE            NULL,
    [AccountId]             INT             NULL,
    [DosageFormId]          INT             NULL,
    [Quantity]              DECIMAL (18, 4) NULL,
    [Amount]                DECIMAL (18, 4) NULL,
    [DistributorBranchCode] NVARCHAR (MAX)  NULL,
    [DistributorBranchName] NVARCHAR (MAX)  NULL,
    [CustomerMapped]        BIT             CONSTRAINT [DF_ImportedFileRecord_CustomerMapped_1] DEFAULT ((0)) NOT NULL,
    [ProductMapped]         BIT             CONSTRAINT [DF_ImportedFileRecord_ProductMapped_1] DEFAULT ((0)) NOT NULL,
    [DuplicateRow]          BIT             CONSTRAINT [DF_ImportedFileRecord_DuplicateRow_1] DEFAULT ((0)) NOT NULL,
    [InvalidQty]            BIT             CONSTRAINT [DF_ImportedFileRecord_InvalidQty_1] DEFAULT ((0)) NOT NULL,
    [InvalidRow]            BIT             CONSTRAINT [DF_ImportedFileRecord_InvalidRow_1] DEFAULT ((0)) NOT NULL,
    [AccountToBeCreated]    BIT             CONSTRAINT [DF_ImportedFileRecord_AccountToBeCreated] DEFAULT ((0)) NOT NULL,
    [CreationDate]          DATETIME        CONSTRAINT [ImportedFileRecord_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]      DATETIME        NULL,
    [CreatedById]           INT             NULL,
    [LastModifiedById]      INT             NULL,
    CONSTRAINT [PK_ImportedFileRecord] PRIMARY KEY CLUSTERED ([FileRecordId] ASC),
    CONSTRAINT [FK_ImportedFileRecord_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ImportedFileRecord_DistributorBranch] FOREIGN KEY ([DistributorBranchId]) REFERENCES [Sales].[DistributorBranch] ([BranchId]),
    CONSTRAINT [FK_ImportedFileRecord_ImportedFile] FOREIGN KEY ([FileId]) REFERENCES [Sales].[ImportedFile] ([FileId]),
    CONSTRAINT [FK_ImportedFileRecord_ImportedFileRecord] FOREIGN KEY ([AccountId]) REFERENCES [Shared].[Account] ([AccountId]),
    CONSTRAINT [FK_ImportedFileRecord_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ImportedFileRecord_ProductDosageForm] FOREIGN KEY ([DosageFormId]) REFERENCES [Shared].[ProductDosageForm] ([DosageFormId])
);


GO
ALTER TABLE [Sales].[ImportedFileRecord] NOCHECK CONSTRAINT [FK_ImportedFileRecord_CreatedBy];


GO
ALTER TABLE [Sales].[ImportedFileRecord] NOCHECK CONSTRAINT [FK_ImportedFileRecord_DistributorBranch];


GO
ALTER TABLE [Sales].[ImportedFileRecord] NOCHECK CONSTRAINT [FK_ImportedFileRecord_ImportedFile];


GO
ALTER TABLE [Sales].[ImportedFileRecord] NOCHECK CONSTRAINT [FK_ImportedFileRecord_ImportedFileRecord];


GO
ALTER TABLE [Sales].[ImportedFileRecord] NOCHECK CONSTRAINT [FK_ImportedFileRecord_LastModifiedBy];


GO
ALTER TABLE [Sales].[ImportedFileRecord] NOCHECK CONSTRAINT [FK_ImportedFileRecord_ProductDosageForm];













