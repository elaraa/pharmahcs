﻿CREATE TABLE [Sales].[ImportPlugin] (
    [PluginId]              INT            NOT NULL,
    [PluginName]            NVARCHAR (MAX) NOT NULL,
    [DllPath]               NVARCHAR (MAX) NOT NULL,
    [IsFiles]               BIT            NOT NULL,
    [ClassName]             NVARCHAR (MAX) NOT NULL,
    [TemplatePath]          NVARCHAR (MAX) NOT NULL,
    [RequireTimeDefinition] BIT            CONSTRAINT [DF_ImportPlugin_RequireTimeDefinition] DEFAULT ((1)) NOT NULL,
    [CreationDate]          DATETIME       CONSTRAINT [ImportPlugin_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]      DATETIME       NULL,
    [CreatedById]           INT            NULL,
    [LastModifiedById]      INT            NULL,
    CONSTRAINT [PK_ImportPlugin] PRIMARY KEY CLUSTERED ([PluginId] ASC),
    CONSTRAINT [FK_ImportPlugin_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ImportPlugin_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [Sales].[ImportPlugin] NOCHECK CONSTRAINT [FK_ImportPlugin_CreatedBy];


GO
ALTER TABLE [Sales].[ImportPlugin] NOCHECK CONSTRAINT [FK_ImportPlugin_LastModifiedBy];











