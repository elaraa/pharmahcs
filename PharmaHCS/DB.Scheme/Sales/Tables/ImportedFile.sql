﻿CREATE TABLE [Sales].[ImportedFile] (
    [FileId]                INT            IDENTITY (1, 1) NOT NULL,
    [FileName]              NVARCHAR (MAX) NOT NULL,
    [DistributorId]         INT            NOT NULL,
    [PluginId]              INT            NOT NULL,
    [StatusId]              INT            NOT NULL,
    [ErrorDescription]      NVARCHAR (MAX) NULL,
    [TimeDefinitionId]      INT            NULL,
    [ImportMode]            CHAR (1)       CONSTRAINT [DF_ImportedFile_ImportMode] DEFAULT ('O') NOT NULL,
    [TotalRecords]          INT            CONSTRAINT [DF_ImportedFile_TotalRecords] DEFAULT ((0)) NOT NULL,
    [TotalUnmappedAccounts] INT            CONSTRAINT [DF_ImportedFile_TotalUnmappedAccounts] DEFAULT ((0)) NOT NULL,
    [TotalUnmappedProducts] INT            CONSTRAINT [DF_ImportedFile_TotalUnmappedProducts] DEFAULT ((0)) NOT NULL,
    [TotalDuplicateEntries] INT            CONSTRAINT [DF_ImportedFile_TotalDuplicateEntries] DEFAULT ((0)) NOT NULL,
    [TotalInvalidEntries]   INT            CONSTRAINT [DF_ImportedFile_TotalInvalidEntries] DEFAULT ((0)) NOT NULL,
    [CountryId]             INT            NOT NULL,
    [CreationDate]          DATETIME       CONSTRAINT [ImportedFile_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]      DATETIME       NULL,
    [CreatedById]           INT            NULL,
    [LastModifiedById]      INT            NULL,
    CONSTRAINT [PK_ImportedFile] PRIMARY KEY CLUSTERED ([FileId] ASC),
    CONSTRAINT [FK_DistributorSalesImportFile_TimeDefinition] FOREIGN KEY ([TimeDefinitionId]) REFERENCES [Shared].[TimeDefinition] ([TimeDefinitionId]),
    CONSTRAINT [FK_ImportedFile_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_ImportedFile_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ImportedFile_Distributor] FOREIGN KEY ([DistributorId]) REFERENCES [Sales].[Distributor] ([DistributorId]),
    CONSTRAINT [FK_ImportedFile_ImportedFileStatus] FOREIGN KEY ([StatusId]) REFERENCES [Sales].[ImportedFileStatus] ([StatusId]),
    CONSTRAINT [FK_ImportedFile_ImportPlugin] FOREIGN KEY ([PluginId]) REFERENCES [Sales].[ImportPlugin] ([PluginId]),
    CONSTRAINT [FK_ImportedFile_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [Sales].[ImportedFile] NOCHECK CONSTRAINT [FK_DistributorSalesImportFile_TimeDefinition];


GO
ALTER TABLE [Sales].[ImportedFile] NOCHECK CONSTRAINT [FK_ImportedFile_Country];


GO
ALTER TABLE [Sales].[ImportedFile] NOCHECK CONSTRAINT [FK_ImportedFile_CreatedBy];


GO
ALTER TABLE [Sales].[ImportedFile] NOCHECK CONSTRAINT [FK_ImportedFile_Distributor];


GO
ALTER TABLE [Sales].[ImportedFile] NOCHECK CONSTRAINT [FK_ImportedFile_ImportedFileStatus];


GO
ALTER TABLE [Sales].[ImportedFile] NOCHECK CONSTRAINT [FK_ImportedFile_ImportPlugin];


GO
ALTER TABLE [Sales].[ImportedFile] NOCHECK CONSTRAINT [FK_ImportedFile_LastModifiedBy];






















































































GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'O => Overwrite
A => Append
 E => Overwrite Existing', @level0type = N'SCHEMA', @level0name = N'Sales', @level1type = N'TABLE', @level1name = N'ImportedFile', @level2type = N'COLUMN', @level2name = N'ImportMode';













































































