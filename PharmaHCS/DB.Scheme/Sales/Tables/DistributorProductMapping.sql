﻿CREATE TABLE [Sales].[DistributorProductMapping] (
    [DistributorProductMappingId] INT            IDENTITY (1, 1) NOT NULL,
    [DistributorId]               INT            NOT NULL,
    [DosageFormId]                INT            NOT NULL,
    [DosageFormCode]              NVARCHAR (MAX) NOT NULL,
    [DosageFormName]              NVARCHAR (MAX) NULL,
    [FileId]                      INT            NULL,
    [CreationDate]                DATETIME       CONSTRAINT [DistributorProductMapping_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]            DATETIME       NULL,
    [CreatedById]                 INT            NULL,
    [LastModifiedById]            INT            NULL,
    CONSTRAINT [PK_DistributorProductMapping] PRIMARY KEY CLUSTERED ([DistributorProductMappingId] ASC),
    CONSTRAINT [FK_DistributorProductMapping_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_DistributorProductMapping_Distributor] FOREIGN KEY ([DistributorId]) REFERENCES [Sales].[Distributor] ([DistributorId]),
    CONSTRAINT [FK_DistributorProductMapping_ImportedFile] FOREIGN KEY ([FileId]) REFERENCES [Sales].[ImportedFile] ([FileId]),
    CONSTRAINT [FK_DistributorProductMapping_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_DistributorProductMapping_ProductDosageForm] FOREIGN KEY ([DosageFormId]) REFERENCES [Shared].[ProductDosageForm] ([DosageFormId])
);


GO
ALTER TABLE [Sales].[DistributorProductMapping] NOCHECK CONSTRAINT [FK_DistributorProductMapping_CreatedBy];


GO
ALTER TABLE [Sales].[DistributorProductMapping] NOCHECK CONSTRAINT [FK_DistributorProductMapping_Distributor];


GO
ALTER TABLE [Sales].[DistributorProductMapping] NOCHECK CONSTRAINT [FK_DistributorProductMapping_ImportedFile];


GO
ALTER TABLE [Sales].[DistributorProductMapping] NOCHECK CONSTRAINT [FK_DistributorProductMapping_LastModifiedBy];


GO
ALTER TABLE [Sales].[DistributorProductMapping] NOCHECK CONSTRAINT [FK_DistributorProductMapping_ProductDosageForm];







