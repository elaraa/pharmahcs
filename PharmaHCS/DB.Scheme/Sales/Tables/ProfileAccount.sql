﻿CREATE TABLE [Sales].[ProfileAccount] (
    [ProfileAccountId] INT             IDENTITY (1, 1) NOT NULL,
    [ProfileId]        INT             NOT NULL,
    [AccountId]        INT             NOT NULL,
    [Perccentage]      DECIMAL (18, 6) NOT NULL,
    [ProductId]        INT             NULL,
    [DosageFromId]     INT             NULL,
    [FromDate]         DATE            NOT NULL,
    [ToDate]           DATE            NULL,
    [CreationDate]     DATETIME        CONSTRAINT [ProfileAccount_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME        NULL,
    [CreatedById]      INT             NULL,
    [LastModifiedById] INT             NULL,
    CONSTRAINT [PK_ProfileAccount] PRIMARY KEY CLUSTERED ([ProfileAccountId] ASC),
    CONSTRAINT [FK_ProfileAccount_Account] FOREIGN KEY ([AccountId]) REFERENCES [Shared].[Account] ([AccountId]),
    CONSTRAINT [FK_ProfileAccount_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileAccount_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileAccount_Product] FOREIGN KEY ([ProductId]) REFERENCES [Shared].[Product] ([ProductId]),
    CONSTRAINT [FK_ProfileAccount_ProductDosageForm] FOREIGN KEY ([DosageFromId]) REFERENCES [Shared].[ProductDosageForm] ([DosageFormId]),
    CONSTRAINT [FK_ProfileAccount_TeamProfile] FOREIGN KEY ([ProfileId]) REFERENCES [Shared].[TeamProfile] ([ProfileId])
);


GO
ALTER TABLE [Sales].[ProfileAccount] NOCHECK CONSTRAINT [FK_ProfileAccount_Account];


GO
ALTER TABLE [Sales].[ProfileAccount] NOCHECK CONSTRAINT [FK_ProfileAccount_CreatedBy];


GO
ALTER TABLE [Sales].[ProfileAccount] NOCHECK CONSTRAINT [FK_ProfileAccount_LastModifiedBy];


GO
ALTER TABLE [Sales].[ProfileAccount] NOCHECK CONSTRAINT [FK_ProfileAccount_Product];


GO
ALTER TABLE [Sales].[ProfileAccount] NOCHECK CONSTRAINT [FK_ProfileAccount_ProductDosageForm];


GO
ALTER TABLE [Sales].[ProfileAccount] NOCHECK CONSTRAINT [FK_ProfileAccount_TeamProfile];












GO
CREATE NONCLUSTERED INDEX [ProfileAccount_ProfileId]
    ON [Sales].[ProfileAccount]([ProfileId] ASC)
    INCLUDE([AccountId], [FromDate], [ToDate]);


GO
CREATE NONCLUSTERED INDEX [ProfileAccount_AccountId]
    ON [Sales].[ProfileAccount]([AccountId] ASC)
    INCLUDE([ProfileId]);


GO
CREATE NONCLUSTERED INDEX [IX_ProfileAccount_Profile_Product]
    ON [Sales].[ProfileAccount]([ProfileId] ASC, [ProductId] ASC)
    INCLUDE([AccountId], [Perccentage], [FromDate], [ToDate]);


GO
CREATE NONCLUSTERED INDEX [IX_ProfileAccount_ProductId]
    ON [Sales].[ProfileAccount]([ProductId] ASC)
    INCLUDE([ProfileId], [AccountId], [Perccentage], [FromDate], [ToDate]);

