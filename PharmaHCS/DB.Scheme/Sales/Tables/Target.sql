﻿CREATE TABLE [Sales].[Target] (
    [TargetId]         INT             IDENTITY (1, 1) NOT NULL,
    [TargetTypeId]     INT             NOT NULL,
    [Year]             INT             NOT NULL,
    [TimeDefinitionId] INT             NULL,
    [ProductId]        INT             NULL,
    [DosageFormId]     INT             NULL,
    [AccountId]        INT             NULL,
    [TerritoryId]      INT             NULL,
    [TeamId]           INT             NULL,
    [ProfileId]        INT             NULL,
    [EmployeeId]       INT             NULL,
    [Qty]              DECIMAL (18, 4) NULL,
    [Amount]           DECIMAL (18, 4) NULL,
    [Percentage]       DECIMAL (18, 6) NULL,
    [CountryId]        INT             NOT NULL,
    [CreationDate]     DATETIME        CONSTRAINT [Target_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME        NULL,
    [CreatedById]      INT             NULL,
    [LastModifiedById] INT             NULL,
    CONSTRAINT [PK_Target] PRIMARY KEY CLUSTERED ([TargetId] ASC),
    CONSTRAINT [FK_Target_Account] FOREIGN KEY ([AccountId]) REFERENCES [Shared].[Account] ([AccountId]),
    CONSTRAINT [FK_Target_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_Target_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Target_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Target_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Target_Product] FOREIGN KEY ([ProductId]) REFERENCES [Shared].[Product] ([ProductId]),
    CONSTRAINT [FK_Target_ProductDosageForm] FOREIGN KEY ([DosageFormId]) REFERENCES [Shared].[ProductDosageForm] ([DosageFormId]),
    CONSTRAINT [FK_Target_TargetType] FOREIGN KEY ([TargetTypeId]) REFERENCES [Sales].[TargetType] ([TargetTypeId]),
    CONSTRAINT [FK_Target_Team] FOREIGN KEY ([TeamId]) REFERENCES [Shared].[Team] ([TeamId]),
    CONSTRAINT [FK_Target_TeamProfile] FOREIGN KEY ([ProfileId]) REFERENCES [Shared].[TeamProfile] ([ProfileId]),
    CONSTRAINT [FK_Target_Territory] FOREIGN KEY ([TerritoryId]) REFERENCES [Shared].[Territory] ([TerritoryId]),
    CONSTRAINT [FK_Target_TimeDefinition] FOREIGN KEY ([TimeDefinitionId]) REFERENCES [Shared].[TimeDefinition] ([TimeDefinitionId])
);


GO
ALTER TABLE [Sales].[Target] NOCHECK CONSTRAINT [FK_Target_Account];


GO
ALTER TABLE [Sales].[Target] NOCHECK CONSTRAINT [FK_Target_Country];


GO
ALTER TABLE [Sales].[Target] NOCHECK CONSTRAINT [FK_Target_CreatedBy];


GO
ALTER TABLE [Sales].[Target] NOCHECK CONSTRAINT [FK_Target_Employee];


GO
ALTER TABLE [Sales].[Target] NOCHECK CONSTRAINT [FK_Target_LastModifiedBy];


GO
ALTER TABLE [Sales].[Target] NOCHECK CONSTRAINT [FK_Target_Product];


GO
ALTER TABLE [Sales].[Target] NOCHECK CONSTRAINT [FK_Target_ProductDosageForm];


GO
ALTER TABLE [Sales].[Target] NOCHECK CONSTRAINT [FK_Target_TargetType];


GO
ALTER TABLE [Sales].[Target] NOCHECK CONSTRAINT [FK_Target_Team];


GO
ALTER TABLE [Sales].[Target] NOCHECK CONSTRAINT [FK_Target_TeamProfile];


GO
ALTER TABLE [Sales].[Target] NOCHECK CONSTRAINT [FK_Target_Territory];


GO
ALTER TABLE [Sales].[Target] NOCHECK CONSTRAINT [FK_Target_TimeDefinition];


















GO
CREATE NONCLUSTERED INDEX [Target_Year_Profile_Amount]
    ON [Sales].[Target]([TargetTypeId] ASC, [CountryId] ASC)
    INCLUDE([Year], [ProfileId], [Amount]);


GO
CREATE NONCLUSTERED INDEX [Target_TerritoryId_Amount]
    ON [Sales].[Target]([TargetTypeId] ASC, [Year] ASC, [CountryId] ASC)
    INCLUDE([TerritoryId], [Amount]);


GO
CREATE NONCLUSTERED INDEX [Target_Territory_year_country]
    ON [Sales].[Target]([TargetTypeId] ASC, [Year] ASC, [TerritoryId] ASC, [CountryId] ASC)
    INCLUDE([Amount]);


GO
CREATE NONCLUSTERED INDEX [Target_Profile_Amount]
    ON [Sales].[Target]([TargetTypeId] ASC, [Year] ASC, [CountryId] ASC)
    INCLUDE([ProfileId], [Amount]);


GO
CREATE NONCLUSTERED INDEX [Target_Amount]
    ON [Sales].[Target]([TargetTypeId] ASC, [Year] ASC, [ProfileId] ASC, [CountryId] ASC)
    INCLUDE([Amount]);


GO
CREATE NONCLUSTERED INDEX [IX_Target_TargetTypeId_ProfileId]
    ON [Sales].[Target]([TargetTypeId] ASC, [ProfileId] ASC)
    INCLUDE([Year], [TimeDefinitionId], [ProductId], [TerritoryId], [Qty], [Amount], [CountryId], [CreationDate], [LastModifiedDate], [CreatedById], [LastModifiedById]);


GO
CREATE NONCLUSTERED INDEX [IX_Target_TargetTypeId_Year_TimeDefinitionId_ProductId_CountryId]
    ON [Sales].[Target]([TargetTypeId] ASC, [Year] ASC, [TimeDefinitionId] ASC, [ProductId] ASC, [CountryId] ASC)
    INCLUDE([ProfileId], [Qty]);


GO
CREATE NONCLUSTERED INDEX [IX_Target_TargetTypeId_Year_CountryId]
    ON [Sales].[Target]([TargetTypeId] ASC, [Year] ASC, [CountryId] ASC)
    INCLUDE([TimeDefinitionId], [ProductId], [ProfileId], [Qty]);

