﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 22Sep19
-- Description:	
-- =============================================
CREATE FUNCTION Sales.fn_TargetProductPhasing_Export 
(	
	-- Add the parameters for the function here
	@year int, 
	@countryId int
)
RETURNS TABLE 
AS
RETURN 
(
	select p.ProductId, p.ProductName,td.PeriodName, t.Percentage
	from Shared.Product p
		left join Shared.TimeDefinition td on td.Year = @year
		left join sales.[Target] t on t.ProductId = p.ProductId
			and t.TargetTypeId = 3 /*product phasing*/
			and t.ProductId = p.ProductId
			and t.TimeDefinitionId = td.TimeDefinitionId
	where Active = 1 
	and p.CountryId = @countryId
)