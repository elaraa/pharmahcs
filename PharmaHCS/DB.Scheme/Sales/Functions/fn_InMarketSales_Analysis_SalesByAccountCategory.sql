﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 6 Oct 19
-- Description:	
-- =============================================
CReate FUNCTION [Sales].[fn_InMarketSales_Analysis_SalesByAccountCategory] 
(	
	@countryId int, 
	@year int,
	@userId int,
	@teamId int,
	@productId int,
	@territoryId int,
	@employeeId	int
)
RETURNS TABLE 
AS
RETURN 
(	
	select ac.CategoryName, sum(Qty) Qty, sum(Amount) Amount
	from sales.InMarketSales s with (nolock)
		left join Shared.Account a on s.AccountId = a.AccountId
		join Shared.fn_AccountCategory_GetParentLevelForLeaf(2,@countryId) pac on pac.CategoryId = a.CategoryId
		join Shared.AccountCategory ac on pac.RootCategoryId = ac.CategoryId
	where year(SalesDate) = @year
		and s.CountryId = @countryId
	group by ac.CategoryName
)