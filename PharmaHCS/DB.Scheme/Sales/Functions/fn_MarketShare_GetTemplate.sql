﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 10 Oct 19
-- Description:	
-- =============================================
CREATE FUNCTION sales.fn_MarketShare_GetTemplate 
(	
	-- Add the parameters for the function here
	@year int, 
	@level int,
	@countryId int
)
RETURNS TABLE 
AS
RETURN 
(
		select emty.RootTerritoryName TerritoryName, isnull(imported.MarketShare,emty.MarketShare) MarketShare
		from
		(select distinct RootTerritoryName, null MarketShare
		from Shared.fn_Territory_GetParentLevelForLeaf(@level, @countryId) tl
		)emty left join 
		(
		select tl.RootTerritoryName, sum(mst.MarketShare) MarketShare
		from sales.MarketShare ms
			join sales.MarketShareTerritory mst on ms.MarketShareId = mst.MarketShareId
			join Shared.fn_Territory_GetParentLevelForLeaf(@level, @countryId) tl
				on tl.TerritoryId = mst.TerritoryId
		where ms.CountryId = @countryId and ms.Year = @year
		group by tl.RootTerritoryName
		)imported on emty.RootTerritoryName = imported.RootTerritoryName
)