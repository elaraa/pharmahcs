﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 6 Oct 19
-- Description:	
-- =============================================
CREATE FUNCTION sales.fn_InMarketSales_Analysis_DistributorSales 
(	
	@countryId int, 
	@year int,
	@userId int,
	@teamId int,
	@productId int,
	@territoryId int,
	@employeeId	int
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here	
	select d.DistributorName,  sum(s.Qty) Qty, sum(s.Amount) Amount
	from sales.InMarketSales s with (nolock)
		join sales.Distributor d on d.DistributorId = s.DistributorId
	where year(SalesDate) = @year
		and s.CountryId = @countryId
	group by d.DistributorName

)