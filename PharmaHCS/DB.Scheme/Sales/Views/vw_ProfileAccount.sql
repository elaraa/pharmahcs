﻿







CREATE VIEW [Sales].[vw_ProfileAccount]
as
SELECT      e.ProfileAccountId 
		,	e.ProfileId
		,	tp.ProfileCode 
		,	tp.TeamId Team
		,	e.AccountId
		,	a.AccountCode +' | '+ a.AccountName AccountName
		,	ac.CategoryId
		,	ac.RootCategoryName CategoryName
		,	terr.TerritoryId
		,	terr.TerritoryCode +' | '+ terr.TerritoryName TerritoryName
		,	e.Perccentage
		,	e.ProductId	
		,	p.ProductCode +' | '+ p.ProductName ProductName
		,	e.DosageFromId
		,	e.FromDate
		,	e.ToDate
		, a.CountryId
	, c.CountryCode
	, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
	, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from sales.ProfileAccount e
		left join Shared.Product p on p.ProductId = e.ProductId
		join Shared.Account a on a.AccountId = e.AccountId
		join Shared.TeamProfile tp on tp.ProfileId = e.ProfileId
		join Shared.fn_AccountCategory_GetParentLevelForLeaf(1,null) ac on a.CategoryId = ac.CategoryId
		join Shared.Territory terr on a.TerritoryId = terr.TerritoryId
		LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)
		left join Shared.Country c on a.CountryId = c.CountryId