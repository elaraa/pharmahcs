﻿
CREATE view [Sales].[vw_ImportedFile]
as
SELECT  e.FileId
		, e.DistributorId
		, Sales.Distributor.DistributorCode + ' | ' + Sales.Distributor.DistributorName AS DistributorName
		, e.StatusId
		, Sales.ImportedFileStatus.StatusName
		, e.TimeDefinitionId
		, Shared.TimeDefinition.PeriodName
		, e.ImportMode
		, e.CountryId
		, c.CountryCode
		, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
		, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
		, u.EmployeeName UpdatedByLogonName
FROM  Sales.ImportedFile AS e 
			JOIN Sales.Distributor ON e.DistributorId = Sales.Distributor.DistributorId 
			JOIN Sales.ImportedFileStatus ON e.StatusId = Sales.ImportedFileStatus.StatusId 
			left join Shared.Country c on e.CountryId = c.CountryId
			LEFT OUTER JOIN  Shared.TimeDefinition ON e.TimeDefinitionId = Shared.TimeDefinition.TimeDefinitionId					 
			LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)