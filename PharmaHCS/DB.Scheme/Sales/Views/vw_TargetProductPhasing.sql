﻿
CREATE view [Sales].[vw_TargetProductPhasing]
as
select  isnull(min(tc.TargetId),-1) TargetId, tc.Year, count(distinct tc.ProductId) Products
	, tc.CountryId
	, c.CountryCode
	, max(ISNULL(tc.LastModifiedDate, tc.CreationDate)) LastUpdate
	, max(ISNULL(tc.LastModifiedById, tc.CreatedById)) UpdatedBy
	, max(u.EmployeeName) UpdatedByLogonName
from sales.Target tc
	left join Shared.Country c on tc.CountryId = c.CountryId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(tc.LastModifiedById, tc.CreatedById)
where tc.TargetTypeId  = 3
group by tc.Year, tc.CountryId
	, c.CountryCode