﻿



CREATE view [Sales].[vw_ProfileTerritory]
as
SELECT      e.ProfileTerritoryId 
		,	e.ProfileId
		,	t.ProfileCode +' | '+ t.ProfileName ProfileName
		,	e.TerritoryId
		,	a.TerritoryCode +' | '+ a.TerritoryName TerritoryName
		,	e.Percentage
		,	e.FromDate
		,	e.ToDate
		, a.CountryId
	, c.CountryCode
	, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
	, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from sales.ProfileTerritory e
			join Shared.TeamProfile t on t.ProfileId = e.ProfileId
			join Shared.Territory a on a.TerritoryId = e.TerritoryId
				LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)
	left join Shared.Country c on a.CountryId = c.CountryId