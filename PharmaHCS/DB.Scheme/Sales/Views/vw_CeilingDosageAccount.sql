﻿


Create VIEW [Sales].[vw_CeilingDosageAccount]
as
select  cda.CeilingDosageAccountId
	,	cda.AccountId
	,	act.AccountCode +' | '+ act.AccountName AccountName 
	,	act.TerritoryId 
	,	terr.TerritoryCode +' | '+ terr.TerritoryName TerritoryName
	,	act.Active
	,	act.CategoryId
	,	ca.CategoryCode +' | '+ ca.CategoryName CategoryName 
	,	cda.DosageFormId 
	,	pdf.DosageFormCode +' | '+ pdf.DosageFormName DosageFormName 
	,	cda.CeilingQty
	,	tdfrom.PeriodName FromPeriod
	,	tdto.PeriodName ToPeriod 
	, ISNULL(cda.LastModifiedDate, cda.CreationDate) LastUpdate
	, ISNULL(cda.LastModifiedById, cda.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from sales.CeilingDosageAccount cda
	left JOIN Shared.Account act on cda.AccountId = act.AccountId
	left JOIN Shared.Territory terr on act.TerritoryId = terr.TerritoryId
	left join Shared.AccountCategory ca on ca.CategoryId = act.CategoryId
	join Shared.ProductDosageForm pdf on pdf.DosageFormId = cda.DosageFormId
	join Shared.TimeDefinition tdfrom on tdfrom.TimeDefinitionId = cda.FromTimeId
	left join Shared.TimeDefinition tdto on tdto.TimeDefinitionId = cda.ToTimeId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(cda.LastModifiedById, cda.CreatedById)