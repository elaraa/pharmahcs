﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 24-Feb-2020
-- Description:	
-- =============================================
CREATE PROCEDURE sales.sp_DeleteTarget 
	-- Add the parameters for the stored procedure here
	@targetId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	delete t from sales.Target t
	join sales.Target s on t.TargetTypeId = s.TargetTypeId
		and t.CountryId = s.CountryId
		and t.Year = s.Year
	where s.TargetId =  @targetId
END