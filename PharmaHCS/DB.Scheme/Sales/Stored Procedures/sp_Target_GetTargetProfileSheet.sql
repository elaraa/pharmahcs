﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 30 Sep 19
-- Description:	
-- =============================================
CREATE PROCEDURE [Sales].[sp_Target_GetTargetProfileSheet]
	-- Add the parameters for the stored procedure here
	@employeeId int, 
	@date date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select TeamName 'Team', ManagerName 'Sup', area, productName product,price XFactory, categoryName category
			, Qty_Lastyear2, Amount_Lastyear2
			, Qty_Lastyear1, Amount_Lastyear1
			, Qty_Currentyear, Amount_Currentyear
			, Qty_Currentyear_estimate, Amount_Currentyear_estimate
			, MarketShare, Sales_Office, Line_Sales, Product_Sales
			, null tgt_qty
	from Temp.Target_Sheet t
		join Shared.fn_Security_GetProductsForEmployee(@employeeId, @date) sp on t.ProductId = sp.ProductId
	--OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE')) 

	------declare @year int = year(@date)
	------declare @cyfactorDivider int = month(@date)

	------/*prepare create temp tables*/
	------IF OBJECT_ID('tempdb..#st') IS NOT NULL drop table #st
	------CREATE TABLE #st (pid int, y int, Qty decimal(18,6), Amount decimal(18,6), cid int, terrid int) 

	------CREATE NONCLUSTERED INDEX [IX_st_qty] ON #st ([y]) INCLUDE ([Qty])
	------CREATE NONCLUSTERED INDEX [IX_pid] ON #st ([pid]) INCLUDE ([y],[Qty],[terrid])

	------IF OBJECT_ID('tempdb..#teamTerr') IS NOT NULL drop table #teamTerr
	------CREATE TABLE #teamTerr (profid int, teamid int, terrId int) 

	------IF OBJECT_ID('tempdb..#terrSalesPercent') IS NOT NULL drop table #terrSalesPercent
	------CREATE TABLE #terrSalesPercent (terrId int, TerritorySalesPercentage decimal(18,6)) 

	------IF OBJECT_ID('tempdb..#LineSalesPercent') IS NOT NULL drop table #LineSalesPercent
	------CREATE TABLE #LineSalesPercent (TeamId int,terrId int, LineSalesPercentage decimal(18,6)) 

	------IF OBJECT_ID('tempdb..#LineProdSalesPercent') IS NOT NULL drop table #LineProdSalesPercent
	------CREATE TABLE #LineProdSalesPercent (TeamId int,prodId int, ProdSalesPercentage decimal(18,6)) 

	------IF OBJECT_ID('tempdb..#result') IS NOT NULL drop table #result
	------CREATE TABLE #result (countryId int, teamId int,prodId int, profileId int, terrId int, Qty_Currentyear decimal(18,6), Amount_Currentyear decimal(18,6)
	------	, Qty_Currentyear_estimate decimal(18,6), Amount_Currentyear_estimate decimal(18,6)
	------	, Qty_Lastyear1 decimal(18,6), Amount_Lastyear1 decimal(18,6)
	------	, Qty_Lastyear2 decimal(18,6), Amount_Lastyear2 decimal(18,6)
	------	, MarketShare decimal(18,6)
	------	, Sales_Office decimal(18,6)
	------	, Line_Sales decimal(18,6)
	------	, Product_Sales decimal(18,6)
	
	------	,repid int
	------	,rep nvarchar(max)
	------	,Product nvarchar(50)
	------	,AreaCode nvarchar(max)
	------	,area nvarchar(max)
	------	,XFactory decimal(18,6)
	
	------	) 

	------CREATE NONCLUSTERED INDEX [IX_result_prod] ON #result ([prodId])
	------CREATE NONCLUSTERED INDEX [IX_result_Terr] ON #result (terrId)
	------CREATE NONCLUSTERED INDEX [IX_result_Profile] ON #result (profileId)
	------/*end prepare create temp tables*/


	------insert into #st
	------select df.ProductId,year(SalesDate),sum(df.PackFactor * qty), sum(df.PackFactor * Amount), s.CountryId, a.TerritoryId
	------from sales.InMarketSales s with (nolock)
	------	join shared.ProductDosageForm df on s.DosageFormId = df.DosageFormId
	------	join Shared.Account a on s.AccountId = a.AccountId
	------where exists (select top 1 1 from Shared.fn_Security_GetProductsForEmployee(@employeeId,@date) sp where df.ProductId = sp.ProductId)
	------group by df.ProductId,year(SalesDate), s.CountryId, a.TerritoryId


	------insert into #teamTerr
	------select distinct tmp.ProfileId, tmp.TeamId,isnull(pt.TerritoryId, a.TerritoryId)--, terr.TerritoryId ,terr.TerritoryName
	------from Shared.TeamProfile tmp 
	------	left join sales.ProfileTerritory pt on tmp.ProfileId = pt.ProfileId
	------	left join sales.ProfileAccount pa on tmp.ProfileId = pa.ProfileId
	------	left join shared.Account a on pa.AccountId = a.AccountId
	------	--left join shared.Territory terr on isnull(pt.TerritoryId, a.TerritoryId) = terr.TerritoryId
	------where tmp.Active = 1 and isnull(pt.TerritoryId, a.TerritoryId) is not null
	------	and exists (select top 1 1 from [Shared].[fn_Security_GetProfilesForEmployee](@employeeId, @date) sp where tmp.ProfileId = sp.ProfileId)


	------insert into #terrSalesPercent
	------select s.terrid, sum(s.Qty)/ tsales.qty TerritorySalesPercentage
	------from #st s 
	------	left join (
	------		select y year, sum(Qty) qty
	------		from #st
	------		where y = @year
	------		group by y
	------	)tsales on y = tsales.year
	------group by s.terrid, y, tsales.qty


	------insert into #LineSalesPercent
	------select tp.TeamId,s.terrid, sum(s.Qty)/ tsales.qty TerritorySalesPercentage
	------from #st s 
	------	join sales.TeamSalesProduct tp on s.pid = tp.ProductId 
	------		and  shared.fnCompareDates(@date, @date, tp.FromDate, tp.ToDate) = 1
	------	left join (
	------		select y year, sum(Qty) qty
	------		from #st s
	------			join sales.TeamSalesProduct tp on s.pid = tp.ProductId 
	------			and  shared.fnCompareDates(@date, @date, tp.FromDate, tp.ToDate) = 1
	------		where y = @year
	------		group by y
	------	)tsales on y = tsales.year
	------group by tp.TeamId,s.terrid, y, tsales.qty


	------insert into #LineProdSalesPercent
	------select tp.TeamId,s.pid, sum(s.Qty)/ tsales.qty
	------from #st s 
	------	join sales.TeamSalesProduct tp on s.pid = tp.ProductId 
	------		and  shared.fnCompareDates(@date, @date, tp.FromDate, tp.ToDate) = 1
	------	left join (
	------		select y year, sum(Qty) qty, tp.TeamId
	------		from #st s
	------			join sales.TeamSalesProduct tp on s.pid = tp.ProductId 
	------			and  shared.fnCompareDates(@date, @date, tp.FromDate, tp.ToDate) = 1
	------		where y = @year
	------		group by y, tp.TeamId
	------	)tsales on y = tsales.year and tsales.TeamId = tp.TeamId
	------group by tp.TeamId,s.pid, y, tsales.qty

	------/*result*/
	------insert into #result(countryId, teamId, prodId, profileId, terrId, Qty_Currentyear, Amount_Currentyear, Qty_Currentyear_estimate, Amount_Currentyear_estimate
	------	,Qty_Lastyear1, Amount_Lastyear1, Qty_Lastyear2, Amount_Lastyear2
	------	,MarketShare, Sales_Office, Line_Sales, Product_Sales)
	------select distinct t.CountryId,t.TeamId, tp.ProductId,terr.profid, terr.terrId 
	------, sum(sc.Qty) Qty_Currentyear, sum(sc.Amount) Amount_Currentyear
	------, (sum(sc.Qty)/@cyfactorDivider)*12 Qty_Currentyear_estimate, (sum(sc.Amount)/@cyfactorDivider)*12 Amount_Currentyear_estimate

	------, sum(sl1.Qty) Qty_Lastyear1, sum(sl1.Amount) Amount_Lastyear1
	------, sum(sl2.Qty) Qty_Lastyear2, sum(sl2.Amount) Amount_Lastyear2

	------,mst.MarketShare
	------,terrPercent.TerritorySalesPercentage 'Sales Office'
	------,linePercent.LineSalesPercentage 'Line Sales'
	------,lineProdPercent.ProdSalesPercentage 'Product Sales'

	------from Shared.Team t
	------	join sales.TeamSalesProduct tp on t.TeamId = tp.TeamId
	------	--join Shared.Product p  on tp.ProductId = tp.ProductId
	------	left join #teamTerr terr on tp.TeamId = terr.teamid
	------	left join sales.MarketShare ms on ms.CountryId = t.CountryId
	------		and ms.Year = @year
	------	left join sales.MarketShareTerritory mst on ms.MarketShareId = mst.MarketShareId
	------		and mst.TerritoryId = terr.terrId

	------	left join #st sc on tp.ProductId = sc.pid
	------		and t.CountryId = sc.cid
	------		and sc.y = @year
	------		and sc.terrid = terr.terrId
	------	left join #st sl1 on tp.ProductId = sl1.pid
	------		and t.CountryId = sl1.cid
	------		and sl1.y = @year -1
	------		and sl1.terrid = terr.terrId
	------	left join #st sl2 on tp.ProductId = sl2.pid
	------		and t.CountryId = sl2.cid
	------		and sl2.y = @year-2
	------		and sl2.terrid = terr.terrId
	------	left join #terrSalesPercent terrPercent on terrPercent.terrId = terr.terrId
	------	left join #LineSalesPercent linePercent on linePercent.terrId = terr.terrId
	------		and linePercent.TeamId = terr.teamid
	------	left join #LineProdSalesPercent lineProdPercent on lineProdPercent.prodId = tp.ProductId
	------		and lineProdPercent.TeamId = tp.TeamId
		
	------group by t.CountryId, t.TeamId, tp.ProductId,terr.profid, terr.terrId, mst.MarketShare
	------	,terrPercent.TerritorySalesPercentage
	------	,linePercent.LineSalesPercentage
	------	,lineProdPercent.ProdSalesPercentage

	------update #result set Product = p.ProductName from #result r join Shared.Product p on r.prodId = p.ProductId
	------update #result set AreaCode = t.TerritoryCode, area=t.TerritoryName from #result r join Shared.Territory t on r.terrId = t.TerritoryId
	------update #result set rep = e.EmployeeName, repid = e.EmployeeId
	------from #result r 
	------	join Shared.EmployeeProfile ep on r.profileId = ep.ProfileId 
	------		and Shared.fnCompareDates(@date, @date, ep.FromDate, ep.ToDate) = 1
	------	join hr.Employee e on ep.EmployeeId = e.EmployeeId
	------update #result set XFactory = p.Price 
	------from #result r 
	------	join Sales.ProductPrice p on r.prodId = p.ProductId
	------		and p.AccountId is null and p.AccountCategoryId is null and p.DistributorId is null and p.TerritoryId is null
	------		and Shared.fnCompareDates(@date, @date, p.FromDate, p.ToDate)=1

	------select distinct TeamName,/*upperManager.EmployeeName PM,*/directManager.EmployeeName Sup,rep,Product,XFactory, AreaCode, area
	------	, Qty_Lastyear2, Amount_Lastyear2
	------	, Qty_Lastyear1, Amount_Lastyear1
	------	, Qty_Currentyear, Amount_Currentyear
	------	, Qty_Currentyear_estimate, Amount_Currentyear_estimate
	------	, Sales_Office
	------	, MarketShare
	------	, Line_Sales
	------	, Product_Sales
	------	, null tgt_qty
	------from #result r
	------	join Shared.Team t on r.teamId = t.TeamId
	------	left join hr.EmployeeHistory eh_directManager on r.repid = eh_directManager.EmployeeId
	------		and Shared.fnCompareDates(@date, @date, eh_directManager.FromDate, eh_directManager.ToDate) = 1
	------	left join hr.Employee directManager on eh_directManager.ManagerId = directManager.EmployeeId

		--left join hr.EmployeeHistory eh_upperManager on directManager.EmployeeId = eh_upperManager.ManagerId
		--	and Shared.fnCompareDates(@date, @date, eh_upperManager.FromDate, eh_upperManager.ToDate) = 1
		--left join hr.Employee upperManager on eh_upperManager.ManagerId = upperManager.EmployeeId
END