﻿-- =============================================
-- Author:		Nader Ghattas
-- Create date: 13 Jan 2020
-- Description:	
-- =============================================
CREATE PROCEDURE [Sales].[sp_UploadProfileAccount] 
	-- Add the parameters for the stored procedure here
	@PeriodId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    delete from temp.uploadProfileAccount where ProfileCode is null and AccountId is null

	if exists (
	select p.TeamId,AccountId
		from Temp.uploadProfileAccount z
			join Shared.TeamProfile p on ltrim(rtrim(z.ProfileCode)) = ltrim(rtrim(p.ProfileCode))
				and p.Active = 1
		group by p.TeamId,AccountId
		having sum(cast(Percentage as decimal(18,2))) > 1.0
			or sum(cast(Percentage as decimal(18,2))) < 1.0
	) 
	begin 

		select p.TeamId,AccountId, sum(cast(Percentage as decimal(18,2))) TotalPercentage
			, case 
				when sum(cast(Percentage as decimal(18,2))) > 1.0 then 'Percentage over 100%'
				else 'Percentage under 100%'
				end 'Error'
		from Temp.uploadProfileAccount z
			join Shared.TeamProfile p on ltrim(rtrim(z.ProfileCode)) = ltrim(rtrim(p.ProfileCode))
				and p.Active = 1
		group by p.TeamId,AccountId
		having sum(cast(Percentage as decimal(18,2))) > 1.0
			or sum(cast(Percentage as decimal(18,2))) < 1.0

	end
	else
	begin
		---delete from upload where account with profile with percentage th		
		delete z
		from temp.uploadProfileAccount z
			join Shared.TimeDefinition td on TimeDefinitionId = @PeriodId
			join Shared.TeamProfile p on ltrim(rtrim(z.ProfileCode)) = ltrim(rtrim(p.ProfileCode))
			join sales.ProfileAccount a on z.AccountId = a.AccountId
				and Shared.fnCompareDates(td.FromDate, td.ToDate,a.FromDate, a.ToDate) = 1
				and p.ProfileId = a.ProfileId
				and a.Perccentage = cast (z.Percentage as decimal(18,2))
		---close records for the same account with different profile or different percentage
		update a set
		ToDate = DATEADD(day,-1,td.FromDate),
		LastModifiedDate = getdate()
		from temp.uploadProfileAccount z
			join Shared.TimeDefinition td on TimeDefinitionId = @PeriodId
			join Shared.TeamProfile p on ltrim(rtrim(z.ProfileCode)) = ltrim(rtrim(p.ProfileCode))
			join Shared.TeamProfile  tp on p.TeamId = tp.TeamId and tp.Active = 1
			join sales.ProfileAccount a on z.AccountId = a.AccountId
				and Shared.fnCompareDates(td.FromDate, td.ToDate,a.FromDate, a.ToDate) = 1
				and (
					(p.ProfileId <> a.ProfileId or a.Perccentage <> cast (z.Percentage as decimal(18,2)))
					and (tp.ProfileId = a.ProfileId )
					)

		delete from sales.ProfileAccount where ToDate < FromDate

		--insert data to profileaccount table
		insert into sales.ProfileAccount(ProfileId, AccountId, Perccentage, FromDate, CreationDate)
		select p.ProfileId, a.AccountId, cast(z.Percentage as decimal(18,2)),td.FromDate,getdate()
		from temp.uploadProfileAccount z
			join Shared.Account a on z.AccountId = a.AccountId
			join Shared.TeamProfile p on ltrim(rtrim(z.ProfileCode)) = ltrim(rtrim(p.ProfileCode))
				and p.Active = 1
			join Shared.TimeDefinition td on TimeDefinitionId = @PeriodId
	end
END