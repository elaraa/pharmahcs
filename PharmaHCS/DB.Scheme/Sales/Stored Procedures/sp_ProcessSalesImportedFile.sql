﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 13-Aug-2019
-- Description:	
-- =============================================
CREATE PROCEDURE [Sales].[sp_ProcessSalesImportedFile]
	-- Add the parameters for the stored procedure here
	@fileId int,
	@userId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- 1. map distributor
	update r set
	DistributorBranchId = db.BranchId
	from [Sales].ImportedFileRecord r 
		join [Sales].[DistributorBranch] db on (r.DistributorBranchCode = db.BranchCode or r.DistributorBranchName = db.BranchName)
		join [Sales].ImportedFile f on r.FileId = f.FileId and f.FileId = @fileId
			and db.DistributorId = f.DistributorId
	-- 2. map customer
	update r set
	AccountId = dam.AccountId,
	CustomerMapped = 1
	from [Sales].ImportedFileRecord r 
		join [Sales].ImportedFile f on r.FileId = f.FileId and f.FileId = @fileId
		join [Sales].DistributorAccountMapping dam on dam.DistributorId = f.DistributorId
			and ( rtrim(ltrim(dam.AccountCode)) = rtrim(ltrim(r.CustomerCode)) /*or dam.AccountName = r.CustomerName or r.CustomerAddress = dam.AccountAddress */)		
	where CustomerMapped = 0 and r.AccountId is null
	-- 3. map product
	update r set
	DosageFormId = dpm.DosageFormId,
	ProductMapped = 1
	from [Sales].ImportedFileRecord r 
		join [Sales].ImportedFile f on r.FileId = f.FileId and f.FileId = @fileId
		join [Sales].DistributorProductMapping dpm on dpm.DistributorId = f.DistributorId
			and ( rtrim(ltrim(dpm.DosageFormCode)) = rtrim(ltrim(r.DosageFormCode)) /*or dpm.DosageFormName = r.DosageFormName*/ )
	where ProductMapped = 0 and r.DosageFormId is null
	-- 4. check qty
	update [Sales].ImportedFileRecord set
	Quantity = TRY_CONVERT(decimal,FileQuantity),
	InvalidQty = 0
	where FileId  = @fileId
	and Quantity is null
	and InvalidQty = 1
	-- 5. check qty
	update Sales.ImportedFileRecord set InvalidQty = 1 where FileId = @fileId and Quantity is null

	update [Sales].ImportedFileRecord set
	InvalidRow = case when (InvalidQty = 1 or CustomerMapped = 0 or ProductMapped = 0 ) then 1 else 0 end
	where FileId  = @fileId

	/*
	update main table regarding the records
	*/
	update f set
		TotalRecords = isnull(fr.TotalReccords,0),
		TotalInvalidEntries = isnull(fr.TotalInvalidRecords,0),
		TotalUnmappedAccounts = isnull(fr.TotalUnmappedCustomers,0),
		TotalUnmappedProducts = isnull(fr.TotalUnmappedProducts,0),
		TotalDuplicateEntries = isnull(fr.TotalDuplicates,0)
	from [Sales].ImportedFile f
		left join (
				select FileId, count(*) TotalReccords, sum(cast(InvalidRow as int)) TotalInvalidRecords, 
					count(distinct case when customermapped = 0 then customerCode end)TotalUnmappedCustomers,
					count(distinct case when ProductMapped = 0 then DosageFormCode end)TotalUnmappedProducts,
					sum(cast(DuplicateRow as int)) TotalDuplicates
				from [Sales].ImportedFileRecord  where FileId = @fileId
				group by FileId
			) fr on f.FileId = fr.FileId
	where f.FileId = @fileId
	-- 6. if all records are ok, move to sales
	if(exists(select 1 from [Sales].ImportedFileRecord where FileId = @fileId and (CustomerMapped = 0 or ProductMapped = 0 or InvalidQty = 1 or InvalidRow = 1 ) ))
	begin 
		select 0 --N'UnderValidation'
		return
	end
	/*
	insert records into sales
	*/
	exec [Sales].sp_PushSalesImport @fileId, @userId
    -- Insert statements for procedure here
	select 1--N'Completed'
END