﻿CREATE TABLE [Staging].[DistributorAccountDosagePercentage] (
    [DistributorId]              INT             NOT NULL,
    [AccountId]                  INT             NULL,
    [DosageFormId]               INT             NOT NULL,
    [CountryId]                  INT             NOT NULL,
    [TimeDefinitionId]           INT             NULL,
    [DistributorSalesPercentage] DECIMAL (38, 6) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_DistributorAccountDosagePercentage]
    ON [Staging].[DistributorAccountDosagePercentage]([DistributorId] ASC);

