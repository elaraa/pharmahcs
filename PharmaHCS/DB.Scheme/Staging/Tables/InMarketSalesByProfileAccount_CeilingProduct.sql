﻿CREATE TABLE [Staging].[InMarketSalesByProfileAccount_CeilingProduct] (
    [ProfileId]        INT             NOT NULL,
    [TeamId]           INT             NOT NULL,
    [ProductId]        INT             NOT NULL,
    [AccountId]        INT             NULL,
    [DosageFormId]     INT             NOT NULL,
    [CountryId]        INT             NOT NULL,
    [TimeDefinitionId] INT             NULL,
    [Qty]              DECIMAL (38, 6) NULL,
    [RemainingQty]     DECIMAL (38, 6) NULL
);

