﻿CREATE TABLE [Staging].[InMarketSalesIncludingCeilingQty] (
    [AccountId]        INT             NULL,
    [DosageFormId]     INT             NOT NULL,
    [CountryId]        INT             NOT NULL,
    [TimeDefinitionId] INT             NULL,
    [Qty]              DECIMAL (38, 4) NULL,
    [RemainingQty]     DECIMAL (38, 4) NULL
);

