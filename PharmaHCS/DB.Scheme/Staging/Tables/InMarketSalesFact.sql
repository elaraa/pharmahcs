﻿CREATE TABLE [Staging].[InMarketSalesFact] (
    [CountryId]        INT             NOT NULL,
    [DistributorId]    INT             NOT NULL,
    [AccountId]        INT             NOT NULL,
    [TerritoryId]      INT             NULL,
    [ProductId]        INT             NOT NULL,
    [DosageFormId]     INT             NOT NULL,
    [TimeDefinitionId] INT             NOT NULL,
    [ProfileId]        INT             NOT NULL,
    [TeamId]           INT             NOT NULL,
    [SharePercentage]  DECIMAL (18, 6) NOT NULL,
    [Qty]              DECIMAL (38, 6) NOT NULL,
    [ShareType]        NVARCHAR (50)   NOT NULL,
    [Amount]           DECIMAL (18, 6) NULL
);



