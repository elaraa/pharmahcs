﻿CREATE TABLE [HR].[VacationType] (
    [VacationTypeId]     INT            NOT NULL,
    [VacationTypeName]   NVARCHAR (MAX) NOT NULL,
    [NoticePeriodPerDay] INT            NOT NULL,
    [CountryId]          INT            NULL,
    [CreationDate]       DATETIME       CONSTRAINT [VacationType_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]   DATETIME       NULL,
    [CreatedById]        INT            NULL,
    [LastModifiedById]   INT            NULL,
    CONSTRAINT [PK_VacationType] PRIMARY KEY CLUSTERED ([VacationTypeId] ASC),
    CONSTRAINT [FK_VacationType_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_VacationType_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VacationType_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [HR].[VacationType] NOCHECK CONSTRAINT [FK_VacationType_Country];


GO
ALTER TABLE [HR].[VacationType] NOCHECK CONSTRAINT [FK_VacationType_CreatedBy];


GO
ALTER TABLE [HR].[VacationType] NOCHECK CONSTRAINT [FK_VacationType_LastModifiedBy];





