﻿CREATE TABLE [HR].[Department] (
    [DepartmentId]          INT            NOT NULL,
    [DepartmentName]        NVARCHAR (MAX) NOT NULL,
    [DepartmentCode]        NVARCHAR (50)  NULL,
    [DepartmentDescription] NVARCHAR (MAX) NULL,
    [IsCRMDepartment]       BIT            NOT NULL,
    [CountryId]             INT            NULL,
    [CreationDate]          DATETIME       CONSTRAINT [Department_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]      DATETIME       NULL,
    [CreatedById]           INT            NULL,
    [LastModifiedById]      INT            NULL,
    CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED ([DepartmentId] ASC),
    CONSTRAINT [FK_Department_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_Department_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Department_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [HR].[Department] NOCHECK CONSTRAINT [FK_Department_Country];


GO
ALTER TABLE [HR].[Department] NOCHECK CONSTRAINT [FK_Department_CreatedBy];


GO
ALTER TABLE [HR].[Department] NOCHECK CONSTRAINT [FK_Department_LastModifiedBy];







