﻿CREATE TABLE [HR].[EmployeeHistory] (
    [EmployeeHistoryId] INT                 IDENTITY (1, 1) NOT NULL,
    [EmployeeId]        INT                 NOT NULL,
    [JobTitleId]        INT                 NOT NULL,
    [DepartmentId]      INT                 NOT NULL,
    [ManagerId]         INT                 NULL,
    [OrganizationNode]  [sys].[hierarchyid] NULL,
    [OrganizationLevel] AS                  ([OrganizationNode].[GetLevel]()),
    [FromDate]          DATE                NOT NULL,
    [ToDate]            DATE                NULL,
    [CountryId]         INT                 NULL,
    [TeamId]            INT                 NULL,
    [CanVisit]          BIT                 NULL,
    [CreationDate]      DATETIME            CONSTRAINT [EmployeeHistory_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]  DATETIME            NULL,
    [CreatedById]       INT                 NULL,
    [LastModifiedById]  INT                 NULL,
    CONSTRAINT [PK_EmployeeHistory] PRIMARY KEY CLUSTERED ([EmployeeHistoryId] ASC),
    CONSTRAINT [FK_EmployeeHistory_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_EmployeeHistory_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_EmployeeHistory_Department] FOREIGN KEY ([DepartmentId]) REFERENCES [HR].[Department] ([DepartmentId]),
    CONSTRAINT [FK_EmployeeHistory_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_EmployeeHistory_JobTitle] FOREIGN KEY ([JobTitleId]) REFERENCES [HR].[JobTitle] ([JobTitleId]),
    CONSTRAINT [FK_EmployeeHistory_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_EmployeeHistory_Manager] FOREIGN KEY ([ManagerId]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_EmployeeHistory_Team] FOREIGN KEY ([TeamId]) REFERENCES [Shared].[Team] ([TeamId])
);


GO
ALTER TABLE [HR].[EmployeeHistory] NOCHECK CONSTRAINT [FK_EmployeeHistory_Country];


GO
ALTER TABLE [HR].[EmployeeHistory] NOCHECK CONSTRAINT [FK_EmployeeHistory_CreatedBy];


GO
ALTER TABLE [HR].[EmployeeHistory] NOCHECK CONSTRAINT [FK_EmployeeHistory_Department];


GO
ALTER TABLE [HR].[EmployeeHistory] NOCHECK CONSTRAINT [FK_EmployeeHistory_Employee];


GO
ALTER TABLE [HR].[EmployeeHistory] NOCHECK CONSTRAINT [FK_EmployeeHistory_JobTitle];


GO
ALTER TABLE [HR].[EmployeeHistory] NOCHECK CONSTRAINT [FK_EmployeeHistory_LastModifiedBy];


GO
ALTER TABLE [HR].[EmployeeHistory] NOCHECK CONSTRAINT [FK_EmployeeHistory_Manager];


GO
ALTER TABLE [HR].[EmployeeHistory] NOCHECK CONSTRAINT [FK_EmployeeHistory_Team];












GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The depth of the employee in the corporate hierarchy.', @level0type = N'SCHEMA', @level0name = N'HR', @level1type = N'TABLE', @level1name = N'EmployeeHistory', @level2type = N'COLUMN', @level2name = N'OrganizationLevel';


GO
CREATE NONCLUSTERED INDEX [IX_EmployeeHistory_ManagerId]
    ON [HR].[EmployeeHistory]([ManagerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_EmployeeHistory_EmployeeId_FromToTeam]
    ON [HR].[EmployeeHistory]([EmployeeId] ASC)
    INCLUDE([FromDate], [ToDate], [TeamId]);

