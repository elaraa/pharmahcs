﻿CREATE TABLE [HR].[JobTitle] (
    [JobTitleId]          INT            NOT NULL,
    [JobTitledCode]       NVARCHAR (50)  NULL,
    [JobTitleName]        NVARCHAR (MAX) NOT NULL,
    [JobTitleDescription] NVARCHAR (MAX) NULL,
    [CountryId]           INT            NULL,
    [CreationDate]        DATETIME       CONSTRAINT [JobTitle_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]    DATETIME       NULL,
    [CreatedById]         INT            NULL,
    [LastModifiedById]    INT            NULL,
    CONSTRAINT [PK_JobTitle] PRIMARY KEY CLUSTERED ([JobTitleId] ASC),
    CONSTRAINT [FK_JobTitle_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_JobTitle_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_JobTitle_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [HR].[JobTitle] NOCHECK CONSTRAINT [FK_JobTitle_Country];


GO
ALTER TABLE [HR].[JobTitle] NOCHECK CONSTRAINT [FK_JobTitle_CreatedBy];


GO
ALTER TABLE [HR].[JobTitle] NOCHECK CONSTRAINT [FK_JobTitle_LastModifiedBy];







