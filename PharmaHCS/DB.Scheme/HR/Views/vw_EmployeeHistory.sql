﻿




CREATE VIEW [HR].[vw_EmployeeHistory]
as
select  e.EmployeeHistoryId
	, e.EmployeeId
	, u1.EmployeeCode +' | '+ u1.EmployeeName EmployeeName 
	, e.JobTitleId
	, j.JobTitledCode +' | '+ j.JobTitleName JobTitleName 
	, e.DepartmentId
	, d.DepartmentCode +' | '+ d.DepartmentName DepartmentName 
	, ManagerId
	, u1.EmployeeCode +' | '+ u1.EmployeeName ManagerName 
	, e.OrganizationNode
	, e.OrganizationLevel
	, e.FromDate
	, e.ToDate
	, e.TeamId
	, t.TeamCode +' | '+ t.TeamName TeamName 
	, e.CountryId
	, c.CountryCode
	, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
	, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from HR.EmployeeHistory e
	left join Shared.Team t on t.TeamId = e.TeamId
	left join HR.JobTitle j on j.JobTitleId = e.JobTitleId
	left join HR.Department d on d.DepartmentId = e.DepartmentId
	left join HR.Employee u1 on u1.EmployeeId = e.ManagerId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)
	left join Shared.Country c on e.CountryId = c.CountryId