﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 3 Oct 19
-- Description:	Refresh All Views
-- =============================================
CREATE PROCEDURE [App].[sp_RefreshAllViews] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @sqlcmd NVARCHAR(MAX) = ''
	SELECT  @sqlcmd = @sqlcmd +  'EXEC sp_refreshview ''' + s.name+'.' +so.name + ''';
	' 
	FROM sys.objects AS so 
		join sys.schemas s on so.schema_id = s.schema_id
	WHERE so.type = 'V' 

	print @sqlcmd

	EXEC(@sqlcmd)
END