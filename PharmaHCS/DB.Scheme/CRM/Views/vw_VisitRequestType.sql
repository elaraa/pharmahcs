﻿


create VIEW [CRM].[vw_VisitRequestType]
as
select  vrt.VisitRequestTypeId
	,	vrt.VisitRequestTypeName
	,	vrt.Active
	,	ISNULL(vrt.LastModifiedDate, vrt.CreationDate) LastUpdate
	,	ISNULL(vrt.LastModifiedById, vrt.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from CRM.VisitRequestType vrt
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(vrt.LastModifiedById, vrt.CreatedById)