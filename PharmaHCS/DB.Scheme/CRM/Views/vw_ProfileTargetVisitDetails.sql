﻿
CREATE view [CRM].[vw_ProfileTargetVisitDetails]
as
select ProfileTargetDetailId, ProfileTargetId
	, tvd.AccountId
	, isnull(d.DoctorName,a.AccountCode +' | '+ a.AccountName) AccountName
	, act.AccountTypeId
	, act.AccountTypeCode +' | '+ act.AccountTypeName AccountTypeName
	, tvd.DoctorId
	, s.SpecialtyId
	, s.SpecialtyCode
	, a.Address
	, t.TerritoryId
	, t.TerritoryCode+' | '+t.TerritoryName TerritoryName
	, tvd.PotentialId
	, p.PotentialCode
	, Frequency
from crm.ProfileTargetVisitDetail tvd 
	join Shared.Account a on tvd.AccountId = a.AccountId
	join Shared.AccountType act on a.AccountTypeId = act.AccountTypeId
	join Shared.Territory t on a.TerritoryId = t.TerritoryId
	join crm.Potential p on tvd.PotentialId = p.PotentialId
	left join crm.Doctor d on tvd.DoctorId = d.DoctorId
	left join crm.Specialty s on d.SpecialtyId = s.SpecialtyId