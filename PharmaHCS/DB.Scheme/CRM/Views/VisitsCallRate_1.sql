﻿CREATE view CRM.VisitsCallRate
as
	select t.ProfileTargetId
	, e.EmployeeId, e.EmployeeCode+' | '+e.EmployeeName EmployeeName
	, t.ProfileId, tp.ProfileCode+' | '+tp.ProfileName ProfileName
	, t.CountryId
	, t.TimeDefinitionId, td.PeriodName, td.Year 
	, tlist.TargetVisits
	, v.VisitsCount
	, cast (v.VisitsCount as decimal(18,5)) / cast (tlist.TargetVisits as decimal(18,5)) AccountsCallRate

from CRM.ProfileTargetVisit t 
	join Shared.TimeDefinition td on t.TimeDefinitionId = td.TimeDefinitionId
	join Shared.EmployeeProfile ep on t.ProfileId = ep.ProfileId
		and Shared.fnCompareDates(td.FromDate, td.ToDate, ep.FromDate, ep.ToDate) = 1
	join hr.Employee e on ep.EmployeeId = e.EmployeeId
	join Shared.TeamProfile tp on t.ProfileId = tp.ProfileId
	left join (
		select ProfileTargetId, sum(Frequency) TargetVisits
		from crm.ProfileTargetVisitDetail
		group by ProfileTargetId
	) tlist on t.ProfileTargetId = tlist.ProfileTargetId
	left join (
	select EmployeeId, td.TimeDefinitionId, count(VisitId) VisitsCount
	from crm.Visit v
		join Shared.TimeDefinition td on Shared.fnCompareDates(v.VisitDate, v.VisitDate, td.FromDate, td.ToDate) = 1
	group by EmployeeId, td.TimeDefinitionId
	)v on ep.EmployeeId = v.EmployeeId and t.TimeDefinitionId = v.TimeDefinitionId