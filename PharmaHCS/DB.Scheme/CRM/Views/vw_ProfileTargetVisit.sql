﻿CREATE view CRM.vw_ProfileTargetVisit
as
select t.ProfileTargetId
	, t.TimeDefinitionId
	, td.PeriodName
	, td.Year
	, t.ProfileId 
	, tp.ProfileCode
	, e.EmployeeId
	, e.EmployeeCode + ' | '+ e.EmployeeName EmployeeName 
	,Case 
		when lower(ManagerResponse) = 'd' then 'Draft' 
		when lower(ManagerResponse) = 'p' then 'Pending Approval'
		when lower(ManagerResponse) = 'a' then 'Approved'
		when lower(ManagerResponse) = 'r' then 'Rejected'
	END as [TargetStatus]
	,ManagerId
	,m.EmployeeCode + ' | '+ m.EmployeeName ManagerName
	, targetDetails.TargetAccountsCount
	, targetDetails.TargetDoctorsCount
	, targetDetails.TargetVisits
	, 0.1 EstimatedCallRate
	, tp.AvgCallRate
	, tp.ListTargetDoctorsCount
	, t.CountryId
	, c.CountryCode
	, ISNULL(t.LastModifiedDate, t.CreationDate) LastUpdate
	, ISNULL(t.LastModifiedById, t.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName

from crm.ProfileTargetVisit t
	join Shared.TimeDefinition td on t.TimeDefinitionId = td.TimeDefinitionId
	join Shared.TeamProfile tp on t.ProfileId = tp.ProfileId
	join Shared.EmployeeProfile ep on tp.ProfileId = ep.ProfileId
		and shared.fnCompareDates(td.FromDate, td.ToDate, ep.FromDate, ep.ToDate) = 1
	join HR.Employee e on ep.EmployeeId = e.EmployeeId
	left join HR.Employee m on t.ManagerId = m.EmployeeId
	left join (
		select ProfileTargetId, count(AccountId) TargetAccountsCount, count(DoctorId) TargetDoctorsCount, sum(Frequency) TargetVisits
		from crm.ProfileTargetVisitDetail
		group by ProfileTargetId
	) targetDetails on t.ProfileTargetId = targetDetails.ProfileTargetId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(t.LastModifiedById, t.CreatedById)
	left join Shared.Country c on t.CountryId = c.CountryId