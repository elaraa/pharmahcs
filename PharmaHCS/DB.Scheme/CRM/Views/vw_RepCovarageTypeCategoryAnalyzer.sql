﻿Create VIEW [CRM].[vw_RepCovarageTypeCategoryAnalyzer]
as
SELECT        tp.ProfileId, tp.ProfileCode, ep.EmployeeId, e.EmployeeName, ac.CategoryId, ac.CategoryName, act.AccountTypeId, act.AccountTypeName, td.TimeDefinitionId, td.PeriodName, COUNT(DISTINCT ptvd.AccountId) 
                         AS VisitsAccountsTarget, COUNT(DISTINCT v.AccountId) AS VisitsAccounts
FROM            Shared.TeamProfile AS tp INNER JOIN
                         Shared.EmployeeProfile AS ep ON ep.ProfileId = tp.ProfileId INNER JOIN
                         HR.Employee AS e ON e.EmployeeId = ep.EmployeeId INNER JOIN
                         Shared.TimeDefinition AS td ON Shared.fnCompareDates(td.FromDate, td.ToDate, ep.FromDate, ep.ToDate) = 1 LEFT OUTER JOIN
                         CRM.ProfileTargetVisit AS ptv ON ptv.ProfileId = tp.ProfileId AND ptv.TimeDefinitionId = td.TimeDefinitionId LEFT OUTER JOIN
                         CRM.ProfileTargetVisitDetail AS ptvd ON ptvd.ProfileTargetId = ptv.ProfileTargetId LEFT OUTER JOIN
                         Shared.Account AS a ON a.AccountId = ptvd.AccountId LEFT OUTER JOIN
                         Shared.AccountCategory AS ac ON ac.CategoryId = a.CategoryId LEFT OUTER JOIN
                         Shared.AccountType AS act ON act.AccountTypeId = a.AccountTypeId LEFT OUTER JOIN
                         CRM.Visit AS v ON v.EmployeeId = e.EmployeeId AND v.VisitDate BETWEEN td.FromDate AND td.ToDate AND v.IsFalseVisit = 0 LEFT OUTER JOIN
                         Shared.Account AS va ON va.AccountId = v.AccountId LEFT OUTER JOIN
                         Shared.AccountCategory AS vac ON vac.CategoryId = va.CategoryId AND vac.CategoryId = ac.CategoryId LEFT OUTER JOIN
                         Shared.AccountType AS vact ON vact.AccountTypeId = va.AccountTypeId AND vact.AccountTypeId = act.AccountTypeId
GROUP BY tp.ProfileId, tp.ProfileCode, ep.EmployeeId, e.EmployeeName, ac.CategoryId, ac.CategoryName, act.AccountTypeId, act.AccountTypeName, td.TimeDefinitionId, td.PeriodName