﻿





CREATE VIEW [CRM].[vw_Specialty]
as
select  s.SpecialtyId
	,	s.SpecialtyCode
	,	s.SpecialtyName
	,	isnull(pcc.ProductCount,0) ProductCount
	,	s.CountryId
	,	c.CountryCode
	,	ISNULL(s.LastModifiedDate, s.CreationDate) LastUpdate
	,	ISNULL(s.LastModifiedById, s.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from CRM.Specialty s	
	left join (select distinct count(ProductId) ProductCount, SpecialtyId from CRM.ProductSpecialty group by SpecialtyId) pcc
		on s.SpecialtyId = pcc.SpecialtyId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(s.LastModifiedById, s.CreatedById)
	left join Shared.Country c on s.CountryId = c.CountryId