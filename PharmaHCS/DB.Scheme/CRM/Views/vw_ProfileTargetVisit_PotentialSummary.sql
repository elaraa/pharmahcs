﻿create view crm.vw_ProfileTargetVisit_PotentialSummary
as
select 
	t.ProfileTargetId
	, dp.PotentialId
	, dp.PotentialCode
	, dp.PotentialName
	, sum(t.Frequency) TargetVisit
	, count(distinct t.DoctorId) TargetDoctors
from crm.ProfileTargetVisitDetail t
	join crm.Doctor d on t.DoctorId = d.DoctorId
	join crm.Potential dp on d.PotentialId = dp.PotentialId
group by t.ProfileTargetId
	, dp.PotentialId
	, dp.PotentialCode
	, dp.PotentialName