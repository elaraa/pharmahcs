﻿




CREATE view [CRM].[vw_VisitsPerDate]
as
select isnull(cast(VisitDate as date),getdate()) VisitDate, v.EmployeeId, e.EmployeeCode+' | '+e.EmployeeName EmployeeName, v.CountryId
	, count(distinct v.AccountId) AccountsCount, count(distinct DoctorId) DoctorsCount
	, count(distinct a.TerritoryId) TerritoriesCount
from crm.Visit v
	left join crm.VisitDoctor vd on v.VisitId = vd.VisitId
	join Shared.Account a on v.AccountId = a.AccountId
	join hr.Employee e on v.EmployeeId = e.EmployeeId
group by isnull(cast(VisitDate as date),getdate()), v.EmployeeId, e.EmployeeCode+' | '+e.EmployeeName, v.CountryId