﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 25 Nov19
-- Description:	
-- =============================================
CREATE PROCEDURE CRM.sp_RepTargetCreate 
	@repId int, 
	@timeId int, 
	@createOption int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if not exists(select top 1 1 
		from shared.EmployeeProfile ep
			join Shared.TimeDefinition td on Shared.fnCompareDates(td.FromDate, td.ToDate, ep.FromDate, ep.ToDate) = 1
		where EmployeeId = @repId and td.TimeDefinitionId = @timeId)
	begin
		select -2 /*Employee has no profile at this time*/
	end	
	else if not exists (select top 1 1 
	from crm.ProfileTargetVisit t
		join Shared.TimeDefinition td on t.TimeDefinitionId = td.TimeDefinitionId
		join Shared.EmployeeProfile ep on ep.ProfileId = t.ProfileId
			and Shared.fnCompareDates(td.FromDate, td.ToDate, ep.FromDate, ep.ToDate) = 1
	where EmployeeId = @repId and t.TimeDefinitionId = @timeId)
	begin
		declare @profileId int, @countryId int
		select @profileId = ProfileId, @countryId = e.CountryId
		from  Shared.EmployeeProfile ep 
			join Shared.TimeDefinition td on td.TimeDefinitionId = @timeId
			and Shared.fnCompareDates(td.FromDate, td.ToDate, ep.FromDate, ep.ToDate) = 1
			and ep.EmployeeId = @repId
			join hr.Employee e on ep.EmployeeId = e.EmployeeId

		insert into crm.ProfileTargetVisit(TimeDefinitionId, ProfileId, CountryId, ManagerResponse, CreationDate, CreatedById)
		values (@timeId, @profileId, @countryId,null,getdate(),@repId)

		declare @targetId int = SCOPE_IDENTITY()

		if (@createOption = 1 ) -- copy from previous period
		begin
		
			insert into crm.ProfileTargetVisitDetail(ProfileTargetId, AccountId, DoctorId, PotentialId, Frequency, CreationDate, CreatedById)		
			select @targetId,d.AccountId, DoctorId, PotentialId, Frequency, getdate(), @repId 
			from crm.ProfileTargetVisitDetail d
				join crm.ProfileTargetVisit t on d.ProfileTargetId = t.ProfileTargetId
			where t.ProfileId = @profileId 
			and t.TimeDefinitionId = @timeId-1 

		end
		--else if (@createOption = 2) -- make empty target
		--begin
		--	-- do nothing
		--end
		select @targetId
		end
		else select -1 /*target exists*/
	END