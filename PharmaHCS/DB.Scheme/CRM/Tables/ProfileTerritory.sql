﻿CREATE TABLE [CRM].[ProfileTerritory] (
    [ProfileTerritoryId] INT      NOT NULL,
    [ProfileId]          INT      NOT NULL,
    [TerritoryId]        INT      NOT NULL,
    [CreationDate]       DATETIME CONSTRAINT [ProfileTerritory_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]   DATETIME NULL,
    [CreatedById]        INT      NULL,
    [LastModifiedById]   INT      NULL,
    CONSTRAINT [PK_ProfileTerritory] PRIMARY KEY CLUSTERED ([ProfileTerritoryId] ASC),
    CONSTRAINT [FK_ProfileTerritory_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileTerritory_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileTerritory_TeamProfile] FOREIGN KEY ([ProfileId]) REFERENCES [Shared].[TeamProfile] ([ProfileId]),
    CONSTRAINT [FK_ProfileTerritory_Territory] FOREIGN KEY ([TerritoryId]) REFERENCES [Shared].[Territory] ([TerritoryId])
);


GO
ALTER TABLE [CRM].[ProfileTerritory] NOCHECK CONSTRAINT [FK_ProfileTerritory_CreatedBy];


GO
ALTER TABLE [CRM].[ProfileTerritory] NOCHECK CONSTRAINT [FK_ProfileTerritory_LastModifiedBy];


GO
ALTER TABLE [CRM].[ProfileTerritory] NOCHECK CONSTRAINT [FK_ProfileTerritory_TeamProfile];


GO
ALTER TABLE [CRM].[ProfileTerritory] NOCHECK CONSTRAINT [FK_ProfileTerritory_Territory];





