﻿CREATE TABLE [CRM].[ProductPresentation] (
    [PresentationId]   INT            IDENTITY (1, 1) NOT NULL,
    [PresentationName] NVARCHAR (MAX) NOT NULL,
    [FilePath]         NVARCHAR (MAX) NULL,
    [Ext]              NVARCHAR (50)  NULL,
    [ProductId]        INT            NOT NULL,
    [Active]           BIT            NOT NULL,
    [CreationDate]     DATETIME       CONSTRAINT [ProductPresentation_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_ProductPresentation] PRIMARY KEY CLUSTERED ([PresentationId] ASC),
    CONSTRAINT [FK_ProductPresentation_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProductPresentation_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProductPresentation_Product] FOREIGN KEY ([ProductId]) REFERENCES [Shared].[Product] ([ProductId])
);


GO
ALTER TABLE [CRM].[ProductPresentation] NOCHECK CONSTRAINT [FK_ProductPresentation_CreatedBy];


GO
ALTER TABLE [CRM].[ProductPresentation] NOCHECK CONSTRAINT [FK_ProductPresentation_LastModifiedBy];


GO
ALTER TABLE [CRM].[ProductPresentation] NOCHECK CONSTRAINT [FK_ProductPresentation_Product];




GO
ALTER TABLE [CRM].[ProductPresentation] NOCHECK CONSTRAINT [FK_ProductPresentation_CreatedBy];


GO
ALTER TABLE [CRM].[ProductPresentation] NOCHECK CONSTRAINT [FK_ProductPresentation_LastModifiedBy];


GO
ALTER TABLE [CRM].[ProductPresentation] NOCHECK CONSTRAINT [FK_ProductPresentation_Product];




GO
ALTER TABLE [CRM].[ProductPresentation] NOCHECK CONSTRAINT [FK_ProductPresentation_CreatedBy];


GO
ALTER TABLE [CRM].[ProductPresentation] NOCHECK CONSTRAINT [FK_ProductPresentation_LastModifiedBy];


GO
ALTER TABLE [CRM].[ProductPresentation] NOCHECK CONSTRAINT [FK_ProductPresentation_Product];





