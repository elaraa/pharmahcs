﻿CREATE TABLE [CRM].[VisitCommentType] (
    [CommentTypeId]    INT            NOT NULL,
    [CommentType]      NVARCHAR (MAX) COLLATE Latin1_General_CI_AS NOT NULL,
    [Active]           BIT            CONSTRAINT [DF_VisitCommentType_Active_1] DEFAULT ((1)) NOT NULL,
    [CreationDate]     DATETIME       CONSTRAINT [VisitCommentType_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_VisitCommentType] PRIMARY KEY CLUSTERED ([CommentTypeId] ASC),
    CONSTRAINT [FK_VisitCommentType_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitCommentType_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [CRM].[VisitCommentType] NOCHECK CONSTRAINT [FK_VisitCommentType_CreatedBy];


GO
ALTER TABLE [CRM].[VisitCommentType] NOCHECK CONSTRAINT [FK_VisitCommentType_LastModifiedBy];





