﻿CREATE TABLE [CRM].[Visit] (
    [VisitId]          UNIQUEIDENTIFIER NOT NULL,
    [VisitDate]        DATETIME         NOT NULL,
    [EmployeeId]       INT              NOT NULL,
    [AccountId]        INT              NOT NULL,
    [IsReviewed]       BIT              CONSTRAINT [DF_Visit_IsReviewed] DEFAULT ((0)) NOT NULL,
    [ReviewedById]     INT              NULL,
    [IsFalseVisit]     BIT              CONSTRAINT [DF_Visit_IsFalseVisit] DEFAULT ((0)) NOT NULL,
    [FalseVisitReason] NVARCHAR (MAX)   COLLATE Latin1_General_CI_AS NULL,
    [MarkedFalseById]  INT              NULL,
    [EntryTypeId]      INT              NOT NULL,
    [EntrySource]      NVARCHAR (MAX)   COLLATE Latin1_General_CI_AS NULL,
    [Latitude]         DECIMAL (18, 10) NULL,
    [Longitude]        DECIMAL (18, 10) NULL,
    [CountryId]        INT              NOT NULL,
    [CreationDate]     DATETIME         CONSTRAINT [Visit_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME         NULL,
    [CreatedById]      INT              NULL,
    [LastModifiedById] INT              NULL,
    CONSTRAINT [PK_Visit] PRIMARY KEY CLUSTERED ([VisitId] ASC),
    CONSTRAINT [FK_Visit_Account] FOREIGN KEY ([AccountId]) REFERENCES [Shared].[Account] ([AccountId]),
    CONSTRAINT [FK_Visit_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_Visit_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Visit_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Visit_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Visit_MarkedFalse] FOREIGN KEY ([MarkedFalseById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Visit_Review] FOREIGN KEY ([ReviewedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [CRM].[Visit] NOCHECK CONSTRAINT [FK_Visit_Account];


GO
ALTER TABLE [CRM].[Visit] NOCHECK CONSTRAINT [FK_Visit_Country];


GO
ALTER TABLE [CRM].[Visit] NOCHECK CONSTRAINT [FK_Visit_CreatedBy];


GO
ALTER TABLE [CRM].[Visit] NOCHECK CONSTRAINT [FK_Visit_Employee];


GO
ALTER TABLE [CRM].[Visit] NOCHECK CONSTRAINT [FK_Visit_LastModifiedBy];


GO
ALTER TABLE [CRM].[Visit] NOCHECK CONSTRAINT [FK_Visit_MarkedFalse];


GO
ALTER TABLE [CRM].[Visit] NOCHECK CONSTRAINT [FK_Visit_Review];







