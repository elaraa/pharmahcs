﻿CREATE TABLE [CRM].[Specialty] (
    [SpecialtyId]      INT            NOT NULL,
    [SpecialtyName]    NVARCHAR (MAX) NOT NULL,
    [SpecialtyCode]    NVARCHAR (50)  NULL,
    [CountryId]        INT            NULL,
    [CreationDate]     DATETIME       CONSTRAINT [Specialty_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_Specialty] PRIMARY KEY CLUSTERED ([SpecialtyId] ASC),
    CONSTRAINT [FK_Specialty_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_Specialty_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Specialty_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [CRM].[Specialty] NOCHECK CONSTRAINT [FK_Specialty_Country];


GO
ALTER TABLE [CRM].[Specialty] NOCHECK CONSTRAINT [FK_Specialty_CreatedBy];


GO
ALTER TABLE [CRM].[Specialty] NOCHECK CONSTRAINT [FK_Specialty_LastModifiedBy];





