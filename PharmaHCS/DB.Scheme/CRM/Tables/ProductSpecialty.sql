﻿CREATE TABLE [CRM].[ProductSpecialty] (
    [ProductId]        INT      NOT NULL,
    [SpecialtyId]      INT      NOT NULL,
    [CreationDate]     DATETIME CONSTRAINT [ProductSpecialty_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME NULL,
    [CreatedById]      INT      NULL,
    [LastModifiedById] INT      NULL,
    CONSTRAINT [PK_ProductSpecialty] PRIMARY KEY CLUSTERED ([ProductId] ASC, [SpecialtyId] ASC),
    CONSTRAINT [FK_ProductSpecialty_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProductSpecialty_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProductSpecialty_Product] FOREIGN KEY ([ProductId]) REFERENCES [Shared].[Product] ([ProductId]),
    CONSTRAINT [FK_ProductSpecialty_Specialty] FOREIGN KEY ([SpecialtyId]) REFERENCES [CRM].[Specialty] ([SpecialtyId])
);


GO
ALTER TABLE [CRM].[ProductSpecialty] NOCHECK CONSTRAINT [FK_ProductSpecialty_CreatedBy];


GO
ALTER TABLE [CRM].[ProductSpecialty] NOCHECK CONSTRAINT [FK_ProductSpecialty_LastModifiedBy];


GO
ALTER TABLE [CRM].[ProductSpecialty] NOCHECK CONSTRAINT [FK_ProductSpecialty_Product];


GO
ALTER TABLE [CRM].[ProductSpecialty] NOCHECK CONSTRAINT [FK_ProductSpecialty_Specialty];





