﻿CREATE TABLE [CRM].[ProfileTargetVisitDetail] (
    [ProfileTargetDetailId] INT      IDENTITY (1, 1) NOT NULL,
    [ProfileTargetId]       INT      NOT NULL,
    [AccountId]             INT      NOT NULL,
    [DoctorId]              INT      NULL,
    [PotentialId]           INT      NOT NULL,
    [Frequency]             INT      NOT NULL,
    [CreationDate]          DATETIME CONSTRAINT [ProfileTargetVisitDetail_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]      DATETIME NULL,
    [CreatedById]           INT      NULL,
    [LastModifiedById]      INT      NULL,
    CONSTRAINT [PK_ProfileTargetVisitDetail] PRIMARY KEY CLUSTERED ([ProfileTargetDetailId] ASC),
    CONSTRAINT [FK_ProfileTargetVisitDetail_Account] FOREIGN KEY ([AccountId]) REFERENCES [Shared].[Account] ([AccountId]),
    CONSTRAINT [FK_ProfileTargetVisitDetail_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileTargetVisitDetail_Doctor] FOREIGN KEY ([DoctorId]) REFERENCES [CRM].[Doctor] ([DoctorId]),
    CONSTRAINT [FK_ProfileTargetVisitDetail_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileTargetVisitDetail_Potential] FOREIGN KEY ([PotentialId]) REFERENCES [CRM].[Potential] ([PotentialId]),
    CONSTRAINT [FK_ProfileTargetVisitDetail_ProfileTargetVisit] FOREIGN KEY ([ProfileTargetId]) REFERENCES [CRM].[ProfileTargetVisit] ([ProfileTargetId])
);


GO
ALTER TABLE [CRM].[ProfileTargetVisitDetail] NOCHECK CONSTRAINT [FK_ProfileTargetVisitDetail_Account];


GO
ALTER TABLE [CRM].[ProfileTargetVisitDetail] NOCHECK CONSTRAINT [FK_ProfileTargetVisitDetail_CreatedBy];


GO
ALTER TABLE [CRM].[ProfileTargetVisitDetail] NOCHECK CONSTRAINT [FK_ProfileTargetVisitDetail_Doctor];


GO
ALTER TABLE [CRM].[ProfileTargetVisitDetail] NOCHECK CONSTRAINT [FK_ProfileTargetVisitDetail_LastModifiedBy];


GO
ALTER TABLE [CRM].[ProfileTargetVisitDetail] NOCHECK CONSTRAINT [FK_ProfileTargetVisitDetail_Potential];


GO
ALTER TABLE [CRM].[ProfileTargetVisitDetail] NOCHECK CONSTRAINT [FK_ProfileTargetVisitDetail_ProfileTargetVisit];







