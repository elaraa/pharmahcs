﻿CREATE TABLE [CRM].[VisitPresentation] (
    [VisitPresentationId] INT              IDENTITY (1, 1) NOT NULL,
    [VisitId]             UNIQUEIDENTIFIER NOT NULL,
    [PresentationId]      INT              NOT NULL,
    [CreationDate]        DATETIME         CONSTRAINT [VisitPresentation_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]    DATETIME         NULL,
    [CreatedById]         INT              NULL,
    [LastModifiedById]    INT              NULL,
    CONSTRAINT [PK_VisitPresentation] PRIMARY KEY CLUSTERED ([VisitPresentationId] ASC),
    CONSTRAINT [FK_VisitPresentation_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitPresentation_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitPresentation_ProductPresentation] FOREIGN KEY ([PresentationId]) REFERENCES [CRM].[ProductPresentation] ([PresentationId]),
    CONSTRAINT [FK_VisitPresentation_Visit] FOREIGN KEY ([VisitId]) REFERENCES [CRM].[Visit] ([VisitId])
);


GO
ALTER TABLE [CRM].[VisitPresentation] NOCHECK CONSTRAINT [FK_VisitPresentation_CreatedBy];


GO
ALTER TABLE [CRM].[VisitPresentation] NOCHECK CONSTRAINT [FK_VisitPresentation_LastModifiedBy];


GO
ALTER TABLE [CRM].[VisitPresentation] NOCHECK CONSTRAINT [FK_VisitPresentation_ProductPresentation];


GO
ALTER TABLE [CRM].[VisitPresentation] NOCHECK CONSTRAINT [FK_VisitPresentation_Visit];







