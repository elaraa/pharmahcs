﻿CREATE TABLE [CRM].[ProfileTargetVisit] (
    [ProfileTargetId]  INT            IDENTITY (1, 1) NOT NULL,
    [TimeDefinitionId] INT            NOT NULL,
    [ProfileId]        INT            NOT NULL,
    [CountryId]        INT            NOT NULL,
    [ManagerResponse]  CHAR (1)       CONSTRAINT [DF_ProfileTargetVisit_ManagerResponse] DEFAULT ('d') NULL,
    [ManagerId]        INT            NULL,
    [ManagerComment]   NVARCHAR (MAX) NULL,
    [CreationDate]     DATETIME       CONSTRAINT [ProfileTargetVisit_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_ProfileTargetVisit] PRIMARY KEY CLUSTERED ([ProfileTargetId] ASC),
    CONSTRAINT [FK_ProfileTargetVisit_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_ProfileTargetVisit_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileTargetVisit_Employee] FOREIGN KEY ([ManagerId]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileTargetVisit_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileTargetVisit_TeamProfile] FOREIGN KEY ([ProfileId]) REFERENCES [Shared].[TeamProfile] ([ProfileId]),
    CONSTRAINT [FK_ProfileTargetVisit_TimeDefinition] FOREIGN KEY ([TimeDefinitionId]) REFERENCES [Shared].[TimeDefinition] ([TimeDefinitionId])
);


GO
ALTER TABLE [CRM].[ProfileTargetVisit] NOCHECK CONSTRAINT [FK_ProfileTargetVisit_Country];


GO
ALTER TABLE [CRM].[ProfileTargetVisit] NOCHECK CONSTRAINT [FK_ProfileTargetVisit_CreatedBy];


GO
ALTER TABLE [CRM].[ProfileTargetVisit] NOCHECK CONSTRAINT [FK_ProfileTargetVisit_Employee];


GO
ALTER TABLE [CRM].[ProfileTargetVisit] NOCHECK CONSTRAINT [FK_ProfileTargetVisit_LastModifiedBy];


GO
ALTER TABLE [CRM].[ProfileTargetVisit] NOCHECK CONSTRAINT [FK_ProfileTargetVisit_TeamProfile];


GO
ALTER TABLE [CRM].[ProfileTargetVisit] NOCHECK CONSTRAINT [FK_ProfileTargetVisit_TimeDefinition];










GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'd => draft
p => pending approval
a => approved
r => rejected', @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'TABLE', @level1name = N'ProfileTargetVisit', @level2type = N'COLUMN', @level2name = N'ManagerResponse';

