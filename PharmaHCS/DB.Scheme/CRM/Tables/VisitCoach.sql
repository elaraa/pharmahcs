﻿CREATE TABLE [CRM].[VisitCoach] (
    [VisitId]          UNIQUEIDENTIFIER NOT NULL,
    [CoachId]          INT              NOT NULL,
    [CreationDate]     DATETIME         CONSTRAINT [VisitCoach_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME         NULL,
    [CreatedById]      INT              NULL,
    [LastModifiedById] INT              NULL,
    CONSTRAINT [PK_VisitCoach] PRIMARY KEY CLUSTERED ([VisitId] ASC, [CoachId] ASC),
    CONSTRAINT [FK_VisitCoach_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitCoach_Employee] FOREIGN KEY ([CoachId]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitCoach_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitCoach_Visit] FOREIGN KEY ([VisitId]) REFERENCES [CRM].[Visit] ([VisitId])
);


GO
ALTER TABLE [CRM].[VisitCoach] NOCHECK CONSTRAINT [FK_VisitCoach_CreatedBy];


GO
ALTER TABLE [CRM].[VisitCoach] NOCHECK CONSTRAINT [FK_VisitCoach_Employee];


GO
ALTER TABLE [CRM].[VisitCoach] NOCHECK CONSTRAINT [FK_VisitCoach_LastModifiedBy];


GO
ALTER TABLE [CRM].[VisitCoach] NOCHECK CONSTRAINT [FK_VisitCoach_Visit];





