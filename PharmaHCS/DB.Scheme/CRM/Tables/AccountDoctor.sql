﻿CREATE TABLE [CRM].[AccountDoctor] (
    [AccountId]        INT      NOT NULL,
    [DoctorId]         INT      NOT NULL,
    [IsPrimary]        BIT      NOT NULL,
    [CreationDate]     DATETIME CONSTRAINT [AccountDoctor_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME NULL,
    [CreatedById]      INT      NULL,
    [LastModifiedById] INT      NULL,
    CONSTRAINT [PK_AccountDoctor] PRIMARY KEY CLUSTERED ([AccountId] ASC, [DoctorId] ASC),
    CONSTRAINT [FK_AccountDoctor_Account] FOREIGN KEY ([AccountId]) REFERENCES [Shared].[Account] ([AccountId]),
    CONSTRAINT [FK_AccountDoctor_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_AccountDoctor_Doctor] FOREIGN KEY ([DoctorId]) REFERENCES [CRM].[Doctor] ([DoctorId]),
    CONSTRAINT [FK_AccountDoctor_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [CRM].[AccountDoctor] NOCHECK CONSTRAINT [FK_AccountDoctor_Account];


GO
ALTER TABLE [CRM].[AccountDoctor] NOCHECK CONSTRAINT [FK_AccountDoctor_CreatedBy];


GO
ALTER TABLE [CRM].[AccountDoctor] NOCHECK CONSTRAINT [FK_AccountDoctor_Doctor];


GO
ALTER TABLE [CRM].[AccountDoctor] NOCHECK CONSTRAINT [FK_AccountDoctor_LastModifiedBy];





