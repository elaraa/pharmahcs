﻿CREATE TABLE [CRM].[VisitComment] (
    [VisitId]          UNIQUEIDENTIFIER NOT NULL,
    [CommentTypeId]    INT              NOT NULL,
    [CommentText]      NVARCHAR (MAX)   COLLATE Latin1_General_CI_AS NOT NULL,
    [CreationDate]     DATETIME         CONSTRAINT [VisitComment_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME         NULL,
    [CreatedById]      INT              NULL,
    [LastModifiedById] INT              NULL,
    CONSTRAINT [PK_VisitComment] PRIMARY KEY CLUSTERED ([VisitId] ASC, [CommentTypeId] ASC),
    CONSTRAINT [FK_VisitComment_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitComment_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitComment_Visit] FOREIGN KEY ([VisitId]) REFERENCES [CRM].[Visit] ([VisitId]),
    CONSTRAINT [FK_VisitComment_VisitCommentType] FOREIGN KEY ([CommentTypeId]) REFERENCES [CRM].[VisitCommentType] ([CommentTypeId])
);


GO
ALTER TABLE [CRM].[VisitComment] NOCHECK CONSTRAINT [FK_VisitComment_CreatedBy];


GO
ALTER TABLE [CRM].[VisitComment] NOCHECK CONSTRAINT [FK_VisitComment_LastModifiedBy];


GO
ALTER TABLE [CRM].[VisitComment] NOCHECK CONSTRAINT [FK_VisitComment_Visit];


GO
ALTER TABLE [CRM].[VisitComment] NOCHECK CONSTRAINT [FK_VisitComment_VisitCommentType];





