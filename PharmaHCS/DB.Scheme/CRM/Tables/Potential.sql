﻿CREATE TABLE [CRM].[Potential] (
    [PotentialId]      INT            IDENTITY (1, 1) NOT NULL,
    [PotentialName]    NVARCHAR (MAX) NOT NULL,
    [PotentialCode]    NVARCHAR (50)  NULL,
    [CountryId]        INT            NULL,
    [CreationDate]     DATETIME       CONSTRAINT [Potential_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_Potential] PRIMARY KEY CLUSTERED ([PotentialId] ASC),
    CONSTRAINT [FK_Potential_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_Potential_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Potential_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [CRM].[Potential] NOCHECK CONSTRAINT [FK_Potential_Country];


GO
ALTER TABLE [CRM].[Potential] NOCHECK CONSTRAINT [FK_Potential_CreatedBy];


GO
ALTER TABLE [CRM].[Potential] NOCHECK CONSTRAINT [FK_Potential_LastModifiedBy];







