﻿CREATE TABLE [CRM].[VisitRequestType] (
    [VisitRequestTypeId]   INT            NOT NULL,
    [VisitRequestTypeName] NVARCHAR (MAX) COLLATE Latin1_General_CI_AS NOT NULL,
    [Active]               BIT            CONSTRAINT [DF_VisitRequestType_Active_1] DEFAULT ((1)) NOT NULL,
    [CreationDate]         DATETIME       CONSTRAINT [VisitRequestType_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]     DATETIME       NULL,
    [CreatedById]          INT            NULL,
    [LastModifiedById]     INT            NULL,
    CONSTRAINT [PK_VisitRequestType] PRIMARY KEY CLUSTERED ([VisitRequestTypeId] ASC),
    CONSTRAINT [FK_VisitRequestType_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitRequestType_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [CRM].[VisitRequestType] NOCHECK CONSTRAINT [FK_VisitRequestType_CreatedBy];


GO
ALTER TABLE [CRM].[VisitRequestType] NOCHECK CONSTRAINT [FK_VisitRequestType_LastModifiedBy];





