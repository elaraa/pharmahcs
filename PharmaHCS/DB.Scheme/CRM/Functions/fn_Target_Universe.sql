﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 23 Dec 19
-- Description:	require further checking
-- =============================================
CREATE FUNCTION [CRM].[fn_Target_Universe] 
(
	-- Add the parameters for the function here
	@TargetId int
)
RETURNS 
@Result TABLE 
(
	-- Add the column definitions for the TABLE variable here
	AccountId int not null, 
	AccountName nvarchar(max),
	DoctorId int,
	AccountTypeId int,
	AccountTypeName nvarchar(max),
	TerritoryId int,
	TerritoryName nvarchar(max),
	Address nvarchar(max),
	PotentialId int, PotentialCode nvarchar(max),
	SpecialtyId int, SpecialtyName nvarchar(max)
)
AS
BEGIN
	
	declare @fromDate date, @todate date, @profileId int, @timeId int
	
	select @profileId = ProfileId , @timeId = td.TimeDefinitionId, @fromDate=FromDate, @todate = ToDate 
	from CRM.ProfileTargetVisit tv 
		join Shared.TimeDefinition td on tv.TimeDefinitionId = td.TimeDefinitionId
	where ProfileTargetId = @TargetId
	
	insert into @Result
	select *
	from(
	select a.AccountId , a.AccountCode + ' | ' + a.AccountName AccountName
		, null DoctorId
		, act.AccountTypeId, act.AccountTypeName	
		, a.TerritoryId, t.TerritoryCode + ' | ' + t.TerritoryName TerritoryName
		, a.Address
		, p.PotentialId, p.PotentialCode
		, null SpecialtyId, null SpecialtyName
	from crm.ProfileAccount pa
		join Shared.Account a on pa.AccountId = a.AccountId
		join Shared.AccountType act on a.AccountTypeId = act.AccountTypeId
		join Shared.Territory t on a.TerritoryId = t.TerritoryId
		left join crm.Potential p on a.PotentialId = p.PotentialId

	where Shared.fnCompareDates(@fromDate, @todate, pa.FromDate, pa.ToDate) = 1	
		and pa.ProfileId = @profileId

	union all

	select a.AccountId , a.AccountCode + ' | ' + a.AccountName AccountName
		, null DoctorId
		, act.AccountTypeId, act.AccountTypeName	
		, a.TerritoryId, t.TerritoryCode + ' | ' + t.TerritoryName TerritoryName
		, a.Address
		, p.PotentialId, p.PotentialCode
		, null SpecialtyId, null SpecialtyName
	from sales.ProfileAccount pa
		join Shared.Account a on pa.AccountId = a.AccountId
		join Shared.AccountType act on a.AccountTypeId = act.AccountTypeId
		join Shared.Territory t on a.TerritoryId = t.TerritoryId
		left join crm.Potential p on a.PotentialId = p.PotentialId

	where Shared.fnCompareDates(@fromDate, @todate, pa.FromDate, pa.ToDate) = 1	
		and pa.ProfileId = @profileId

	union all

	select a.AccountId, isnull(d.DoctorName, a.AccountCode + ' | ' + a.AccountName) AccountName 
		, d.DoctorId
		, act.AccountTypeId, act.AccountTypeName	
		, pt.TerritoryId, t.TerritoryCode + ' | ' + t.TerritoryName TerritoryName
		, a.Address
		, p.PotentialId, p.PotentialCode
		, s.SpecialtyId, s.SpecialtyCode+ ' | ' + s.SpecialtyName
	from sales.ProfileTerritory pt 
		join Shared.Territory t on pt.TerritoryId = t.TerritoryId
		join Shared.Account a on t.TerritoryId = a.TerritoryId
		join Shared.AccountType act on a.AccountTypeId = act.AccountTypeId
		left join crm.Potential p on a.PotentialId = p.PotentialId
		left join crm.AccountDoctor ad on a.AccountId = ad.AccountId
			and act.MustHaveDoctor = 1
		left join crm.Doctor d on ad.DoctorId = d.DoctorId
		left join crm.Specialty s on d.SpecialtyId = s.SpecialtyId
		left join (
		select tsp.TeamId, tp.ProfileId, tsp.ProductId, ps.SpecialtyId--, FromDate, ToDate
		from sales.TeamSalesProduct tsp
			join Shared.TeamProfile tp on tsp.TeamId = tp.TeamId and tp.Active = 1
			join crm.ProductSpecialty ps on tsp.ProductId = ps.ProductId
		where tp.ProfileId = @profileId and Shared.fnCompareDates(@fromDate, @todate, tsp.FromDate, tsp.ToDate) = 1
		) ps on d.SpecialtyId = ps.SpecialtyId 


	where Shared.fnCompareDates(@fromDate, @todate, pt.FromDate, pt.ToDate) = 1	
		and pt.ProfileId = @profileId


	union all

	select a.AccountId, isnull(d.DoctorName, a.AccountCode + ' | ' + a.AccountName) AccountName 
		, d.DoctorId
		, act.AccountTypeId, act.AccountTypeName	
		, pt.TerritoryId, t.TerritoryCode + ' | ' + t.TerritoryName TerritoryName
		, a.Address
		, p.PotentialId, p.PotentialCode
		, s.SpecialtyId, s.SpecialtyCode+ ' | ' + s.SpecialtyName
	from CRM.ProfileTerritory pt 
		join Shared.Territory t on pt.TerritoryId = t.TerritoryId
		join Shared.Account a on t.TerritoryId = a.TerritoryId
		join Shared.AccountType act on a.AccountTypeId = act.AccountTypeId
		left join crm.Potential p on a.PotentialId = p.PotentialId
		left join crm.AccountDoctor ad on a.AccountId = ad.AccountId
			and act.MustHaveDoctor = 1
		left join crm.Doctor d on ad.DoctorId = d.DoctorId
		left join crm.Specialty s on d.SpecialtyId = s.SpecialtyId
		left join (
		select tsp.TeamId, tp.ProfileId, tsp.ProductId, ps.SpecialtyId--, FromDate, ToDate
		from sales.TeamSalesProduct tsp
			join Shared.TeamProfile tp on tsp.TeamId = tp.TeamId and tp.Active = 1
			join crm.ProductSpecialty ps on tsp.ProductId = ps.ProductId
		where tp.ProfileId = @profileId and Shared.fnCompareDates(@fromDate, @todate, tsp.FromDate, tsp.ToDate) = 1
		) ps on d.SpecialtyId = ps.SpecialtyId 

	where pt.ProfileId = @profileId
	)dat
	where not exists (
		select 1 
		from crm.ProfileTargetVisitDetail tvd
		where tvd.ProfileTargetId = @TargetId
			and tvd.AccountId = dat.AccountId
			and (tvd.DoctorId = dat.DoctorId or tvd.DoctorId is null)
	)

	RETURN 
END