﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 2 Oct 19
-- Description:	
-- =============================================
CREATE FUNCTION [Shared].[fn_Territory_GetParentLevelForLeaf] 
(	
	-- Add the parameters for the function here
	@Level int, 
	@countryId int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH Territory_Cte
	AS
	(
		SELECT O.TerritoryId,o.TerritoryName,  O.ParentTerritoryId, Depth AS Lvl,
			-- #2
			o.TerritoryId as [RootTerritoryId], o.TerritoryName RootTerritoryName
		FROM Shared.Territory O
		-- #1
		where o.Depth = @Level and o.CountryId = @countryId
		UNION ALL
		SELECT OG.TerritoryId, OG.TerritoryName, OG.ParentTerritoryId, Lvl+1 AS Lvl,
			-- #2
			oi.RootTerritoryId, oi.RootTerritoryName
		FROM Shared.Territory OG INNER JOIN Territory_Cte OI
		ON OI.TerritoryId = OG.ParentTerritoryId
			and OG.CountryId = @countryId
	)
	SELECT  TerritoryId, TerritoryName, RootTerritoryId, RootTerritoryName
	FROM Territory_Cte
)