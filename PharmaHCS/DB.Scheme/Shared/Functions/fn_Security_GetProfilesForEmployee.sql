﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 30 Sep 19
-- Description:	
-- =============================================
CREATE FUNCTION [Shared].[fn_Security_GetProfilesForEmployee]
(	
	-- Add the parameters for the function here
	@employeeId int, 
	@date date
)
RETURNS TABLE 
AS
RETURN 
(
	select ep.EmployeeId, ep.ProfileId, tp.TeamId
	from Shared.EmployeeProfile ep
		join Shared.TeamProfile tp on ep.ProfileId = tp.ProfileId
	where Shared.fnCompareDates(@date, @date, ep.FromDate, ep.ToDate) = 1
	and exists(select top 1 1 from hr.fn_Security_GetEmployeesForEmployee(@employeeId, @date) se where ep.EmployeeId = se.EmployeeId)
)