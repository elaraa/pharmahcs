﻿CREATE TABLE [Shared].[EmployeeProfile] (
    [EmployeeProfileId] INT      IDENTITY (1, 1) NOT NULL,
    [EmployeeId]        INT      NOT NULL,
    [ProfileId]         INT      NOT NULL,
    [FromDate]          DATE     NOT NULL,
    [ToDate]            DATE     NULL,
    [CreationDate]      DATETIME CONSTRAINT [EmployeeProfile_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]  DATETIME NULL,
    [CreatedById]       INT      NULL,
    [LastModifiedById]  INT      NULL,
    CONSTRAINT [PK_EmployeeProfile] PRIMARY KEY CLUSTERED ([EmployeeProfileId] ASC),
    CONSTRAINT [FK_EmployeeProfile_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_EmployeeProfile_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_EmployeeProfile_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_EmployeeProfile_TeamProfile] FOREIGN KEY ([ProfileId]) REFERENCES [Shared].[TeamProfile] ([ProfileId])
);


GO
ALTER TABLE [Shared].[EmployeeProfile] NOCHECK CONSTRAINT [FK_EmployeeProfile_CreatedBy];


GO
ALTER TABLE [Shared].[EmployeeProfile] NOCHECK CONSTRAINT [FK_EmployeeProfile_Employee];


GO
ALTER TABLE [Shared].[EmployeeProfile] NOCHECK CONSTRAINT [FK_EmployeeProfile_LastModifiedBy];


GO
ALTER TABLE [Shared].[EmployeeProfile] NOCHECK CONSTRAINT [FK_EmployeeProfile_TeamProfile];










GO
CREATE NONCLUSTERED INDEX [IX_EmployeeProfile_ByEmployeeId_ProfileFromTo]
    ON [Shared].[EmployeeProfile]([EmployeeId] ASC)
    INCLUDE([EmployeeProfileId], [ProfileId], [FromDate], [ToDate]);

