﻿CREATE TABLE [Shared].[Team] (
    [TeamId]           INT            IDENTITY (1, 1) NOT NULL,
    [TeamName]         NVARCHAR (MAX) NOT NULL,
    [TeamCode]         NVARCHAR (50)  NULL,
    [CountryId]        INT            NOT NULL,
    [CreationDate]     DATETIME       CONSTRAINT [Team_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_Team] PRIMARY KEY CLUSTERED ([TeamId] ASC),
    CONSTRAINT [FK_Team_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_Team_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Team_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [Shared].[Team] NOCHECK CONSTRAINT [FK_Team_Country];


GO
ALTER TABLE [Shared].[Team] NOCHECK CONSTRAINT [FK_Team_CreatedBy];


GO
ALTER TABLE [Shared].[Team] NOCHECK CONSTRAINT [FK_Team_LastModifiedBy];







