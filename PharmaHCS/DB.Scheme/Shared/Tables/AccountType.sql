﻿CREATE TABLE [Shared].[AccountType] (
    [AccountTypeId]           INT            IDENTITY (1, 1) NOT NULL,
    [AccountTypeName]         NVARCHAR (MAX) NOT NULL,
    [AccountTypeCode]         NVARCHAR (50)  NULL,
    [MustHaveDoctor]          BIT            NOT NULL,
    [TargetAndPlanWithDoctor] BIT            NOT NULL,
    [IncludeInSales]          BIT            NOT NULL,
    [CountryId]               INT            NULL,
    [CreationDate]            DATETIME       CONSTRAINT [AccountType_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]        DATETIME       NULL,
    [CreatedById]             INT            NULL,
    [LastModifiedById]        INT            NULL,
    CONSTRAINT [PK_AccountType] PRIMARY KEY CLUSTERED ([AccountTypeId] ASC),
    CONSTRAINT [FK_AccountType_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_AccountType_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_AccountType_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [Shared].[AccountType] NOCHECK CONSTRAINT [FK_AccountType_Country];


GO
ALTER TABLE [Shared].[AccountType] NOCHECK CONSTRAINT [FK_AccountType_CreatedBy];


GO
ALTER TABLE [Shared].[AccountType] NOCHECK CONSTRAINT [FK_AccountType_LastModifiedBy];







