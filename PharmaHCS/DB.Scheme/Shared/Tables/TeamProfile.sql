﻿CREATE TABLE [Shared].[TeamProfile] (
    [ProfileId]              INT             IDENTITY (1, 1) NOT NULL,
    [TeamId]                 INT             NOT NULL,
    [ProfileCode]            NVARCHAR (50)   NULL,
    [ProfileName]            NVARCHAR (MAX)  NOT NULL,
    [Active]                 BIT             CONSTRAINT [DF_TeamProfile_Active] DEFAULT ((1)) NOT NULL,
    [CreationDate]           DATETIME        CONSTRAINT [TeamProfile_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]       DATETIME        NULL,
    [CreatedById]            INT             NULL,
    [LastModifiedById]       INT             NULL,
    [AvgCallRate]            DECIMAL (18, 6) NULL,
    [ListTargetDoctorsCount] INT             NULL,
    CONSTRAINT [PK_TeamProfile] PRIMARY KEY CLUSTERED ([ProfileId] ASC),
    CONSTRAINT [FK_TeamProfile_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_TeamProfile_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_TeamProfile_Team] FOREIGN KEY ([TeamId]) REFERENCES [Shared].[Team] ([TeamId])
);


GO
ALTER TABLE [Shared].[TeamProfile] NOCHECK CONSTRAINT [FK_TeamProfile_CreatedBy];


GO
ALTER TABLE [Shared].[TeamProfile] NOCHECK CONSTRAINT [FK_TeamProfile_LastModifiedBy];


GO
ALTER TABLE [Shared].[TeamProfile] NOCHECK CONSTRAINT [FK_TeamProfile_Team];











