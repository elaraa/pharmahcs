﻿




CREATE view [Shared].[vw_DosageForm]
as
SELECT        d.DosageFormId, 
			 d.DosageFormCode, 
			 d.DosageFormName, 
			 d.ProductId,
			 e.ProductCode +' | '+ e.ProductName ProductName ,
			  d.Size, d.UnitId, 
			  dfu.UnitCode +' | '+ dfu.UnitName UnitName ,
			  d.PackFactor, 
			  d.Active, 
			  ISNULL(d.LastModifiedDate, 
			  d.CreationDate) AS LastUpdate, 
			  ISNULL(d.LastModifiedById, 
			  d.CreatedById) AS UpdatedBy, 
                u.EmployeeName AS UpdatedByLogonName, 
				 d.CountryId , 
			 c.CountryCode
FROM            Shared.ProductDosageForm AS d LEFT OUTER JOIN
				Shared.DosageFormUnit dfu on dfu.UnitId = d.UnitId LEFT OUTER JOIN
                         Shared.Product AS e ON e.ProductId = d.ProductId LEFT OUTER JOIN

                         Shared.Country AS c ON d.CountryId = c.CountryId LEFT OUTER JOIN
                         HR.Employee AS u ON u.EmployeeId = ISNULL(d.LastModifiedById, d.CreatedById)