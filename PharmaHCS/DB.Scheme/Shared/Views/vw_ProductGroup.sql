﻿




create view [Shared].[vw_ProductGroup]
as
select  a.GroupId	
	,	a.GroupCode
	,	a.GroupName
	,	a.ParentGroupId
	,	a.CountryId
	,	c.CountryCode
	, ISNULL(a.LastModifiedDate, a.CreationDate) LastUpdate
	, ISNULL(a.LastModifiedById, a.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from Shared.ProductGroup a
	left join (select count(GroupId) ProductGroupCount, GroupId from shared.ProductGroup group by GroupId) acc
		on a.GroupId = acc.GroupId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(a.LastModifiedById, a.CreatedById)
	left join Shared.Country c on a.CountryId = c.CountryId