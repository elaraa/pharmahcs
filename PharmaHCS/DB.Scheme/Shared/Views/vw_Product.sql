﻿
CREATE view [Shared].[vw_Product]
as
select  e.ProductId
	, e.ProductCode
	, e.ProductName
	, df.DosageFormCount
	, e.Active
	, e.CountryId
	, c.CountryCode
	, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
	, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from Shared.Product e
	LEFT JOIN (select ProductId, count(*) DosageFormCount from shared.ProductDosageForm group by ProductId) df
		on df.ProductId = e.ProductId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)
	left join Shared.Country c on e.CountryId = c.CountryId