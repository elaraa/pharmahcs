﻿
CREATE view [Shared].[vw_AccountType]
as
select  e.AccountTypeId
	, e.AccountTypeCode
	, e.AccountTypeName
	, e.MustHaveDoctor
	, e.TargetAndPlanWithDoctor
	, e.IncludeInSales
	, isnull(acc.AccountsCount,0) AccountsCount
	, e.CountryId
	, c.CountryCode
	, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
	, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from Shared.AccountType e
	left join (select count(AccountId) AccountsCount, AccountTypeId from shared.Account group by AccountTypeId) acc
		on e.AccountTypeId = acc.AccountTypeId
	left join Shared.Country c on e.CountryId = c.CountryId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)