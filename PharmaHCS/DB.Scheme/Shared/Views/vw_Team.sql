﻿




CREATE VIEW [Shared].[vw_Team]
as
SELECT      e.TeamId 
		,	e.TeamCode
		,	e.TeamName
		,	tp.ProfileCount
		, e.CountryId
	, c.CountryCode
	, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
	, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
			from Shared.Team e
			left join (select TeamId , count(*) ProfileCount  from Shared.TeamProfile where Active = 1 group by TeamId) tp
			on tp.TeamId = e.TeamId				
				LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)
	left join Shared.Country c on e.CountryId = c.CountryId