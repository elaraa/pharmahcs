﻿




create view [Shared].[vw_TeamProfile]
as
select  a.ProfileId	
	,	a.ProfileCode
	,	a.ProfileName
	,	a.TeamId
	,	t.TeamCode +' | '+ t.TeamName TeamName 
	,	a.Active
	,	t.CountryId
	,	c.CountryCode
	, ISNULL(a.LastModifiedDate, a.CreationDate) LastUpdate
	, ISNULL(a.LastModifiedById, a.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from Shared.TeamProfile a
	JOIN Shared.Team t on a.TeamId = t.TeamId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(a.LastModifiedById, a.CreatedById)
	left join Shared.Country c on t.CountryId = c.CountryId