﻿CREATE TABLE [Temp].[Target_AvgPercentage] (
    [TeamId]          INT             NOT NULL,
    [ProfileId]       INT             NOT NULL,
    [ProductId]       INT             NOT NULL,
    [SalesPecentage]  DECIMAL (38, 6) NULL,
    [TargetPecentage] DECIMAL (38, 6) NULL,
    [AvgPercentage]   DECIMAL (38, 6) NULL
);

