﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
/*
Standard Plugin
*/


SET IDENTITY_INSERT [Sales].[ImportPlugin] ON 
GO
MERGE [Sales].[ImportPlugin] using (VALUES 
(1, N'Standard File', N'bin\Sales.Import.Plugin.Standard.dll', 0, N'Sales.Import.Plugin.Standard.StandardFile', N'~/uploads/sales/pluginstemplates/SalesStandardFile.xlsx',1, CAST(N'2019-08-13T09:34:03.483' AS DateTime), NULL, NULL, NULL),
(2, N'POS', N'bin\Sales.Import.Plugin.POS.dll', 0, N'Sales.Import.Plugin.POS.POSFile', N'~/uploads/sales/pluginstemplates/POS.xlsx',1, CAST(N'2019-08-13T09:34:03.483' AS DateTime), NULL, NULL, NULL),
(3, N'LTD', N'bin\Sales.Import.Plugin.LTD.dll', 0, N'Sales.Import.Plugin.LTD.LTDFile', N'~/uploads/sales/pluginstemplates/LTD.TXT',1, CAST(N'2019-08-13T09:34:03.483' AS DateTime), NULL, NULL, NULL),
(4, N'EGD', N'bin\Sales.Import.Plugin.EGD.dll', 0, N'Sales.Import.Plugin.EGD.EGDFile', N'~/uploads/sales/pluginstemplates/EGD.zip', 1, CAST(N'2019-10-08T18:42:55.620' AS DateTime), NULL, NULL, NULL)
)
as Source([PluginId], [PluginName], [DllPath], [IsFiles], [ClassName], [TemplatePath],[RequireTimeDefinition], [CreationDate], [LastModifiedDate], [CreatedById], [LastModifiedById])
ON [Sales].[ImportPlugin].[PluginId] = SOURCE.[PluginId]
WHEN MATCHED THEN 
UPDATE SET [PluginName] = Source.[PluginName], [DllPath] = Source.[DllPath],[IsFiles] = Source.[IsFiles],[ClassName] = Source.[ClassName],
[TemplatePath] = Source.[TemplatePath],[RequireTimeDefinition] = Source.[RequireTimeDefinition], [CreationDate] = Source.[CreationDate]
WHEN NOT MATCHED THEN 
INSERT ([PluginId], [PluginName], [DllPath], [IsFiles], [ClassName], [TemplatePath],[RequireTimeDefinition], [CreationDate], [LastModifiedDate], [CreatedById], [LastModifiedById]) 
Values(Source.[PluginId], Source.[PluginName],Source.[DllPath],Source.[IsFiles],Source.[ClassName],Source.[TemplatePath],Source.[RequireTimeDefinition],Source.[CreationDate],null,null,null);
GO
SET IDENTITY_INSERT [Sales].[ImportPlugin] OFF
GO

/*Imported File Status*/

MERGE [Sales].[ImportedFileStatus] using (VALUES 
(1, N'Under Validation', CAST(N'2019-09-22T13:31:50.417' AS DateTime), NULL, NULL, NULL),
(2, N'Saved w/t Territory', CAST(N'2019-09-22T13:32:00.233' AS DateTime), NULL, NULL, NULL),
(3, N'Saved', CAST(N'2019-09-22T13:32:04.433' AS DateTime), NULL, NULL, NULL),
(4, N'Deleted (Full)', CAST(N'2019-09-22T13:32:13.570' AS DateTime), NULL, NULL, NULL),
(5, N'Deleted w/t Mapping', CAST(N'2019-09-22T13:32:25.520' AS DateTime), NULL, NULL, NULL),
(6, N'Failed', CAST(N'2019-09-22T13:32:30.907' AS DateTime), NULL, NULL, NULL),
(7, N'Canceled', CAST(N'2019-09-22T13:32:36.890' AS DateTime), NULL, NULL, NULL)
)
as Source([StatusId], [StatusName], [CreationDate], [LastModifiedDate], [CreatedById], [LastModifiedById])
ON [Sales].[ImportedFileStatus].[StatusId] = SOURCE.[StatusId]
WHEN MATCHED THEN 
UPDATE SET [StatusName] = Source.[StatusName], [CreationDate] = Source.[CreationDate], [LastModifiedDate] = Source.[LastModifiedDate],
	[CreatedById] = Source.[CreatedById], [LastModifiedById] = Source.[LastModifiedById]
WHEN NOT MATCHED THEN 
INSERT ([StatusId], [StatusName], [CreationDate], [LastModifiedDate], [CreatedById], [LastModifiedById]) 
Values(Source.[StatusId], Source.[StatusName], Source.[CreationDate], Source.[LastModifiedDate], Source.[CreatedById], Source.[LastModifiedById]);
GO

/*TargetType*/

MERGE [Sales].[TargetType] using (VALUES 
(1, N'Country', CAST(N'2019-10-08T13:35:53.417' AS DateTime), NULL, NULL, NULL),
(2, N'Product', CAST(N'2019-10-08T13:36:00.793' AS DateTime), NULL, NULL, NULL),
(3, N'Product Phasing', CAST(N'2019-10-08T13:36:07.323' AS DateTime), NULL, NULL, NULL),
(4, N'Team', CAST(N'2019-10-08T13:36:13.050' AS DateTime), NULL, NULL, NULL),
(5, N'Employee', CAST(N'2019-10-08T13:36:17.703' AS DateTime), NULL, NULL, NULL),
(6, N'Profile', CAST(N'2019-10-08T13:36:20.903' AS DateTime), NULL, NULL, NULL)
)
as Source([TargetTypeId], [TargetTypeName], [CreationDate], [LastModifiedDate], [CreatedById], [LastModifiedById])
ON [Sales].[TargetType].[TargetTypeId] = SOURCE.[TargetTypeId]
WHEN MATCHED THEN 
UPDATE SET [TargetTypeName] = Source.[TargetTypeName], [CreationDate] = Source.[CreationDate], [LastModifiedDate] = Source.[LastModifiedDate],
	[CreatedById] = Source.[CreatedById], [LastModifiedById] = Source.[LastModifiedById]
WHEN NOT MATCHED THEN 
INSERT ([TargetTypeId], [TargetTypeName], [CreationDate], [LastModifiedDate], [CreatedById], [LastModifiedById]) 
Values(Source.[TargetTypeId], Source.[TargetTypeName], Source.[CreationDate], Source.[LastModifiedDate], Source.[CreatedById], Source.[LastModifiedById]);
GO



/*ScheduledJob*/

--MERGE [App].[ScheduledJob] using (VALUES 
--(1, N'Country', CAST(N'2019-10-08T13:35:53.417' AS DateTime), NULL, NULL, NULL),
--(2, N'Product', CAST(N'2019-10-08T13:36:00.793' AS DateTime), NULL, NULL, NULL),
--(3, N'Product Phasing', CAST(N'2019-10-08T13:36:07.323' AS DateTime), NULL, NULL, NULL),
--(4, N'Team', CAST(N'2019-10-08T13:36:13.050' AS DateTime), NULL, NULL, NULL),
--(5, N'Employee', CAST(N'2019-10-08T13:36:17.703' AS DateTime), NULL, NULL, NULL),
--(6, N'Profile', CAST(N'2019-10-08T13:36:20.903' AS DateTime), NULL, NULL, NULL)
--)
--as Source([JobId], [JobName], [Module], [StoredProcedure], [FromDate], [ToDate], [ScheduleType], [Active], [LastRunTime], [LastRunSpan])
--ON [App].[ScheduledJob].[TargetTypeId] = SOURCE.[TargetTypeId]
--WHEN MATCHED THEN 
--UPDATE SET [JobId] = Source.[JobId], [JobName] = Source.[JobName], [Module] = Source.[Module],
--	[StoredProcedure] = Source.[StoredProcedure], [FromDate] = Source.[FromDate],
--	[ToDate] = Source.[ToDate], [ScheduleType] = Source.[ScheduleType], [Active] = Source.[Active], [LastRunTime] = Source.[LastRunTime], [LastRunSpan] = Source.[LastRunSpan]
--WHEN NOT MATCHED THEN 
--INSERT ([JobId], [JobName], [Module], [StoredProcedure], [FromDate], [ToDate], [ScheduleType], [Active], [LastRunTime], [LastRunSpan]) 
--Values(Source.[JobId], Source.[JobName],Source.[Module], Source.[StoredProcedure], Source.[FromDate], Source.[ToDate], Source.[ScheduleType], Source.[Active], Source.[LastRunTime], Source.[LastRunSpan]);
--GO


/*ScheduledJob*/

--MERGE [App].[Setting] using (VALUES 
--(1, N'Country', CAST(N'2019-10-08T13:35:53.417' AS DateTime), NULL, NULL, NULL),
--(2, N'Product', CAST(N'2019-10-08T13:36:00.793' AS DateTime), NULL, NULL, NULL),
--(3, N'Product Phasing', CAST(N'2019-10-08T13:36:07.323' AS DateTime), NULL, NULL, NULL),
--(4, N'Team', CAST(N'2019-10-08T13:36:13.050' AS DateTime), NULL, NULL, NULL),
--(5, N'Employee', CAST(N'2019-10-08T13:36:17.703' AS DateTime), NULL, NULL, NULL),
--(6, N'Profile', CAST(N'2019-10-08T13:36:20.903' AS DateTime), NULL, NULL, NULL)
--)
--as Source([SettingId], [SettingLabel], [SettingValue], [SettingType], [SettingAvailableValues], [Module], [CountryId], [CreationDate], [LastModifiedDate], [CreatedById], [LastModifiedById])
--ON [App].[Setting].[SettingId] = SOURCE.[SettingId]
--WHEN NOT MATCHED THEN 
--INSERT ([SettingId], [SettingLabel], [SettingValue], [SettingType], [SettingAvailableValues], [Module], [CountryId], [CreationDate], [LastModifiedDate], [CreatedById], [LastModifiedById]) 
--Values(Source.[SettingId], Source.[SettingLabel],Source.[SettingValue], Source.[SettingType], Source.[SettingAvailableValues], Source.[Module], Source.[CountryId], Source.[CreationDate], Source.[LastModifiedDate], Source.[CreatedById], Source.[LastModifiedById]);
--GO

/*
System Menu
*/
MERGE [App].[SystemMenu] using (VALUES 
(1, N'Home', N'icon-home', NULL, NULL, N'Home', N'Index', 1, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(2, N'Control Panel', N'icon-wrench', NULL, NULL, NULL, NULL, 2, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(3, N'Countries', N'icon-globe', NULL, NULL, N'Country', N'Index', 3, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(4, N'Shared', N'icon-puzzle', NULL, NULL, NULL, NULL, 4, N'shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(5, N'HR', N'icon-settings', NULL, NULL, NULL, NULL, 4, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(6, N'Sales', N'icon-briefcase', NULL, NULL, NULL, NULL, 5, N'Sales', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(7, N'CRM', N'icon-wallet', NULL, NULL, NULL, NULL, 6, N'CRM', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(8, N'Reports', N'icon-bar-chart', NULL, NULL, NULL, NULL, 7, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(9, N'Analyzers', N'fa fa-cube', NULL, NULL, NULL, NULL, 8, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(21, N'Time Definition', N'icon-calendar', 2, NULL, N'TimeDefinition', N'Index', 1, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(22, N'Security', N'icon-lock', 2, NULL, NULL, NULL, 2, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(23, N'Settings', N'icon-settings', 2, NULL, NULL, NULL, 3, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(41, N'Accounts', N'fa fa-hospital-o', 4, NULL, NULL, NULL, 1, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(42, N'Products', N'icon-basket-loaded', 4, NULL, NULL, NULL, 2, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(43, N'Team', N'icon-crop', 4, NULL, N'Team', N'Index', 3, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(44, N'Territory', N'icon-map', 4, NULL, N'Territory', N'Index', 4, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(45, N'Profiles', N'icon-tag', 4, NULL, N'TeamProfile', N'Index', 5, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(51, N'Job Titles', N'icon-tag', 5, NULL, NULL, NULL, 1, N'HR', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(52, N'Departments', N'icon-tag', 5, NULL, NULL, NULL, 2, N'HR', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(53, N'Employees', N'fa fa-users', 5, NULL, N'Employee', N'Index', 3, N'HR', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(61, N'Distributor', N'icon-user', 6, NULL, N'Distributor', N'Index', 1, N'Sales', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(62, N'Sales Imports', N'fa fa-cloud-upload', 6, NULL, N'SalesImport', N'Index', 2, N'Sales', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(63, N'Target', N'icon-cursor-move', 6, NULL, NULL, NULL, 3, N'Sales', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(81, N'BU Achievement', NULL, 8, NULL, N'ReportBUAch', N'Index', 1, N'Sales', CAST(N'2019-10-13T12:38:16.950' AS DateTime), NULL, NULL, NULL)
,
(82, N'BU Achievement by Product', NULL, 8, NULL, N'ReportBUAchProduct', N'Index', 2, N'Sales', CAST(N'2019-10-17T10:51:27.340' AS DateTime), NULL, NULL, NULL)
,
(83, N'PM Achievment', NULL, 8, NULL, N'ReportPMAch', N'Index', 3, N'Sales', CAST(N'2019-10-17T10:51:50.017' AS DateTime), NULL, NULL, NULL)
,
(84, N'PM Achievement by Product', NULL, 8, NULL, N'ReportPMAchProduct', N'Index', 4, N'Sales', CAST(N'2019-10-17T10:52:13.430' AS DateTime), NULL, NULL, NULL)
,
(85, N'Rep Achievement', NULL, 8, NULL, N'ReportRepAch', N'Index', 5, N'Sales', CAST(N'2019-10-17T10:52:36.740' AS DateTime), NULL, NULL, NULL)
,
(86, N'Rep Achivement by Product', NULL, 8, NULL, N'ReportRepAchProduct', N'Index', 6, N'Sales', CAST(N'2019-10-17T10:53:01.840' AS DateTime), NULL, NULL, NULL)
,
(87, N'Supervisor Achivement', NULL, 8, NULL, N'ReportSUPAch', N'Index', 7, N'Sales', CAST(N'2019-10-17T10:53:28.487' AS DateTime), NULL, NULL, NULL)
,
(88, N'Suprtvisor Achievment by Product', NULL, 8, NULL, N'ReportSUPAchProduct', N'Index', 8, N'Sales', CAST(N'2019-10-17T10:53:55.353' AS DateTime), NULL, NULL, NULL)
,
(91, N'Sales', NULL, 9, NULL, NULL, NULL, 1, N'Sales', CAST(N'2019-10-08T18:32:43.980' AS DateTime), NULL, NULL, NULL)
,
(221, N'Users', N'icon-user', 22, NULL, N'User', N'Index', 1, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(222, N'Roles', N'icon-layers', 22, NULL, N'Role', N'Index', 2, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(231, N'CRM Settings', N'icon-settings', 23, NULL, NULL, NULL, 1, N'CRM', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(232, N'Sales Settings', N'icon-settings', 23, NULL, NULL, NULL, 2, N'Sales', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(233, N'HR Settings', N'icon-settings', 23, NULL, NULL, NULL, 3, N'HR', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(411, N'Types', N'icon-tag', 41, NULL, N'AccountType', N'Index', 1, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(412, N'Cate,ries', N'icon-tag', 41, NULL, N'AccountCate,ry', N'Index', 2, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(413, N'Accounts', N'fa fa-hospital-o', 41, NULL, N'Account', N'Index', 3, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(421, N'Groups', N'icon-tag', 42, NULL, NULL, NULL, 1, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(422, N'Products', N'icon-basket-loaded', 42, NULL, N'Product', N'Index', 2, N'Shared', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(424, N'Prices', N'fa fa-money', 42, NULL, N'ProductPrice', N'Index', 1, N'Sales', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(631, N'Product Target', N'icon-cursor-move', 63, NULL, N'TargetProduct', N'Index', 1, N'Sales', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(632, N'Product Phasing', N'icon-cursor-move', 63, NULL, N'TargetProductPhasing', N'Index', 2, N'Sales', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(633, N'Profile Product Territory Target', NULL, 63, NULL, N'TargetProfile', N'Index', 3, N'Sales', CAST(N'2019-10-08T13:37:30.857' AS DateTime), NULL, NULL, NULL)
,
(634, N'IMS Market Share', N'icon-cursor-move', 63, NULL, N'MarketShare', N'Index', 4, N'Sales', CAST(N'2019-10-08T18:31:37.523' AS DateTime), NULL, NULL, NULL)
,
(911, N'Target Analyzer', NULL, 91, NULL, N'TargetAnalyzer', N'Index', 1, N'Sales', CAST(N'2019-10-08T18:33:21.253' AS DateTime), NULL, NULL, NULL)
,
(912, N'Sales Analyzer', NULL, 91, NULL, N'InMarketSalesAnalyzer', N'Index', 2, N'Sales', CAST(N'2019-10-08T18:33:40.950' AS DateTime), NULL, NULL, NULL)
)
as Source([MenuId], [MenuName], [MenuIcon], [ParentMenuId], [Color], [Controller], [Action], [SortOrder], [Module], [CreationDate], [LastModifiedDate], [CreatedById], [LastModifiedById])
ON [App].[SystemMenu].[MenuId] = SOURCE.[MenuId]
WHEN MATCHED THEN 
UPDATE SET [MenuId] = SOURCE.[MenuId], [MenuName]=SOURCE.[MenuName], [MenuIcon]=SOURCE.[MenuIcon], [ParentMenuId]=SOURCE.[ParentMenuId], [Color]=SOURCE.[Color], [Controller]=SOURCE.[Controller], [Action]=SOURCE.[Action], [SortOrder]=SOURCE.[SortOrder], [Module]=SOURCE.[Module], [CreationDate]=SOURCE.[CreationDate], [LastModifiedDate]=SOURCE.[LastModifiedDate], [CreatedById]=SOURCE.[CreatedById], [LastModifiedById]=SOURCE.[LastModifiedById]
WHEN NOT MATCHED THEN 
INSERT ([MenuId], [MenuName], [MenuIcon], [ParentMenuId], [Color], [Controller], [Action], [SortOrder], [Module], [CreationDate], [LastModifiedDate], [CreatedById], [LastModifiedById])
Values(SOURCE.[MenuId], SOURCE.[MenuName], SOURCE.[MenuIcon], SOURCE.[ParentMenuId], SOURCE.[Color], SOURCE.[Controller], SOURCE.[Action], SOURCE.[SortOrder], SOURCE.[Module], getdate(), null, null, null);
GO
