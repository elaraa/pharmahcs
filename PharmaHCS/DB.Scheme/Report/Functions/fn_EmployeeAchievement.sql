﻿-- =============================================
-- Author:		Nader
-- Create date: 24/Feb/2020
-- Description:	
-- =============================================
CREATE FUNCTION [Report].[fn_EmployeeAchievement] 
(	
	-- Add the parameters for the function here
	 @TimeDefinitionId nvarchar(max), 
	 @TeamId nvarchar(max)
)
RETURNS TABLE 
AS
RETURN 
(
	select s.*, p.ProductCode, p.ProductName
	from(
		
			select e.TeamId, e.EmployeeId, EmployeeCode, EmployeeName, e.ProfileId, ProfileCode, e.TimeDefinitionId, SupId, SupCode, SupName, PMId, PMCode, PMName, BUMId, BUMCode, BUMName, PMDId, PMDCode, PMDName, SODId, 
								 SODCode, SODName, SalesQty, SalesAmount
								  ,TargetQty , TargetAmount
								  , e.PeriodName, tsp.ProductId --, p.ProductCode, p.ProductName
		from Report.tb_EmployeeStructurewProfile e
		join [Shared].[fnStringList2Table] (@TimeDefinitionId, ',' ) td
							on e.TimeDefinitionId = td.item
		join [Shared].[fnStringList2Table] (@TeamId, ',' ) tm
							on e.TeamId = tm.item
		join shared.TimeDefinition tdd on tdd.TimeDefinitionId = e.TimeDefinitionId
		join sales.TeamSalesProduct tsp on tsp.TeamId = e.TeamId
		 and shared.fnCompareDates(tdd.FromDate, tdd.ToDate, tsp.FromDate, tsp.ToDate)=1
		left join (
			select s.ProfileId, s.TimeDefinitionId,sum(s.Qty) SalesQty, sum(s.Amount) SalesAmount
									  ,s.ProductId
			from Staging.InMarketSalesByProfileAccount s with (nolock)
				join [Shared].[fnStringList2Table] (@TimeDefinitionId, ',' ) td
						on s.TimeDefinitionId = td.item
			group by  s.ProfileId, s.TimeDefinitionId, s.ProductId
			) s on e.ProfileId = s.ProfileId and e.TimeDefinitionId = s.TimeDefinitionId
				and tsp.ProductId = s.ProductId
		left join (
				select t.ProfileId, t.ProductId,t.TimeDefinitionId ,sum(t.Qty) TargetQty , sum(t.Amount) as TargetAmount
				from sales.target t with (nolock)
				join [Shared].[fnStringList2Table] (@TimeDefinitionId, ',' ) td
							on t.TimeDefinitionId = td.item
				where t.TargetTypeId = 6
				group by t.ProfileId, t.ProductId,t.TimeDefinitionId
			)t on e.ProfileId = t.ProfileId and e.TimeDefinitionId = t.TimeDefinitionId
			and t.ProductId = tsp.ProductId
		)s 
		join Shared.Product p on s.ProductId = p.ProductId

)