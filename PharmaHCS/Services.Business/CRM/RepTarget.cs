﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.CRM
{
    public enum RepTargetCreateOption
    {
        CopyFromPreviousPeriod = 1,
        Empty
    }
    public class ReptargetResult
    {
        public bool Success { get; set; }
        public int ResultId { get; set; }
    }
    public class RepTarget: BaseService<ProfileTargetVisit>
    {
        public RepTarget(int userId): base(userId){}
        public ReptargetResult CreateTarget(int timeDefinitionId, RepTargetCreateOption createOption){
            var res = db.sp_RepTargetCreate(UserId, timeDefinitionId, (int)createOption);
            return new ReptargetResult { Success = res > 0, ResultId = res };
        }
        public IQueryable<crm_fn_Target_Universe> GetTargetUniverse(int targetId) => db.crm_fn_Target_Universe(targetId);
        public IQueryable<vw_ProfileTargetVisitDetails> GetTargetVisitDetails(int targetId) => db.vw_ProfileTargetVisitDetails.Where(w => w.ProfileTargetId == targetId);
        public bool DeleteAccount(int targetDetailId) {
            return (from d in db.ProfileTargetVisitDetails
                    join t in db.ProfileTargetVisits on d.ProfileTargetId equals t.ProfileTargetId
                    where d.ProfileTargetDetailId == targetDetailId
                        && ( t.ManagerResponse == null || t.ManagerResponse.ToLower() == "r")
                    select d
                ).DeleteFromQuery()>0;
        }
        public bool UpdateAccount(int targetDetailId, int potentialId, int frequency) {
            return db.ProfileTargetVisitDetails
                .Where(w => w.ProfileTargetDetailId == targetDetailId)
                .UpdateFromQuery(u=>new ProfileTargetVisitDetail { Frequency = frequency,  PotentialId = potentialId } ) > 0;
        }
        public bool AddAccount(int targetId, int accountId, int? doctorId)
        {
            var potId = (doctorId.HasValue ? db.Doctors.FirstOrDefault(w => w.DoctorId == doctorId).PotentialId : db.Accounts.FirstOrDefault(w => w.AccountId == accountId).PotentialId) ?? db.Potentials.FirstOrDefault().PotentialId;

            db.ProfileTargetVisitDetails.Add(new ProfileTargetVisitDetail {
                ProfileTargetId = targetId,
                AccountId = accountId,
                DoctorId = doctorId,
                PotentialId = potId,
                Frequency = 1
            });
            return db.SaveChanges() > 0;
        }
    }
}
