﻿using DB.ORM.DB;
using LinqKit;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Services.Business.CRM
{
    public class VisitService : BaseService<vw_VisitsPerDate>
    {
        public VisitService(int userId) : base(userId) { }
        public override IQueryable<vw_VisitsPerDate> Get(Expression<Func<vw_VisitsPerDate, bool>> whereClause = null, bool safe = false, params string[] childrenToInclude)
        {
            var employees = db.fn_Security_GetEmployeesForEmployee(UserId, DateTime.Now).ToList();
            var predicate = PredicateBuilder.New<vw_VisitsPerDate>(true);
            if (whereClause != null)
                predicate = predicate.And(whereClause);

            predicate.And(w => employees.Contains(w.EmployeeId));

            return base.Get(predicate, safe, childrenToInclude);
        }
        public vw_VisitsPerDate GetByDate(DateTime date, int employeeId) => GetOne(w => System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(w.VisitDate) == System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(date) && w.EmployeeId == employeeId);
        public IQueryable<vw_VisitsList> ReadVisitsList(DateTime date, int employeeId)
        {
            return db.vw_VisitsList.Where(w => System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(w.VisitDate) == System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(date) && w.EmployeeId == employeeId);
        }

        public bool ApproveRejectVisit(Guid id, bool approve)
        {
            var v = db.Visits.FirstOrDefault(w=>w.VisitId == id && w.ReviewedById == null);
            if (v == null) return false;

            v.ReviewedById = UserId;
            v.IsReviewed = approve;
            
            return db.SaveChanges()> 0;
        }
    }
}
