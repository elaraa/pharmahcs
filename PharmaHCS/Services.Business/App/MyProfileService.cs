﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business
{
    public class SaveResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
    public class MyProfileService
    {
        private int UserId { get; set; }
        public MyProfileService(int userid)
        {
            UserId = userid;
        }
        public SaveResult SaveBasicData(string mobileNumber, string gender, string maritalStatus) {
            var result = new SaveResult();
            if (!mobileNumber.IsEmpty() && mobileNumber.Trim().Length < 11) {
                result.Success = false;
                result.Message = "Mobile Number cannot be less than 11 numbers";
                return result;
            }
            if (!gender.IsEmpty() && !new string[]{"M","F"}.Contains(gender.Trim()))
            {
                result.Success = false;
                result.Message = "Gender values are incorrect";
                return result;
            }
            if (!maritalStatus.IsEmpty() && !new string[] { "S", "M" }.Contains(maritalStatus.Trim()))
            {
                result.Success = false;
                result.Message = "Marital Status values are incorrect";
                return result;
            }
            if (maritalStatus.IsEmpty() && gender.IsEmpty() && mobileNumber.IsEmpty()){ result.Success = true; return result; }

            using (var db = new DBModel())
            {
                db.UserId = UserId;
                var emp = db.Employees.FirstOrDefault(w => w.EmployeeId == UserId);
                if (!mobileNumber.IsEmpty())
                    emp.Mobile = mobileNumber.Trim();
                if (!gender.IsEmpty())
                    emp.Gender = gender.Trim();
                if (!maritalStatus.IsEmpty())
                    emp.MaritalStatus = maritalStatus.Trim();
                result.Success = db.SaveChanges()>0;
                result.Message = result.Success ? "" : "Failed to save data";
                return result;
            }
        }
        public MProfile MyProfile
        {
            get
            {
                MProfile res = new MProfile();
                ///get basic data from Employee
                using (var db = new DBModel())
                {
                    var vemp = db.vw_Employee.FirstOrDefault(w => w.EmployeeId == UserId);
                    var emp = db.Employees.FirstOrDefault(w => w.EmployeeId == UserId);
                    var manager = (from h in db.EmployeeHistories
                                   join e in db.Employees on h.ManagerId equals e.EmployeeId
                                   where h.EmployeeId == UserId && h.FromDate <= DateTime.Now && (h.ToDate >= DateTime.Now || h.ToDate.HasValue == false)
                                   select e.EmployeeName
                                    ).FirstOrDefault();
                        
                        db.EmployeeHistories.FirstOrDefault();
                   

                    res.FullName = vemp.EmployeeName;
                    res.JobTitle = vemp.JobTitleName;
                    res.DepartmentName = vemp.DepartmentName;
                    res.Country = vemp.CountryCode;
                    res.BirthDate = emp.BirthDate;
                    res.Email = emp.Email;
                    res.Gender = emp.Gender;
                    res.GenderText = !emp.Gender.IsEmpty() && emp.Gender.ToLower() == "m" ? "Male" : "Female";
                    res.HiringDate = emp.HireDate;
                    res.MaritalStatus = emp.MaritalStatus;
                    res.MaritalStatusName = emp.MaritalStatus.ToLower() == "s" ? "Single" : "Married";
                    res.Mobile = emp.Mobile;
                    res.Pic = emp.Picture;
                    res.ManagerName = manager;
                }
                return res;
            }
        }

        public bool SavePassword(string currentPassword, string newPassword)
        {
            if (currentPassword.IsEmpty() || newPassword.IsEmpty()) return false;

            using (var db = new DBModel())
            {
                db.UserId = UserId;
                var emp = db.Employees.FirstOrDefault(w => w.EmployeeId == UserId);

                if (emp.Password == LoginService.HashString(currentPassword))
                    emp.Password = LoginService.HashString(newPassword);
                else
                    return false;
                return db.SaveChanges() > 0;
            }
        }

        public bool SavePicture(string fileName)
        {
            if (fileName.IsEmpty()) return true;
            using (var db = new DBModel())
            {
                db.UserId = UserId;
                var emp = db.Employees.FirstOrDefault(w => w.EmployeeId == UserId);
                if (!fileName.IsEmpty())
                    emp.Picture = fileName.Trim();
                return db.SaveChanges() > 0;
            }
        }
    }
}
