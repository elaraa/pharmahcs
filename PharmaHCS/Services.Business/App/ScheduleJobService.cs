﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.SqlServer;
using System.Linq;

namespace Services.Business
{
    public class ScheduleJobService:BaseService<ScheduledJob>
    {
        public static void Run() {
            while (true)
            {

                try
                {
                    ///get the jobs that are eligible to run
                    using (var db = new DBModel())
                    {
                        var jobs = db.fn_ScheduleJobs_Get().ToList();
                        if (jobs != null && jobs.Count() > 0)
                        {
                            var timeId = db.TimeDefinitions.FirstOrDefault(w => w.FromDate <= DateTime.Now && w.ToDate >= DateTime.Now)?.TimeDefinitionId;
                            foreach (var job in jobs)
                            {
                                    //get stored proc parameters
                                    var dtParameters = db.Database.ExecuteDataTable(string.Format("select  name Parameter_name, type_name(user_type_id) Type from sys.parameters where object_id = object_id('{0}')", job.StoredProcedure));
                                    var paramStr = new List<string>();
                                    foreach (DataRow item in dtParameters.Rows)
                                    {
                                        if (item["Parameter_name"].ToString().ToLower().Contains("timeid"))
                                            paramStr.Add(string.Format("{0}={1}", item["Parameter_name"].ToString(), timeId.ToString()));

                                        else if (item["Parameter_name"].ToString().ToLower().Contains("date"))
                                            paramStr.Add(string.Format("{0}='{1}'", item["Parameter_name"].ToString(), DateTime.Now.ToString("yyyy-MM-dd")));

                                    }
                                    job.LastRunTime = DateTime.Now;
                                    //run jobs
                                    db.Database.CommandTimeout = 0;
                                    db.Database.ExecuteScalar(string.Format("exec {0} {1}", job.StoredProcedure, string.Join(",", paramStr)));
                                    //update jobs last run time and elabsed time 
                                    TimeSpan diff = DateTime.Now - job.LastRunTime.Value;
                                    job.LastRunSpan = Convert.ToDecimal(diff.TotalMinutes);   
                            }
                            db.SaveChanges();
                        }

                    }
                    ///run every hour
                }
                catch
                {

                }
                System.Threading.Thread.Sleep(1000 * 60 * 60);
            }
        }
    }
}
