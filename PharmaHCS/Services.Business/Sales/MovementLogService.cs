﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.Sales
{
    public class MovementLogService:BaseService<vw_VisitsList>
    {
        public MovementLogService(int userId) : base(userId)
        {
            
        }

        public IQueryable<vw_VisitsList> getVisits(int id,DateTime date)
        {
            return Get(a => a.EmployeeId == id && a.VisitDate == date.Date);
        }
        
    }
}
