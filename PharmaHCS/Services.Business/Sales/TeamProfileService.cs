﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.Sales
{
    public class TeamProfileService : BaseService<TeamProfile>
    {
        public TeamProfileService(int userId) : base(userId)
        {
        }
        public RunResult ValidateProfileAccount(ProfileAccount1 profile) {
            try {
                 if(Get<ProfileAccount1>(c=> c.ProfileAccountId!= profile.ProfileAccountId && c.AccountId==profile.AccountId 
                                   && c.ProfileId == profile.ProfileId
                                   && (profile.ProductId.HasValue ? c.ProductId == profile.ProductId : true) 
                                   && (profile.DosageFromId !=null ? c.DosageFromId == profile.DosageFromId:true)
                                   && ((c.ToDate.HasValue ? profile.FromDate <= c.ToDate.Value : true)  &&
                                      (profile.ToDate.HasValue ? profile.ToDate.Value >= c.FromDate : true))).Any())
                     return new RunResult { Succeeded = false, ErrorDesc = PH.Message.DateOverlapping };
                else
                {
                    var teamProfile = GetOne<TeamProfile>(p => p.ProfileId == profile.ProfileId);
                    if (teamProfile != null)
                    {
                        var profiles = Get<TeamProfile>(p => p.TeamId == teamProfile.TeamId).Select(c => c.ProfileId);
                        var profilAccounts = Get<ProfileAccount1>(a => a.AccountId == profile.AccountId
                              && (profile.ProductId.HasValue ? a.ProductId == profile.ProductId : true)
                              && (profile.DosageFromId != null ? a.DosageFromId == profile.DosageFromId : true)
                              && a.ProfileAccountId!=profile.ProfileAccountId
                              && profiles.Contains(a.ProfileId) && ((a.ToDate.HasValue ? profile.FromDate <= a.ToDate.Value : true) 
                              && (profile.ToDate.HasValue ? profile.ToDate.Value >= a.FromDate : true)));

                        var TotalPercentage = profilAccounts.Count() > 0 ? profilAccounts.Sum(a => a.Perccentage) : 0;
                        var teamName = GetOne<Team>(c=>c.TeamId == teamProfile.TeamId).TeamName;
                        var accountName = GetOne<Account>(c => c.AccountId == profile.AccountId).AccountName;

                        if (TotalPercentage + profile.Perccentage > 100)
                            return new RunResult { Succeeded = false, ErrorDesc = 
                                string.Format(PH.Message.ProfileAccountErrorCode, decimal.ToInt32(100 - TotalPercentage), accountName, teamName) };  
                        else
                            return new RunResult { Succeeded = true, SuccessDesc = string.Format(PH.Message.ProfileAccountSuccessCode, decimal.ToInt32(100 - (TotalPercentage + profile.Perccentage)), accountName, teamName) };
                    }
                    return new RunResult { Succeeded = false, ErrorDesc = PH.Message.ProfileNotFound };
                }
                     
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RunResult { Succeeded = false, ErrorDesc = PH.Message.ExceptionHappened };
            }
        }

        public RunResult ValidateProfileTerritory(ProfileTerritory1 profile)
        {
            try
            {     //Check This Condetion with Hesham
                if (Get<ProfileTerritory1>(c => c.ProfileTerritoryId != profile.ProfileTerritoryId && c.ProfileId == profile.ProfileId
                                   && c.TerritoryId == profile.TerritoryId &&c.FromDate==profile.FromDate
                                   && ((c.ToDate.HasValue ? profile.FromDate <= c.ToDate.Value : true) &&
                                      (profile.ToDate.HasValue ? profile.ToDate.Value >= c.FromDate : true))).Any())
                    return new RunResult { Succeeded = false, ErrorDesc = PH.Message.DateOverlapping };
                else
                {
                    var teamProfile = GetOne<TeamProfile>(p => p.ProfileId == profile.ProfileId);
                    if (teamProfile != null)
                    {
                        var profiles = Get<TeamProfile>(p => p.TeamId == teamProfile.TeamId).Select(c => c.ProfileId);
                        var profileaTerritory = Get<ProfileTerritory1>(a => a.TerritoryId == profile.TerritoryId && a.ProfileTerritoryId != profile.ProfileTerritoryId &&
                             profiles.Contains(a.ProfileId) && ((a.ToDate.HasValue ? profile.FromDate <= a.ToDate.Value : true) &&
                                      (profile.ToDate.HasValue ? profile.ToDate.Value >= a.FromDate : true)));

                        var TotalPercentage = profileaTerritory.Count() > 0 ? profileaTerritory.Sum(a => a.Percentage) : 0;
                        var teamName = GetOne<Team>(c => c.TeamId == teamProfile.TeamId).TeamName;
                        var territoryName = GetOne<Territory>(c => c.TerritoryId == profile.TerritoryId).TerritoryName; 
                        if (TotalPercentage + profile.Percentage > 100)
                            return new RunResult { Succeeded = false, ErrorDesc = PH.Message.PercentagError };
                        else
                            return new RunResult { Succeeded = true, SuccessDesc = string.Format(PH.Message.ProfileTerritorySuccessCode, decimal.ToInt32(100 - (TotalPercentage + profile.Percentage)), territoryName, teamName) };
                       
                    }
                    return new RunResult { Succeeded = false, ErrorDesc = PH.Message.ProfileNotFound };
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                return new RunResult { Succeeded = false, ErrorDesc = PH.Message.ExceptionHappened };
            }
        }

        public RunResult IsOverlapped(TeamProfile profile)
        {
            try
            {
                if (Get(c => c.ProfileId != profile.ProfileId && (profile.ProfileCode != null ? c.ProfileCode == profile.ProfileCode : false)).Any())
                    return new RunResult { Succeeded = false, ErrorDesc = string.Format(PH.Message.DuplicatedData, PH.Team.ProfileCode) };
                else if (Get(c => c.ProfileId != profile.ProfileId && c.ProfileName == profile.ProfileName).Any())
                    return new RunResult { Succeeded = false, ErrorDesc = string.Format(PH.Message.DuplicatedData, PH.Team.ProfileName) };
                else
                    return new RunResult { Succeeded = true };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RunResult { Succeeded = false, ErrorDesc = PH.Message.ExceptionHappened };
            }
        }
    }
}
