﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.Sales
{
    public class InMarketSalesAnalyzer
    {
        private int UserId { get; set; }
        public InMarketSalesAnalyzer(int userId)
        {
            UserId = userId;
        }

        public fn_InMarketSales_Analysis_GetSummary GetSummary(int? year = null, int? teamId = null, int? productId = null, int? TerritoryId = null, int? employeeId = null)
        {
            var res = new fn_InMarketSales_Analysis_GetSummary();
            using (var db = new DBModel())
                res = db.fn_InMarketSales_Analysis_GetSummary(1, year ?? DateTime.Now.Year, UserId, teamId, productId, TerritoryId, employeeId).FirstOrDefault();

            res.Amount_Growth = res.Amount_Growth ?? 100;
            res.Qty_Growth = res.Qty_Growth ?? 100;
            res.Amount_Ach = res.Amount_Ach ?? 0;
            res.Qty_Ach = res.Qty_Ach?? 0;

            return res;
        }
        public List<fn_InMarketSales_Analysis_DistributorSales> GetDistributorSales(int? year = null, int? teamId = null, int? productId = null, int? TerritoryId = null, int? employeeId = null)
        {
//            var res = new fn_InMarketSales_Analysis_DistributorSales();
            using (var db = new DBModel())
                return db.fn_InMarketSales_Analysis_DistributorSales(1, year ?? DateTime.Now.Year, UserId, teamId, productId, TerritoryId, employeeId).ToList();

 //           return res;
        }

        public List<fn_InMarketSales_Analysis_SalesTrend> GetSalesTrend(int? year = null, int? teamId = null, int? productId = null, int? TerritoryId = null, int? employeeId = null)
        {
            using (var db = new DBModel())
                return db.fn_InMarketSales_Analysis_SalesTrend(1, year ?? DateTime.Now.Year, UserId, teamId, productId, TerritoryId, employeeId).ToList();
        }

        public List<fn_InMarketSales_Analysis_SalesByAccountCategory> GetSalesByAccountCategory(int? year = null, int? teamId = null, int? productId = null, int? TerritoryId = null, int? employeeId = null)
        {
            using (var db = new DBModel())
                return db.fn_InMarketSales_Analysis_SalesByAccountCategory(1, year ?? DateTime.Now.Year, UserId, teamId, productId, TerritoryId, employeeId).ToList();
        }

        public List<fn_InMarketSales_Analysis_SalesByTerritory> GetSalesByTerritory(int? year = null, int? teamId = null, int? productId = null, int? TerritoryId = null, int? employeeId = null)
        {
            using (var db = new DBModel())
                return db.fn_InMarketSales_Analysis_SalesByTerritory(1, year ?? DateTime.Now.Year, UserId, teamId, productId, TerritoryId, employeeId).ToList();
        }
    }
}
