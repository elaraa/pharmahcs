﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.Sales
{
    public class CeilingDosageService : BaseService<CeilingDosageAccount>
    {
        public CeilingDosageService(int userId) : base(userId)
        {
        }
        

        public RunResult IsOverlapped(CeilingDosageAccount ceiling)
        {
            try
            {
                if (Get(c => c.CeilingDosageAccountId != ceiling.CeilingDosageAccountId &&
                                   c.DosageFormId == ceiling.DosageFormId && c.AccountId == ceiling.AccountId
                                      && ((c.ToTimeId.HasValue ? ceiling.FromTimeId <= c.ToTimeId.Value : true) &&
                                         (ceiling.ToTimeId.HasValue ? ceiling.ToTimeId.Value >= c.FromTimeId : true))).Any())
                    return new RunResult { Succeeded = false, ErrorDesc = PH.Message.DateOverlapping };
                else
                  return new RunResult { Succeeded = true };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RunResult { Succeeded = false, ErrorDesc = PH.Message.ExceptionHappened };
            }
        }
    }
}
