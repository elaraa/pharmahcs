﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business
{
    public class SalesImportService: BaseService<ImportedFile>
    {
        protected int FileId { get; set; }
        public SalesImportService(int userId, int fileId): base(userId)
        {
            FileId = fileId;
        }
        public fn_GetImportedFileDetails FileHeaderDetails
        {
            get
            {
                return db.Fn_GetImportedFileDetails(FileId);
            }
        }
        public IQueryable<fn_GetImportedFileSummary> FileSummary { get { return db.Fn_GetImportedFileSummary(FileId); } }
        public IQueryable<fn_GetImportedFileMappedAccounts> MappedAccounts { get { return db.Fn_GetImportedFileMappedAccounts(FileId); } }
        public IQueryable<fn_GetImportedFileMappedProducts> MappedProducts { get { return db.Fn_GetImportedFileMappedProducts(FileId); } }
        public IQueryable<vw_ImportedFile_UnmappedProduct> UnmappedProducts { get {
                db.Configuration.LazyLoadingEnabled = false;
                db.Configuration.ProxyCreationEnabled = false;
                return db.vw_ImportedFile_UnmappedProduct.Where(w=>w.FileId == FileId); } }
        public IQueryable<vw_ImportedFile_UnmappedAccount> UnmappedAccounts
        {
            get
            {
                db.Configuration.LazyLoadingEnabled = false;
                db.Configuration.ProxyCreationEnabled = false;
                return db.vw_ImportedFile_UnmappedAccount.Where(w => w.FileId == FileId);
            }
        }
        public IQueryable<vw_ImportedFile_AccountsToCreate> UnmappedAccountsToCreate {
            get
            {
                db.Configuration.LazyLoadingEnabled = false;
                db.Configuration.ProxyCreationEnabled = false;
                return db.vw_ImportedFile_AccountsToCreate.Where(w => w.FileId == FileId);
            }
        }
        public enum ImportAction { Submit, Delete_Full, Delete_Mapping , Cancel}
        public bool Submit(ImportAction action = ImportAction.Submit) {

            try
            {
                int result = 0;
                switch (action)
                {
                    case ImportAction.Submit:
                        result = db.Sp_PushSalesImport(FileId, UserId);
                        break;
                    case ImportAction.Delete_Full:
                        result = db.Sp_SalesImport_Delete(FileId, UserId);
                        break;
                    case ImportAction.Delete_Mapping:
                        result = db.Sp_SalesImport_Delete(FileId, UserId, false);
                        break;
                    case ImportAction.Cancel:
                        var record = db.ImportedFiles.FirstOrDefault(w => w.FileId == FileId);
                        record.StatusId = (int)Sales.ImportedFileStatus.Cancelled;
                        result = db.SaveChanges();
                        break;
                }
                return result > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private bool CanMapRecord(int recordId, out ImportedFileRecord record, out int distId) {
            record = null;
            distId = 0;
            /// check if file is cancelled or deleted or failed
            /// check if recordid belongs to fileid [done]
            /// check if accountid is valid
            var fileHeader = db.ImportedFiles.FirstOrDefault(w => w.FileId == FileId);
            if (fileHeader == null
                || fileHeader.StatusId == (int)Sales.ImportedFileStatus.Deleted_Full
                || fileHeader.StatusId == (int)Sales.ImportedFileStatus.Deleted_withoutMappings
                || fileHeader.StatusId == (int)Sales.ImportedFileStatus.Failed
                || fileHeader.StatusId == (int)Sales.ImportedFileStatus.Saved
                || fileHeader.StatusId == (int)Sales.ImportedFileStatus.Cancelled
                ) return false;
            distId = fileHeader.DistributorId;
            record = db.ImportedFileRecords.FirstOrDefault(w => w.FileRecordId == recordId && w.FileId == FileId);
            if (record == null) return false;
            return true;
        }
        public bool MapAccount(int recordId, int accountId)
        {
            if (!CanMapRecord(recordId, out ImportedFileRecord custRecord, out int distributorId)) return false;

            var records = db.ImportedFileRecords.Where(w=>w.FileId == FileId && w.CustomerCode == custRecord.CustomerCode);
            foreach (var r in records)
            {
                r.AccountId = accountId;
                r.CustomerMapped = true;
                r.InvalidRow = r.InvalidQty || !r.ProductMapped;
            }
            db.DistributorAccountMappings.Add(new DistributorAccountMapping {
                FileId = FileId,
                AccountId = accountId,
                AccountCode = custRecord.CustomerCode,
                AccountName = custRecord.CustomerName,
                AccountAddress = custRecord.CustomerAddress,
                DistributorId = distributorId
            });
            return db.SaveChanges()>0;
        }
        public bool MapProduct(int recordId, int dosageId)
        {
            if (!CanMapRecord(recordId, out ImportedFileRecord prodRecord, out int distributorId)) return false;

            var records = db.ImportedFileRecords.Where(w => w.FileId == FileId && w.DosageFormCode == prodRecord.DosageFormCode);
            foreach (var r in records)
            {
                r.DosageFormId = dosageId;
                r.ProductMapped = true;
                r.InvalidRow = r.InvalidQty || !r.CustomerMapped;
            }
            db.DistributorProductMappings.Add(new DistributorProductMapping
            {
                FileId = FileId,
                DosageFormId = dosageId,
                DosageFormCode = prodRecord.DosageFormCode,
                DosageFormName = prodRecord.DosageFormName,
                DistributorId = distributorId
            });
            return db.SaveChanges() > 0;
        }
        public bool MarkAccountForCreation(int recordId) {
            if (!CanMapRecord(recordId, out ImportedFileRecord accountRecord, out int distributorId)) return false;

            var allcustRecords = db.ImportedFileRecords.Where(w=>w.CustomerMapped == false && w.AccountId == null && w.CustomerCode == accountRecord.CustomerCode);
            foreach(var custRecord in allcustRecords)
                custRecord.AccountToBeCreated = true;

            return db.SaveChanges() > 0;
        }
        public bool UndoAccountMap(int recordId) {
            if (!CanUndoMapping(recordId, out ImportedFileRecord record, out int distributorId)) return false;

            var distMappingRecords = db.DistributorAccountMappings.Where(w=>w.DistributorId == distributorId 
                    && w.FileId == FileId 
                    && w.AccountCode == record.CustomerCode);
            var fileRecords = db.ImportedFileRecords.Where(w=>w.FileId == FileId && w.CustomerCode==record.CustomerCode && w.AccountId == record.AccountId);

            db.DistributorAccountMappings.RemoveRange(distMappingRecords);
            var accountsToDelete = new List<int>();
            foreach (var item in fileRecords)
            {
                if (item.AccountToBeCreated && item.AccountId.HasValue)/*delete the account if possible*/
                    accountsToDelete.Add(item.AccountId.Value);
                item.AccountId = null;
                item.AccountToBeCreated = false;
                item.CustomerMapped = false;
                item.InvalidRow = false;
            }
            var res = db.SaveChanges()>0;

            try { db.Accounts.Where(w => accountsToDelete.Contains(w.AccountId)).DeleteFromQuery(); db.SaveChanges(); } catch { }
            return res;
        }

        public bool UndoAccountCreation(int recordId){
            if (!CanUndoMapping(recordId, out ImportedFileRecord record, out int distributorId)) return false;

            var allcustRecords = db.ImportedFileRecords.Where(w => w.CustomerMapped == false && w.AccountId == null && w.CustomerCode == record.CustomerCode);

            var accountId = record.AccountId;
            foreach (var custRecord in allcustRecords)
                custRecord.AccountToBeCreated = false;

            var res = db.SaveChanges() > 0;
            try { db.Accounts.Where(w => w.AccountId == accountId).DeleteFromQuery(); db.SaveChanges(); } catch { }

            return res;
        }
        private bool CanUndoMapping(int recordId, out ImportedFileRecord record, out int distId) {
            record = null;
            distId = 0;
            /*
             * if fileId is cancelled or deleted or saved => do nothing and exist
             * if saved with territory or under validation => proceed
             */
            var file = db.ImportedFiles.FirstOrDefault(w => w.FileId == FileId);
            if (file.StatusId == (int)Sales.ImportedFileStatus.Cancelled
                || file.StatusId == (int)Sales.ImportedFileStatus.Deleted_Full
                || file.StatusId == (int)Sales.ImportedFileStatus.Deleted_withoutMappings
                || file.StatusId == (int)Sales.ImportedFileStatus.Saved
                ) return false;
            distId = file.DistributorId;
            record = db.ImportedFileRecords.FirstOrDefault(w => w.FileRecordId == recordId && w.FileId == FileId);
            if (record == null) return false;

            return true;
        }
        public bool UndoProductMap(int recordId) {
            if (!CanUndoMapping(recordId, out ImportedFileRecord record, out int distributorId)) return false;

            var distMappingRecords = db.DistributorProductMappings.Where(w => w.DistributorId == distributorId
                    && w.FileId == FileId
                    && w.DosageFormCode == record.DosageFormCode);
            var fileRecords = db.ImportedFileRecords.Where(w => w.FileId == FileId && w.DosageFormCode == record.DosageFormCode && w.DosageFormId == record.DosageFormId);

            db.DistributorProductMappings.RemoveRange(distMappingRecords);
            foreach (var item in fileRecords)
            {
                item.DosageFormId = null;
                item.ProductMapped = false;
                item.InvalidRow = false;
            }
            return db.SaveChanges() > 0;
        }
        public static List<Sales.ImportedFileStatus> GetActions(Sales.ImportedFileStatus status) {
            var actionsLst = new List<Sales.ImportedFileStatus>();
            switch (status)
            {
                case Sales.ImportedFileStatus.UnderValidation:
                    actionsLst.AddRange(new[] {
                        Sales.ImportedFileStatus.Cancelled,
                        Sales.ImportedFileStatus.Deleted_Full,
                        Sales.ImportedFileStatus.Deleted_withoutMappings,
                        Sales.ImportedFileStatus.Saved
                    });
                    break;
                case Sales.ImportedFileStatus.SavedbyTerritoryOnly:
                    actionsLst.AddRange(new[] {
                        Sales.ImportedFileStatus.Cancelled,
                        Sales.ImportedFileStatus.Deleted_Full,
                        Sales.ImportedFileStatus.Deleted_withoutMappings,
                        Sales.ImportedFileStatus.Saved
                    });
                    break;
                case Sales.ImportedFileStatus.Saved:
                    actionsLst.AddRange(new[] {
                        Sales.ImportedFileStatus.Deleted_Full,
                        Sales.ImportedFileStatus.Deleted_withoutMappings
                    });
                    break;
                case Sales.ImportedFileStatus.Deleted_Full:
                case Sales.ImportedFileStatus.Deleted_withoutMappings:
                case Sales.ImportedFileStatus.Failed:
                case Sales.ImportedFileStatus.Cancelled:
                default:
                    break;
            }
            return actionsLst;
        }

        public bool CreateAndMapAccount(int recordId, string accountCode, string accountName, string accountAddress, int accountTerr, int accountCategory, int accountType)
        {
            if (!CanMapRecord(recordId, out ImportedFileRecord record, out int distributorId)) return false;

            ///if there is an account attached to this record return false
            if (record.AccountId.HasValue) return false;
            ///save this account into db
            var countryId = db.ImportedFiles.FirstOrDefault(w => w.FileId == FileId).CountryId;
            var account = new Account
            {
                AccountCode = accountCode,
                AccountName = accountName,
                Address = accountAddress,
                TerritoryId = accountTerr,
                CategoryId = accountCategory,
                AccountTypeId = accountType,
                Approved = true,
                Active = true,
                CountryId = countryId
            };
            try
            {
                db.Accounts.Add(account);
                db.SaveChanges();
            }
            catch
            {
                return false;
            }
            ///get the id
            ///update all records with this code => AccountId = created ID, AccountToBeCreated = tru, CustomerMapped = true
            db.DistributorAccountMappings.Add(new DistributorAccountMapping { AccountId = account.AccountId, AccountCode =  account.AccountCode, AccountName = account.AccountName, AccountAddress= account.Address, DistributorId = distributorId, FileId = FileId});
            var accountRecords = db.ImportedFileRecords.Where(w => w.FileId == FileId && w.CustomerCode == record.CustomerCode && w.CustomerMapped == false);
            foreach (var rec in accountRecords)
            {
                rec.AccountId = account.AccountId;
                rec.CustomerMapped = 
                rec.AccountToBeCreated = true;
            }
            return db.SaveChanges()>0;
            
        }

        public static List<Sales.ImportedFileStatus> GetActions(int statusId) => GetActions((Sales.ImportedFileStatus)statusId);

        public static bool CanMapAccount(Sales.ImportedFileStatus status) => status == Sales.ImportedFileStatus.UnderValidation || status == Sales.ImportedFileStatus.SavedbyTerritoryOnly ? true : false;
        public static bool CanMapAccount(int statusId) => CanMapAccount((Sales.ImportedFileStatus)statusId);

        public static bool CanMapProduct(Sales.ImportedFileStatus status) => status== Sales.ImportedFileStatus.UnderValidation?true:false;
        public static bool CanMapProduct(int statusId) => CanMapProduct((Sales.ImportedFileStatus)statusId);

    }
}
