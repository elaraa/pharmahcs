﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;
using System.Threading.Tasks;

using System.Reflection;
using System.IO;
using Sales.Core.ImportFile;

namespace Services.Business.Sales
{
    public enum ImportedFileStatus { UnderValidation = 1, SavedbyTerritoryOnly , Saved , Deleted_Full, Deleted_withoutMappings, Failed, Cancelled }
    public class SalesImportEngine
    {
        protected DBModel db;
        private int _UserId { get; set; }
        private string BaseUri { get; set; }
        public SalesImportEngine(int userId, string baseUri)
        {
            _UserId = userId;
            BaseUri = baseUri;
            db = new DBModel
            {
                UserId = _UserId
            };
        }
        protected PluginBase CreatePlugin(int pluginId)
        {

            try
            {
                var plugin = new BaseService<ImportPlugin>(_UserId).GetOne(w => w.PluginId == pluginId, true);
                if (plugin == null) throw new Exception("Plugin does not exist");
                // dynamically load assembly from file Test.dll
                var pluginAssembly = Assembly.LoadFile(string.Format("{0}{1}", BaseUri, plugin.DllPath));///convert path from relative to absolute
                // get type of class plugin from loaded assembly
                // create instance of class plugin
                var plgnConstructor = pluginAssembly.GetType(plugin.ClassName)
                    .GetConstructor(new Type[] { });
                return plgnConstructor.Invoke(null) as PluginBase;
                
                //construct the class and return it
                //return plgnConstructor.Invoke(new object[] { files }) as PluginBase;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public int Import(int distributorId, int timedefinitionId, PluginInput[] files, string importMode = "O") {
            ///1. create record in ImportedFile 
            ///2. plugin.validate
            var dist = db.Distributors.FirstOrDefault(w => w.DistributorId == distributorId);
            if (dist == null || !dist.SalesPluginId.HasValue) return -1;
            ///3. update status
            var plugin = CreatePlugin(dist.SalesPluginId.Value);
            var header = new ImportedFile
            {
                DistributorId = distributorId,
                CountryId = 1,
                PluginId = dist.SalesPluginId.Value,
                //StatusId = (int)ImportedFileStatus.UnderValidation,
                ImportMode = importMode,
                TimeDefinitionId = timedefinitionId,
                FileName = PluginInput.GetFilesNames(files)
            };
            if (plugin == null) {
                header.StatusId = (int)ImportedFileStatus.Failed;
                header.ErrorDescription = "Failed to load plugin";
                db.ImportedFiles
                .Add(header);
                db.SaveChanges();
                return header.FileId;
            }
            plugin.ProcessData(files);
            switch (plugin.ValidateFiles())
            {
                case ValidationStatus.Succeeded:
                    header.StatusId = (int)ImportedFileStatus.UnderValidation;
                    break;
                case ValidationStatus.InvalidFormat:
                    header.StatusId = (int)ImportedFileStatus.Failed;
                    header.ErrorDescription = "Invalid file format";
                    break;
                case ValidationStatus.WrongFileType:
                    header.StatusId = (int)ImportedFileStatus.Failed;
                    header.ErrorDescription = "Wrong file format";
                    break;
                default:
                    break;
            }
            db.ImportedFiles
                .Add(header);
            db.SaveChanges();
            if (plugin.ValidateFiles() != ValidationStatus.Succeeded) return header.FileId;
            ///4. plugin.getdata
            ///5. insert records in db
            var records = ConvertRecords(header.FileId, plugin.FileRecords);
            //db.ImportedFileRecords.BulkInsert(records); /*TODO: check EF extension license*/
            db.ImportedFileRecords.AddRange(records);
            db.SaveChanges();
            ///4. run stored procedure for mapping
            db.Sp_ProcessSalesImportedFile(header.FileId, _UserId);
             ///5. return id of the importedfile
            return header.FileId;
        }
       
        private ImportedFileRecord ConvertRecord(int fileId, PluginRecord record) {///add validation for null param
            return record == null
                ? new ImportedFileRecord()
                : new ImportedFileRecord
            {
                FileId = fileId,
                CustomerCode = record.CustomerCode,
                CustomerName = record.CustomerName,
                CustomerAddress = record.CustomerAddress,
                CustomerTerritory = record.CustomerTerritory,
                DosageFormCode = record.DosageFormCode,
                DosageFormName = record.DosageFormName,
                FileAmount = record.FileAmount,
                FileQuantity = record.FileQuantity,
                Quantity = record.Quantity,
                RawRecord = record.RawRecord,
                SalesDate = record.SalesDate,
                InvoiceNumber = record.InvoiceNumber,
                InvalidQty = record.InvalidQty,
                InvalidRow = record.InvalidRow,
                DistributorBranchCode = record.DistributorBranchCode,
                DistributorBranchName = record.DistributorBranchName,
                CustomerMapped = false,
                ProductMapped = false,
                CreatedById = _UserId,
                CreationDate = DateTime.Now
                

            };
        }
        private ImportedFileRecord[] ConvertRecords(int fileId, PluginRecord[] records) {///add validation for null param
            return records == null ? (new ImportedFileRecord[] { }) : records.Select(s => ConvertRecord(fileId, s)).ToArray();
        }
        private List<ImportedFileRecord> ConvertRecords(int fileId, List<PluginRecord> records){///add validation for null param
            return records == null ? new List<ImportedFileRecord>() : records.Select(s => ConvertRecord(fileId, s)).ToList();
        }

    }

}
