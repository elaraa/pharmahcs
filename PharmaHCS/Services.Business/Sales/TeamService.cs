﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.Sales
{
    public class TeamService : BaseService<Team>
    {
        public TeamService(int userId) : base(userId)
        {
        }
        public RunResult IsOverlapped(TeamSalesProduct teamSalesProduct) {
            try {
                 if(Get<TeamSalesProduct>(c=> c.TeamSalesProductId != teamSalesProduct.TeamSalesProductId && 
                                c.ProductId == teamSalesProduct.ProductId && c.TeamId == teamSalesProduct.TeamId
                                   && ((c.ToDate.HasValue ? teamSalesProduct.FromDate <= c.ToDate.Value : true)  &&
                                      (teamSalesProduct.ToDate.HasValue ? teamSalesProduct.ToDate.Value >= c.FromDate : true))).Any())
                     return new RunResult { Succeeded = false, ErrorDesc = PH.Message.DateOverlapping };
                 else
                     return new RunResult { Succeeded = true };
            }
            catch (Exception e)
            {
                return new RunResult { Succeeded = false, ErrorDesc = e.Message };
            }
        }

        public RunResult IsOverlapped(Team team)
        {
            try
            {
                if (Get(c => c.TeamId != team.TeamId && (team.TeamCode != null ? c.TeamCode == team.TeamCode : false)).Any())
                    return new RunResult { Succeeded = false, ErrorDesc = string.Format(PH.Message.DuplicatedData, PH.Team.TeamCode) };
                else if ( Get(c => c.TeamId != team.TeamId && c.TeamName == team.TeamName).Any())
                    return new RunResult { Succeeded = false, ErrorDesc = string.Format(PH.Message.DuplicatedData, PH.Team.TeamName) }; 
                else
                    return new RunResult { Succeeded = true };
            }
            catch (Exception e)
            {
                
                return new RunResult { Succeeded = false, ErrorDesc = e.Message };
            }
        }
    }
}
