﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Services.Business
{
    public enum TargetType
    {
        Country = 1,
        Product,
        ProductPhasing,
        Team,
        Employee,
        Profile
    }
    public class TargetService : BaseService<Target>
    {
        public TargetService(int userId) : base(userId)
        {
        }

        public override int Delete(Expression<Func<Target, bool>> predicate)
        {
            var list = db.Targets.Where(predicate).Select(s=>s.TargetId).ToList();
            //return (from t in db.Targets
            //        join sel in list on
            //        new { typeid = t.TargetTypeId, cid = t.CountryId, year = t.Year } equals
            //        new { typeid = sel.TargetTypeId, cid = sel.CountryId, year = sel.Year }
            //        select t
            //            ).DeleteFromQuery();
            int totalRecords = 0;
            foreach (var item in list)
            {
                totalRecords += db.Sp_DeleteTarget(item);
            }
            return totalRecords;
        }


        #region Product Target
        public ImportResult ImportProductTarget(int year, int countryId, DataSet dataSet)
        {
            ImportResult result = new ImportResult();

            if (dataSet.Tables.Count < 1) { result.ErrorList.Add("Invalid Format, file does not contain valid columns"); return result; }
            //validate column names
            if (ValidateProductTargetFileColumns(dataSet.Tables[0].Columns, year - 1))
            {
                if (dataSet.Tables[0].Rows.Count < 1) { result.ErrorList.Add("File does not contain records"); return result; }

                var targetProduct = new List<Target>();
                var products = db.Products.Where(w => w.CountryId == countryId).ToList();
                var prodPrice = (from pp in db.vw_target_ProductPrice
                                 join p in db.Products on pp.ProductId equals p.ProductId
                                 where pp.year == year && p.CountryId == countryId
                                 select pp).ToList();

                foreach (DataRow row in dataSet.Tables[0].Rows)
                {
                    if (ValidProductTargetImportRow(row, year, ref products, out List<string> errors))
                        targetProduct.Add(ProcessProductTargetImportRow(row, ref prodPrice, countryId, year));
                    else result.ErrorList.AddRange(errors);
                }
                ///save it to target
                if (result.ErrorList.Count > 0) return result;
                var tt = (int)TargetType.Product;
                db.Targets.Where(w => w.TargetTypeId == tt && w.CountryId == countryId && w.Year == year).DeleteFromQuery();

                foreach (var item in targetProduct)
                    db.Targets.Add(item);

                result.Succeeded = db.SaveChanges() > 0;
                result.ResultId = 1;
            }
            else { result.ErrorList.Add("Invalid Format, column names does not match"); }

            return result;
        }

        private bool ValidProductTargetImportRow(DataRow row, int year, ref List<Product> products, out List<string> errors)
        {
            errors = new List<string>();
            if (row["ProductId"].ToString().IsEmpty())
                errors.Add(string.Format("Row {0}: ProductId is empty", row[0]));
            if (!int.TryParse(row["ProductId"].ToString(), out int pid))
                errors.Add(string.Format("Row {0}: ProductId is not a number '{1}'", row[0], row["ProductId"]));
            else if (products.FirstOrDefault(w => w.ProductId == pid) == null)
                errors.Add(string.Format("Row {0}: ProductId does not exist", row[0]));

            if (!decimal.TryParse(row[string.Format("{0} U.", year)].ToString(), out decimal val))
                errors.Add(string.Format("Row {0}: Target Units is not a valid number '{0}'", row[0], row[string.Format("{0} U.", year)]));

            return errors.Count < 1;
        }

        private Target ProcessProductTargetImportRow(DataRow row, ref List<vw_target_ProductPrice> productPrices, int country, int year)
        {
            var pid = Convert.ToInt32(row["ProductId"]);
            var pp = productPrices.FirstOrDefault(w => w.ProductId == pid);
            var q = Convert.ToDecimal(row[string.Format("{0} U.", year)]);

            return new Target
            {
                CountryId = country,
                Year = year,
                TargetTypeId = (int)TargetType.Product,
                ProductId = pid,
                Qty = q,
                Amount = pp == null ? 0 : q * pp.Price
            };
        }

        private bool ValidateProductTargetFileColumns(DataColumnCollection columns, int year)
        {
            var fcols = new string[] { "Line", "Group", "ProductId", "Product", "X.F Price"
                , string.Format("FCT{0} Unit",year)
                , string.Format("FCT{0} Value", year)
                , string.Format("{0} U.", year + 1)
                , string.Format("{0} V.", year + 1)};
            foreach (var item in fcols)
            {
                if (!columns.Contains(item))
                    return false;
            }
            return true;
        }

        public DataTable ExportProductTarget(DateTime date, int countryId)
        {
            var table = new DataTable("ProductTarget");
            table.Columns.Add("Line");
            table.Columns.Add("Group");
            table.Columns.Add("ProductId");
            table.Columns.Add("Product");
            table.Columns.Add("X.F Price");
            table.Columns.Add("LastYearQty");
            table.Columns.Add("LastYearAmount");
            table.Columns.Add("CurrentYearQty");
            table.Columns.Add("CurrentYearAmount");

            foreach (var item in db.Fn_TargetProduct_Export(date, countryId).ToList())
                table.Rows.Add(item.Line, item.Group, item.ProductId, item.Product, item.Price, item.LastYearQty, item.LastYearAmount, item.CurrentYearQty, item.CurrentYearAmount);

            //modify columns
            table.Columns["LastYearQty"].ColumnName = string.Format("FCT{0} Unit", date.Year - 1);
            table.Columns["LastYearAmount"].ColumnName = string.Format("FCT{0} Value", date.Year - 1);
            table.Columns["CurrentYearQty"].ColumnName = string.Format("{0} U.", date.Year);
            table.Columns["CurrentYearAmount"].ColumnName = string.Format("{0} V.", date.Year);
            //return
            return table;

        }
        #endregion

        #region Product Phasing
        public ImportResult ImportProductPhasing(int year, int countryId, DataSet dataSet)
        {
            ImportResult result = new ImportResult();
            var periodsFull = db.TimeDefinitions.Where(w => w.Year == year).OrderBy(o => o.TimeDefinitionId).ToList();
            var periods = periodsFull.OrderBy(o => o.TimeDefinitionId).Select(s => s.PeriodName).ToList();

            if (dataSet.Tables.Count < 1) { result.ErrorList.Add("Invalid Format, file does not contain valid columns"); return result; }
            //validate column names
            if (ValidateProductPhasingFileColumns(dataSet.Tables[0].Columns, ref periods))
            {
                if (dataSet.Tables[0].Rows.Count < 1) { result.ErrorList.Add("File does not contain records"); return result; }

                var targetProduct = new List<Target>();
                var products = db.Products.Where(w => w.CountryId == countryId && w.Active == true).ToList();

                foreach (DataRow row in dataSet.Tables[0].Rows)
                {
                    if (ValidProductPhasingImportRow(row, ref products, ref periods, out List<string> errors))
                        targetProduct.AddRange(ProcessProductPhasingImportRow(row, ref periodsFull, countryId, year));
                    else result.ErrorList.AddRange(errors);
                }
                ///save it to target
                if (result.ErrorList.Count > 0) return result;
                var tt = (int)TargetType.ProductPhasing;
                db.Targets.Where(w => w.TargetTypeId == tt && w.CountryId == countryId && w.Year == year).DeleteFromQuery();

                foreach (var item in targetProduct)
                    db.Targets.Add(item);

                result.Succeeded = db.SaveChanges() > 0;
                result.ResultId = 1;
            }
            else { result.ErrorList.Add("Invalid Format, column names does not match"); }

            return result;
        }

        private bool ValidProductPhasingImportRow(DataRow row, ref List<Product> products, ref List<string> periods, out List<string> errors)
        {
            errors = new List<string>();
            if (row["ProductId"].ToString().IsEmpty())
                errors.Add(string.Format("Row {0}: ProductId is empty", row[0]));
            if (!int.TryParse(row["ProductId"].ToString(), out int pid))
                errors.Add(string.Format("Row {0}: ProductId is not a number '{1}'", row[0], row["ProductId"]));
            else if (products.FirstOrDefault(w => w.ProductId == pid) == null)
                errors.Add(string.Format("Row {0}: ProductId does not exist", row[0]));

            float totalPercentage = 0;
            foreach (var item in periods)
                if (!row[item].ToString().IsEmpty())
                {
                    if (!float.TryParse(row[item].ToString(), out var val))
                        errors.Add(string.Format("Row {0}: {1} is not a valid number '{2}'", row[0], item, row[item]));
                    else
                        totalPercentage += val;
                }

            if (totalPercentage < 0.995 || totalPercentage > 1.01)
                errors.Add(string.Format("Row {0}: {1} total percetages does not complete 100%", row[0], row["ProductName"]));

            return errors.Count < 1;
        }

        private List<Target> ProcessProductPhasingImportRow(DataRow row, ref List<TimeDefinition> periods, int country, int year)
        {
            var result = new List<Target>();
            var pid = Convert.ToInt32(row["ProductId"]);

            foreach (var item in periods)
            {
                if (row[item.PeriodName].ToString().IsEmpty()) continue;
                decimal.TryParse(row[item.PeriodName].ToString(), out decimal perc);
                result.Add(new Target
                {
                    ProductId = pid,
                    CountryId = country,
                    TargetTypeId = (int)TargetType.ProductPhasing,
                    TimeDefinitionId = item.TimeDefinitionId,
                    Percentage = perc,
                    Year = year
                });
            }
            return result;
        }

        private bool ValidateProductPhasingFileColumns(DataColumnCollection columns, ref List<string> periods)
        {
            foreach (var item in periods)
            {
                if (!columns.Contains(item))
                    return false;
            }

            return columns.Contains("ProductId") && columns.Contains("ProductName");
        }

        public DataTable ExportProductPhasing(int year, int countryId)
        {
            var qry = db.Fn_TargetProductPhasing_Export(year, countryId).ToList();
            var periods = db.TimeDefinitions.Where(w => w.Year == year).OrderBy(o => o.TimeDefinitionId).Select(s => s.PeriodName).ToList();
            var products = qry.Select(s => new { pid = s.ProductId, pname = s.ProductName }).Distinct();


            var table = new DataTable();
            table.Columns.Add("ProductId"); table.Columns.Add("ProductName");

            foreach (var item in periods)
                table.Columns.Add(item);

            foreach (var item in products)
            {
                DataRow row = table.NewRow();
                row[0] = item.pid;
                row[1] = item.pname;
                foreach (var p in periods)
                {
                    var val = qry.FirstOrDefault(w => w.ProductId == item.pid && w.PeriodName == p);
                    row[p] = val?.Percentage;
                }
                table.Rows.Add(row);
            }

            return table;

        }
        //public DataTable ToDataTable<T>(List<T> items)
        //{
        //    DataTable dataTable = new DataTable(typeof(T).Name);
        //    //Get all the properties by using reflection   
        //    var Props = typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
        //    foreach (var prop in Props)
        //    {
        //        //Setting column names as Property names  
        //        dataTable.Columns.Add(prop.Name);
        //    }
        //    foreach (T item in items)
        //    {
        //        var values = new object[Props.Length];
        //        for (int i = 0; i < Props.Length; i++)
        //        {

        //            values[i] = Props[i].GetValue(item, null);
        //        }
        //        dataTable.Rows.Add(values);
        //    }

        //    return dataTable;
        //}
        #endregion

        #region Profile/Territory/Product Target
        public DataTable ExportProfileProductTerritory(int timeId, int countryId)
        {
            var period = db.TimeDefinitions.FirstOrDefault(w => w.TimeDefinitionId == timeId);
            if (period == null) return null;
            var data = db.sp_Target_GetTargetProfileSheet(UserId, period.FromDate).ToList();

            var table = ConvertToDataTable(data);

            table.Columns["Qty_Lastyear2"].ColumnName = string.Format("{0} Q.", period.Year - 2);
            table.Columns["Amount_Lastyear2"].ColumnName = string.Format("{0} V.", period.Year - 2);
            table.Columns["Qty_Lastyear1"].ColumnName = string.Format("{0} Q.", period.Year - 1);
            table.Columns["Amount_Lastyear1"].ColumnName = string.Format("{0} V.", period.Year - 1);
            table.Columns["Qty_Currentyear"].ColumnName = string.Format("YTD {0} Q", period.Year);
            table.Columns["Amount_Currentyear"].ColumnName = string.Format("YTD {0} V.", period.Year);
            table.Columns["Qty_Currentyear_estimate"].ColumnName = string.Format("{0} Est.Q", period.Year);
            table.Columns["Amount_Currentyear_estimate"].ColumnName = string.Format("{0} Est.V.", period.Year);

            table.Columns["MarketShare"].ColumnName = "IMS Share";
            table.Columns["Sales_Office"].ColumnName = "Office share";
            table.Columns["Line_Sales"].ColumnName = "Line Share";
            table.Columns["Product_Sales"].ColumnName = "Product share";

            return table;
        }
        public static DataTable ConvertToDataTable<T>(IList<T> data)
        {
            System.ComponentModel.PropertyDescriptorCollection properties =
               System.ComponentModel.TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (System.ComponentModel.PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (System.ComponentModel.PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }
        public ImportResult ImportProfileProductTerritoryTarget(int fromTimeDefId, int TargetTimeDefId, int countryId, DataSet dataSet)
        {
            ImportResult result = new ImportResult();

            ///insert data into temp table x
            //upload and validate in database
            //if successful push into target table
            //returnt the result

            if (dataSet.Tables.Count < 1) { result.ErrorList.Add("Invalid Format, file does not contain valid columns"); return result; }
            if (dataSet.Tables[0].Rows.Count < 1) { result.ErrorList.Add("File does not contain records"); return result; }

            // 1. prepare the table to only have 3 columns
            var dt = new DataTable();
            dt.Columns.Add("AutoID"); dt.Columns.Add("ProductId"); dt.Columns.Add("ProfileId"); dt.Columns.Add("tgt_qty");
            foreach (DataRow row in dataSet.Tables[0].Rows)
                dt.Rows.Add(row["AutoID"], row["ProductId"], row["ProfileId"], row["tgt_qty"]);

            //int i = 0;
            //while (i!=dataSet.Tables[0].Columns.Count)
            //{
            //    DataColumn column = dataSet.Tables[0].Columns[i];
            //    if (!new[] { "autoid", "product", "area", "tgt_qty" }.Contains(column.ColumnName.ToLower()))
            //        dataSet.Tables[0].Columns.Remove(column);
            //    else i++;
            //}

            var res = new MasterDataUpload(UserId).UploadFile(dt, MasterDataType.Target_ProfileTerritoryProduct,
                "4",
                countryId.ToString(),
                TargetTimeDefId.ToString());
            result.Succeeded = res.Status == MasterDataUpload.UploadStatus.Success;

            switch (res.Status)
            {
                case MasterDataUpload.UploadStatus.FileIsEmpty:
                    result.ErrorList.Add("File does not contain records");
                    break;
                case MasterDataUpload.UploadStatus.InvalidFileHeader:
                    result.ErrorList.Add("Invalid Format, file does not contain valid columns");
                    break;
                case MasterDataUpload.UploadStatus.dbError_FailedtoImportData:
                    result.ErrorList.Add("Failed to Import the file");
                    break;
                case MasterDataUpload.UploadStatus.DataContainsErrors:
                    StringBuilder output;
                    foreach (DataRow row in res.failedReasons.Rows)
                    {
                        output = new StringBuilder();
                        foreach (DataColumn col in res.failedReasons.Columns)
                            output.AppendFormat("{0}:{1} ", col.ColumnName, row[col]);
                        result.ErrorList.Add(output.ToString());
                    }
                    break;
            }

            return result;
        }
        public IQueryable<fn_Target_Profile_SupervisorSummary> GetTargetProfile_SupervisorSummary(int year, int countryId) => db.fn_Target_Profile_SupervisorSummary(year, UserId, countryId);

        #endregion

    }
}
