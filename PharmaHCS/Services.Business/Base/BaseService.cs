﻿
using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using LinqKit;
using Services.Business.Sales;

namespace Services.Business
{
    public class BaseService<TEntity> : IDisposable
      where TEntity : class
    {
        protected DBModel db;
        private bool disposed = false;// to detect redundant calls
        private SecurityContext<TEntity> securityContext;
        public int UserId { get => db.UserId; set => db.UserId = value; }

        public BaseService(int userId = int.MinValue)
        {
            db = new DBModel
            {
                UserId = userId
            };
            db.Database.CommandTimeout = 0;
            securityContext = new SecurityContext<TEntity>(userId, ref db);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing) if (db != null) db.Dispose();

                // new shared cleanup logic
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(db);
        }

        public virtual IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> whereClause = null, bool safe = false, params string[] childrenToInclude)
      
        {
            db.Configuration.LazyLoadingEnabled = !safe;
            db.Configuration.ProxyCreationEnabled = !safe;

            return SecureRetrieve(whereClause, childrenToInclude);

        }

        public virtual IQueryable<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> whereClause = null, bool safe = false, params string[] childrenToInclude)
         where TEntity : class
        {
            db.Configuration.LazyLoadingEnabled = !safe;
            db.Configuration.ProxyCreationEnabled = !safe;

            return SecureRetrieve(whereClause,childrenToInclude);

        }
        private IQueryable<TEntity> SecureRetrieve<TEntity>(Expression<Func<TEntity, bool>> whereClause, params string[] childrenToInclude)
          where TEntity : class
        {
            var entity = db.Set<TEntity>();
            IQueryable<TEntity> result = null;
            foreach (var child in childrenToInclude)
                result = (result??entity).Include(child);

            var predicate = PredicateBuilder.New<TEntity>(true);
            if (whereClause != null)
                predicate = predicate.And(whereClause);

            if (securityContext.GetContraints() is Expression<Func<TEntity, bool>> securityWhere)
                predicate = predicate.And(securityWhere);
            
            return (result ?? entity).AsExpandable().Where(predicate);
        }
        public virtual TEntity GetOne(Expression<Func<TEntity, bool>> whereClause, bool safe = false,params string[] childrenToInclude)
        {
            db.Configuration.LazyLoadingEnabled = !safe;
            db.Configuration.ProxyCreationEnabled = !safe;

            return SecureRetrieve(whereClause,childrenToInclude).FirstOrDefault();
        }
        public virtual TEntity GetOne<TEntity>(Expression<Func<TEntity, bool>> whereClause, bool safe = false, params string[] childrenToInclude)
         where TEntity : class
        {
            db.Configuration.LazyLoadingEnabled = !safe;
            db.Configuration.ProxyCreationEnabled = !safe;

            return SecureRetrieve(whereClause, childrenToInclude).FirstOrDefault();
        }
        public virtual int Delete(Expression<Func<TEntity, bool>> predicate)
        {
            /*
            IQueryable<TEntity> item = db.Set<TEntity>().Where(predicate);
            db.Set<TEntity>().RemoveRange(item);
            */
            SecureRetrieve(predicate).DeleteFromQuery();

            return db.SaveChanges();
        }
        public virtual int Delete<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            SecureRetrieve<T>(predicate).DeleteFromQuery();
            return db.SaveChanges();
        }
        public virtual T AddOrUpdate<T>(T entity) where T : class
        { 
            db.Set<T>().AddOrUpdateExtension(entity);
            int res = db.SaveChanges();
            return entity;
        }
        public virtual bool Any<T>(Expression<Func<T, bool>> whereClause) where T : class
        {
            var isExist = db.Set<T>().Any(whereClause);
            return isExist;
        }
        public virtual RunResult AddOrUpdate(TEntity e, bool isCreate) 
        {
            try
            {
                db.Set<TEntity>().AddOrUpdateExtension(e);
                int res = db.SaveChanges();
                return new RunResult { Succeeded = true, SuccessDesc = "Done Successfully" };
            }
            catch (Exception ex)
            {
                return new RunResult { ErrorDesc = "DB Error" };
            }

        }
        public virtual bool Update(TEntity obj, params Expression<Func<TEntity, object>>[] propertiesToUpdate)
        {
            db.Set<TEntity>().Attach(obj);
            Array.ForEach(propertiesToUpdate, p => db.Entry(obj).Property(p).IsModified = true);
            return db.SaveChanges() > 0;
        }
        public virtual int Update(List<TEntity> l, params Expression<Func<TEntity, object>>[] propertiesToUpdate)
        {
            foreach (TEntity t in l)
            {
                db.Set<TEntity>().Attach(t);
                Array.ForEach(propertiesToUpdate, p => db.Entry(t).Property(p).IsModified = true);
            }
            return db.SaveChanges();
        }

        public virtual bool Add(TEntity e, bool delaySave = false)
        {
            db.Set<TEntity>().Add(e);
            if (!delaySave) return db.SaveChanges() > 0;
            return true;
        }
        
        public virtual void Add(IEnumerable<TEntity> list, bool delaySave = false)
        {
            db.BulkInsert(list);
            //if (!delaySave) db.SaveChanges();
        } 
       
    }

    public class SecurityContext<TEntity>
        where TEntity : class
    {
        public int UserId { get; set; }
        private int? CountryId { get; set; }
        private DBModel DB { get; set; }
        public SecurityContext(int userId,ref DBModel dB )
        {
            UserId = userId;
            ///Get CountryId of Employee
            CountryId = dB.Employees.FirstOrDefault(w=>w.EmployeeId == userId).CountryId;
            //get countryId of user
            DB = dB;

        }
        public LambdaExpression GetContraints()
        {
            ///TODO: Add the extra conditions
            if (typeof(TEntity) == typeof(SystemRole))  { return CountryId.HasValue?(Expression<Func<SystemRole,bool>>)(w=>w.CountryId == CountryId):null; }
            else if(typeof(TEntity) ==typeof(vw_Employee) ) {
                var employees = DB.fn_Security_GetEmployeesForEmployee(UserId,DateTime.Now).ToList();
                return CountryId.HasValue?(Expression<Func<vw_Employee, bool>>)(w => w.CountryId == CountryId && (employees.Contains(w.EmployeeId) || w.EmployeeId == UserId) ):null; }
            else if (typeof(TEntity) == typeof(vw_ImportedFile)) { return CountryId.HasValue ? (Expression<Func<vw_ImportedFile, bool>>)(w => w.CountryId == CountryId) : null; }
            else if (typeof(TEntity) == typeof(vw_Account)) { return CountryId.HasValue ? (Expression<Func<vw_Account, bool>>)(w => w.CountryId == CountryId) : null; }
            else if (typeof(TEntity) == typeof(vw_MarketShare)) { return CountryId.HasValue ? (Expression<Func<vw_MarketShare, bool>>)(w => w.CountryId == CountryId) : null; }
            else if (typeof(TEntity) == typeof(vw_AccountCategory)) { return CountryId.HasValue ? (Expression<Func<vw_AccountCategory, bool>>)(w => w.CountryId == CountryId) : null; }
            else if (typeof(TEntity) == typeof(vw_AccountType)) { return CountryId.HasValue ? (Expression<Func<vw_AccountType, bool>>)(w => w.CountryId == CountryId) : null; }
            else if (typeof(TEntity) == typeof(vw_ProductPrice)) { return CountryId.HasValue ? (Expression<Func<vw_ProductPrice, bool>>)(w => w.CountryId == CountryId) : null; }
            else if (typeof(TEntity) ==typeof(vw_Distributor)) { return CountryId.HasValue ? (Expression<Func<vw_Distributor, bool>>)(w => w.CountryId == CountryId) : null; }
            else if (typeof(TEntity) == typeof(vw_Product)) { return CountryId.HasValue ? (Expression<Func<vw_Product, bool>>)(w => w.CountryId == CountryId) : null; }
            else if (typeof(TEntity) == typeof(vw_Team)) {
                var teams = DB.fn_Security_GetProfilesForEmployee(UserId, DateTime.Now).Select(s => s.TeamId).Distinct().ToList();
                return CountryId.HasValue ? (Expression<Func<vw_Team, bool>>)(w => w.CountryId == CountryId && teams.Contains(w.TeamId)) : null; }
            else if (typeof(TEntity) == typeof(vw_Territory)) { return CountryId.HasValue ? (Expression<Func<vw_Territory, bool>>)(w => w.CountryId == CountryId) : null; }
            else if (typeof(TEntity) == typeof(vw_Territory)) { return CountryId.HasValue ? (Expression<Func<vw_Territory, bool>>)(w => w.CountryId == CountryId) : null; }
            else if (typeof(TEntity) == typeof(vw_Country)) { return CountryId.HasValue ? (Expression<Func<vw_Country, bool>>)(w => w.CountryId == CountryId) : null; }


            else if (typeof(TEntity) == typeof(Country)) { return CountryId.HasValue ? (Expression<Func<Country, bool>>)(w => w.CountryId == CountryId) : null; }
            else if (typeof(TEntity) == typeof(AccountCategory)) { return CountryId.HasValue ? (Expression<Func<AccountCategory, bool>>)(w => w.CountryId == CountryId) : null; }
            else if (typeof(TEntity) == typeof(AccountType)) { return CountryId.HasValue ? (Expression<Func<AccountType, bool>>)(w => w.CountryId == CountryId) : null; }
            else if (typeof(TEntity) == typeof(vw_ProfileTargetVisit)) {
                var employees = DB.fn_Security_GetEmployeesForEmployee(UserId, DateTime.Now).ToList();
                return CountryId.HasValue ? (Expression<Func<vw_ProfileTargetVisit, bool>>)(w => w.CountryId == CountryId && (employees.Contains(w.EmployeeId) || w.EmployeeId == UserId)) : null;
            }

            return null;
        }
       /*
       ProfileAccount
       ProfileTerritory
       Employee
       EmployeeHistory
       EmployeeVacation
       JobTitle
       Distributor
       DistributorAccountMapping
       DistributorBranch
       DistributorProductMapping
       ImportedFile
       ImportedFileRecord
       ImportedFileStatu
       InMarketSale
       MarketShare
       MarketShareTerritory
       ProductPrice
       ProfileAccount1
       ProfileTerritory1
       Target
       TeamSalesProduct
       Account
       AccountCategory
       AccountType
       
       DosageFormUnit
       EmployeeProfile
       Product
       ProductDosageForm
       ProductGroup
       Team
       TeamProfile
       Territory
       TimeDefinition
       InMarketSalesByEmployeeByProfile
       
       vw_EmployeeHistory
       
       
       vw_MarketShareTerritory
       
       vw_target_ProductPrice
       vw_targetPhasing_Details
       vw_TargetProduct
       vw_TargetProduct_Details
       vw_TargetProductPhasing
       vw_UnmappedAccount
       vw_UnmappedProduct
       
       
       
       
       
       vw_DosageForm
       vw_DosageFormUnit
       vw_EmployeeProfile
       
       vw_ProductGroup
       
       vw_TeamProfile
       */
       
    }
}
