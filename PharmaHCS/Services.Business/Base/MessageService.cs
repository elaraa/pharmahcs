﻿using Services.Business.Sales;


namespace Services.Business.Base
{
    public static class MessageService
    {
        public static RunResult ShowSuccessMessage(bool isCreate = false)
        {
            if (isCreate)
                return new RunResult { Succeeded = true, SuccessDesc = PH.Message.SuccessCode };
            else
                return new RunResult { Succeeded = true, SuccessDesc = PH.Message.SuccessEditCode };
        }
    }
}
