﻿using DB.ORM.DB;
using System.Collections.Generic;
using System.Linq;

namespace Services.Business
{
    public class MenuAccessService : BaseService<SystemMenu>
    {
        public MenuAccessService(int userId) : base(userId) { }
        public bool CanAccess(string controllerName)
        {
            if (controllerName.IsEmpty())
                return false;

            return db.fn_CanAccessController(UserId, controllerName);
        }
        public List<SystemMenu> MyMenus => db.fn_GetAccessMenu(UserId).ToList();
    }
}
