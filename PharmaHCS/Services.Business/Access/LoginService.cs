﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business
{
    public class LoginService
    {
        public enum AuthenticationResult
        {
            UserNameOrPasswordCannotBeEmpty = -1,
            UserDoesNotExist = -2,
            UserFound = 0,
            Error = -3
        }
        public class AuthResult
        {
            public AuthenticationResult Result { get; set; }
            public int UserId { get; set; }
            public string FullName { get; set; }
            public string FirstName
            {
                get
                {
                    if (!string.IsNullOrEmpty(FullName))
                    {
                        var s = FullName.Split(' ');
                        if (s.Length > 0)
                            return s[0];
                        return FullName;
                    }
                    return FullName;
                }
            }
            public string LastName
            {
                get
                {
                    if (!string.IsNullOrEmpty(FullName))
                    {
                        var s = FullName.Split(' ');
                        if (s.Length > 1)
                            return s[s.Length - 1];
                        return string.Empty;
                    }
                    return string.Empty;
                }
            }
            public string Picture { get; set; }
            public AuthResult()
            {
                Result = AuthenticationResult.Error;
                UserId = int.MinValue;
                FullName = string.Empty;
                Picture = string.Empty;
            }
        }

        public static string HashString(string str) {
            string hashedString = string.Empty;

            using (var sha512 = System.Security.Cryptography.SHA512.Create())
            {
                // Send a sample text to hash.  
                var hashedBytes = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(str));
                // Get the hashed string.  
                hashedString = BitConverter.ToString(hashedBytes).Replace("-", "");
            }
            return hashedString;
        }
        //private LogFiles log = new LogFiles();
        public static AuthResult Authenticate(string userName, string password)
        {
            try
            {
                if (string.IsNullOrEmpty(userName) || string.IsNullOrWhiteSpace(userName)
                    ||
                    string.IsNullOrEmpty(password) || string.IsNullOrWhiteSpace(password)
                    )
                    return new AuthResult { Result = AuthenticationResult.UserNameOrPasswordCannotBeEmpty };

                string hashedPassword = HashString(password);
                using (DBModel dB = new DBModel()) {
                    var dbUser = dB.Employees.FirstOrDefault(c => c.LoginId.ToLower().Trim() == userName.ToLower().Trim() && c.Password == hashedPassword && c.Active == true);
                    if (dbUser != null)
                        return new AuthResult { Result = AuthenticationResult.UserFound, UserId = dbUser.EmployeeId, FullName = dbUser.EmployeeName, Picture = dbUser.Picture };
                }

                return new AuthResult { Result = AuthenticationResult.UserDoesNotExist };
            }
            catch (Exception e)
            {
                //log.WriteToFile(log.logError, "getSystemUser", "LoginService", e.ToString());
                return new AuthResult();
            }
        }
        public static bool IsRep(int empId) {

            using (DBModel dB = new DBModel()){
                var canVisit = dB.EmployeeHistories.FirstOrDefault(w => w.EmployeeId == empId && w.FromDate <= DateTime.Now && (w.ToDate >= DateTime.Now || w.ToDate == null)).CanVisit;

                return canVisit ?? false;
            }
        }
    }
}
