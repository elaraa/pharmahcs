﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyCsvParser.Mapping;

namespace Sales.Import.Plugin.LTD
{
    public class CsvPersonMapping : CsvMapping<LTDCsvFile>
    {
        public CsvPersonMapping()
            : base()
        {
            MapProperty(0, x => x.empty1);
            MapProperty(1, x => x.empty2);
            MapProperty(2, x => x.custCode);
            MapProperty(3, x => x.custName);
            MapProperty(4, x => x.custAddress);
            MapProperty(5, x => x.ProdCode);
            MapProperty(6, x => x.ProdName);
            MapProperty(7, x => x.Qty);
            MapProperty(8, x => x.Bouns);
            MapProperty(9, x => x.Status);
        }
    }
    public class LTDCsvFile
    {
        public string empty1 { get; set; }
        public string empty2 { get; set; }
        public string custCode { get; set; }
        public string custName { get; set; }
        public string custAddress { get; set; }
        public string ProdCode { get; set; }
        public string ProdName { get; set; }

        public string Qty { get; set; }
        public string Bouns { get; set; }
        public string Status { get; set; }

        public override string ToString()
        {
            return string.Format("custCode: {0}, custName: {1} , custName: {2} , custName: {3} , custName: {4} , custName: {5} , custName: {6} , custName: {7}", custAddress, custName, custAddress, ProdCode, ProdName, Qty, Bouns, Status);
        }

    }
}

