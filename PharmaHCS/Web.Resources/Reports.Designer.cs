﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PH {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Reports {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Reports() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Web.Resources.Reports", typeof(Reports).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BU Achievement Product.
        /// </summary>
        public static string ViewNameBUAchievementProduct {
            get {
                return ResourceManager.GetString("ViewNameBUAchievementProduct", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BU Achievement.
        /// </summary>
        public static string ViewNamePluralBUAchievement {
            get {
                return ResourceManager.GetString("ViewNamePluralBUAchievement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PM Achievement.
        /// </summary>
        public static string ViewNamePMAchievement {
            get {
                return ResourceManager.GetString("ViewNamePMAchievement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PM Achievement Product.
        /// </summary>
        public static string ViewNamePMAchievementProduct {
            get {
                return ResourceManager.GetString("ViewNamePMAchievementProduct", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rep Achievement.
        /// </summary>
        public static string ViewNameRepAchievement {
            get {
                return ResourceManager.GetString("ViewNameRepAchievement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rep Achievement Product.
        /// </summary>
        public static string ViewNameRepAchievementProduct {
            get {
                return ResourceManager.GetString("ViewNameRepAchievementProduct", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SUP Achievement.
        /// </summary>
        public static string ViewNameSUPAchievement {
            get {
                return ResourceManager.GetString("ViewNameSUPAchievement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SUP Achievement Product.
        /// </summary>
        public static string ViewNameSUPAchievementProduct {
            get {
                return ResourceManager.GetString("ViewNameSUPAchievementProduct", resourceCulture);
            }
        }
    }
}
