﻿using OfficeOpenXml;
using Sales.Core.ImportFile;
using Sales.Import.Plugin.Standard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Import.Plugin.POS
{
    public class POSFile : StandardExcelImportFile
    {
        public POSFile() : base()
        {
            HeaderColumns.AddRange(new string[]{
                "Branch",
                "Branch Name",
                "Vendor Name",
                "Customer",
                "Customer Name",
                "Customer Address",
                "Sal. District",
                "Sal. Dist. Desc.",
                "Invoice Serial",
                "Invoice Date",
                "Trans. Type",
                "Mat. Code",
                "Mat. Desc.",
                "Qty",
                "B. Qty",
                "Value"
            });
        }
        protected bool IsEmpty(ExcelRange range) {
            return (range.Value == null || string.IsNullOrEmpty(range.Value.ToString()) || string.IsNullOrWhiteSpace(range.Value.ToString()));
        }
        protected override void FillRecord(List<string> columns, int rowIndex, ExcelRange inputRow)
        {
            if (IsEmpty(inputRow[rowIndex, 1]) && IsEmpty(inputRow[rowIndex, 2]) && IsEmpty(inputRow[rowIndex, 3]) && IsEmpty(inputRow[rowIndex, 4]))
                return;

            DateTime? sd = null;
            if (inputRow[rowIndex, columns.IndexOf("Invoice Date") + 1].Value != null)
                sd = DateTime.TryParse(inputRow[rowIndex, columns.IndexOf("Invoice Date") + 1].Value.ToString(), out DateTime ss) ? ss : (DateTime?)null;

            decimal? qty = null;
            if (inputRow[rowIndex, columns.IndexOf("Qty") + 1].Value != null)
                qty = Decimal.TryParse(inputRow[rowIndex, columns.IndexOf("Qty") + 1].Value.ToString(), out decimal q) ? q : (decimal?)null;

            decimal? bonusQty = null;
            if (inputRow[rowIndex, columns.IndexOf("B. Qty") + 1].Value != null)
                bonusQty = Decimal.TryParse(inputRow[rowIndex, columns.IndexOf("B. Qty") + 1].Value.ToString(), out decimal q) ? q : (decimal?)null;

            decimal? amount = null;
            if (inputRow[rowIndex, columns.IndexOf("Value") + 1].Value != null)
                amount = Decimal.TryParse(inputRow[rowIndex, columns.IndexOf("Value") + 1].Value.ToString(), out decimal q) ? q : (decimal?)null;

            FileRecords.Add(new PluginRecord
            {
                DistributorBranchCode = inputRow[rowIndex, columns.IndexOf("Branch") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("Branch") + 1].Value.ToString(),
                DistributorBranchName = inputRow[rowIndex, columns.IndexOf("Branch Name") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("Branch Name") + 1].Value.ToString(),
                CustomerTerritory = inputRow[rowIndex, columns.IndexOf("Sal. District") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("Sal. District") + 1].Value.ToString()
                                +" | "+ inputRow[rowIndex, columns.IndexOf("Sal. Dist. Desc.") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("Sal. Dist. Desc.") + 1].Value.ToString(),
          
                CustomerCode = inputRow[rowIndex, columns.IndexOf("Customer") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("Customer") + 1].Value.ToString(),
                CustomerName = inputRow[rowIndex, columns.IndexOf("Customer Name") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("Customer Name") + 1].Value.ToString(),
                CustomerAddress = inputRow[rowIndex, columns.IndexOf("Customer Address") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("Customer Address") + 1].Value.ToString(),
                DosageFormCode = inputRow[rowIndex, columns.IndexOf("Mat. Code") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("Mat. Code") + 1].Value.ToString(),
                DosageFormName = inputRow[rowIndex, columns.IndexOf("Mat. Desc.") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("Mat. Desc.") + 1].Value.ToString(),
                FileQuantity = inputRow[rowIndex, columns.IndexOf("Qty") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("Qty") + 1].Value.ToString(),
                FileAmount = inputRow[rowIndex, columns.IndexOf("Value") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("Value") + 1].Value.ToString(),

                SalesDate = sd,
                InvoiceNumber = inputRow[rowIndex, columns.IndexOf("Invoice Serial") + 1].Value == null ? string.Empty : inputRow[rowIndex, columns.IndexOf("Invoice Serial") + 1].Value.ToString(),
                Quantity = qty,
                InvalidQty = !qty.HasValue,
                RawRecord = "NA"
            });
        }
    }
}
