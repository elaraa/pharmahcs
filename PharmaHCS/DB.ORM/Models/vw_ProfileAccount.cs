﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_ProfileAccountMetadata))]
    public partial class vw_ProfileAccount
    {
        public class vw_ProfileAccountMetadata
        {
            [Key]
            public int ProfileAccountId { get; set; }

            [Key]
            public int ProfileId { get; set; }

            [StringLength(50)]
            public string ProfileCode { get; set; }

            [Key]
            public int Team { get; set; }

            [Key]
            public int AccountId { get; set; }
            

            [Key]
            public int TerritoryId { get; set; }

            [Key]
            public string TerritoryName { get; set; }

            [Key]
            public decimal Perccentage { get; set; }

            [StringLength(103)]
            public string ProductName { get; set; }
            
            [Key]
            [Column(Order = 7, TypeName = "date")]
            public DateTime FromDate { get; set; }

            [Key]
            public int CountryId { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
