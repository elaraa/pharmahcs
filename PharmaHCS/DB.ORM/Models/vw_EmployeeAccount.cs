﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_EmployeeAccountMetadata))]
    public partial class vw_EmployeeAccount
    {
        public class vw_EmployeeAccountMetadata
        {
            [Key]
            public int ProfileAccountId { get; set; }

            [Key]
            public int ProfileId { get; set; }

            [Key]
            public int EmployeeId { get; set; }

            [Key]
            public int AccountId { get; set; }
            [Key]
            public int TerritoryId { get; set; }

            [Key]
            [Column(Order = 5)]
            public string TerritoryName { get; set; }

            [Key]
            public int CategoryId { get; set; }

            [Key]
            public int CountryId { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
