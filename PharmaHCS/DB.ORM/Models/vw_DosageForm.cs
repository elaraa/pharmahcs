﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_DosageFormMetadata))]
    public partial class vw_DosageForm
    {
        public class vw_DosageFormMetadata
        {
            [Key]
            public int DosageFormId { get; set; }

            [Key]
            [StringLength(50)]
            public string DosageFormCode { get; set; }

            [Key]
            public string DosageFormName { get; set; }

            [Key]
            public int ProductId { get; set; }

            [StringLength(103)]
            public string ProductName { get; set; }

            [Key]
            [Column(Order = 4)]
            public bool Active { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }
        }
    }
}
