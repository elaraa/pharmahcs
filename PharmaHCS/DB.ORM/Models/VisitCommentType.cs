﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(VisitCommentTypeMetadata))]
    public partial class VisitCommentType
    {
        public class VisitCommentTypeMetadata
        {
            [Key]
            public int CommentTypeId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredCommentType", ErrorMessageResourceType = typeof(PH.VisitCommentType))]
            public string CommentType { get; set; }
        }
    }
}
