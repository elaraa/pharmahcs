﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(TimeDefinitiontMetadata))]
    public partial class TimeDefinition
    {
        public class TimeDefinitiontMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredPeriodName", ErrorMessageResourceType = typeof(PH.TimeDefinition))]
            public string PeriodName { get; set; }
        }
    }
}
