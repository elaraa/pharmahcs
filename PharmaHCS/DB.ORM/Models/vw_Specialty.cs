﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_SpecialtyMetadata))]
    public partial class vw_Specialty
    {
        public class vw_SpecialtyMetadata
        {
            [Key]
            public int SpecialtyId { get; set; }

            [StringLength(50)]
            public string SpecialtyCode { get; set; }

            [Key]
            public string SpecialtyName { get; set; }

            [Key]
            public int ProductCount { get; set; }
            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
