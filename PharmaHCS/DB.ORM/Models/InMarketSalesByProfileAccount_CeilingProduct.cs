﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(InMarketSalesByProfileAccount_CeilingProductMetadata))]
    public partial class InMarketSalesByProfileAccount_CeilingProduct
    {
        public class InMarketSalesByProfileAccount_CeilingProductMetadata
        {
            [Key]
            [Column(Order = 0)]
            public int ProfileId { get; set; }

            [Key]
            [Column(Order = 1)]
            public int TeamId { get; set; }

            [Key]
            [Column(Order = 2)]
            public int ProductId { get; set; }
            [Key]
            [Column(Order = 3)]
            public int DosageFormId { get; set; }

            [Key]
            [Column(Order = 4)]
            public int CountryId { get; set; }
        }
    }
}
