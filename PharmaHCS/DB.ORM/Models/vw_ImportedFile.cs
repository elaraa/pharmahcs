﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_ImportedFileMetadata))]
    public partial class vw_ImportedFile
    {
        public class vw_ImportedFileMetadata
        {
            [Key]
            public int FileId { get; set; }

            [Key]
            public int DistributorId { get; set; }
            
            [Key]
            public int StatusId { get; set; }

            [Key]
            [Column(Order = 3)]
            public string StatusName { get; set; }
            
            [Key]
            [StringLength(1)]
            public string ImportMode { get; set; }

            [Key]
            public int CountryId { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
