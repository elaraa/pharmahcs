﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_DistributorMetadata))]
    public partial class vw_Distributor
    {
        public class vw_DistributorMetadata
        {
            [Key]
            public int DistributorId { get; set; }

            [StringLength(50)]
            public string DistributorCode { get; set; }

            [Key]
            public string DistributorName { get; set; }
            [Key]
            public int BranchesCount { get; set; }

            [Key]
            public int CountryId { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }
            

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
