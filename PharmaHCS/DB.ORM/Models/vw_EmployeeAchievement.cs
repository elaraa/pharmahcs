﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_EmployeeAchievementMetadata))]
    public partial class vw_EmployeeAchievement
    {
        public class vw_EmployeeAchievementMetadata
        {
            [Key]
            public int TeamId { get; set; }

            [Key]
            public int EmployeeId { get; set; }

            [StringLength(50)]
            public string EmployeeCode { get; set; }

            [Key]
            [StringLength(50)]
            public string EmployeeName { get; set; }

            [StringLength(50)]
            public string ProfileCode { get; set; }

            [Key]
            public int TimeDefinitionId { get; set; }

            [Key]
            public int SupId { get; set; }

            [StringLength(50)]
            public string SupCode { get; set; }

            [Key]
            [StringLength(50)]
            public string SupName { get; set; }

            [Key]
            public int PMId { get; set; }

            [StringLength(50)]
            public string PMCode { get; set; }

            [Key]
            [StringLength(50)]
            public string PMName { get; set; }

            [Key]
            public int BUMId { get; set; }

            [StringLength(50)]
            public string BUMCode { get; set; }

            [Key]
            [StringLength(50)]
            public string BUMName { get; set; }

            [Key]
            public int PMDId { get; set; }

            [StringLength(50)]
            public string PMDCode { get; set; }

            [Key]
            [StringLength(50)]
            public string PMDName { get; set; }

            [Key]
            public int SODId { get; set; }

            [StringLength(50)]
            public string SODCode { get; set; }

            [Key]
            [StringLength(50)]
            public string SODName { get; set; }

            [Key]
            [Column(Order = 14)]
            public string PeriodName { get; set; }

            [Key]
            public int ProductId { get; set; }

            [StringLength(50)]
            public string ProductCode { get; set; }

            [Key]
            [StringLength(50)]
            public string ProductName { get; set; }
        }
    }
}
