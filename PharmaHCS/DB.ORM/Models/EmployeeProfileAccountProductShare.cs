﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(EmployeeProfileAccountProductShareMetadata))]
    public partial class EmployeeProfileAccountProductShare
    {
        public class EmployeeProfileAccountProductShareMetadata
        {
            [Key]
            [Column(Order = 0)]
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int DosageFormId { get; set; }

            [Key]
            [Column(Order = 1)]
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int AccountId { get; set; }

            [Key]
            [Column(Order = 2)]
            public decimal Perccentage { get; set; }

            [Key]
            [Column(Order = 3)]
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int ProfileId { get; set; }

            [Key]
            [Column(Order = 4)]
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int EmployeeId { get; set; }

            [Key]
            [Column(Order = 5)]
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int TeamId { get; set; }
        }
    }
}
