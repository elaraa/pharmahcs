﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(SystemRoleMenuMetadata))]
    public partial class SystemRoleMenu
    {
        public class SystemRoleMenuMetadata
        {
            [Key]
            public int MenuId { get; set; }

            [Key]
            public int RoleId { get; set; }
        }
    }
}
