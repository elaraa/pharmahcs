﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(DosageFormUnitMetadata))]
    public partial class DosageFormUnit
    {
        public class DosageFormUnitMetadata
        {
            [Key]
            public int UnitId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredUnitName", ErrorMessageResourceType = typeof(PH.DosageFormUnit))]
            public string UnitName { get; set; }

            [StringLength(50)]
            public string UnitCode { get; set; }
        }
    }
}
