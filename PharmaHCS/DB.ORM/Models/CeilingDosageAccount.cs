﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(CeilingDosageAccountMetadata))]
    public partial class CeilingDosageAccount
    {
        public class CeilingDosageAccountMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredCeilingQty", ErrorMessageResourceType = typeof(PH.Account))]
            [Range(typeof(decimal), "0", "9999999999999999999999999")]
            public decimal CeilingQty { get; set; }
        }
    }
}
