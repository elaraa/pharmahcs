﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ProductDosageFormMetadata))]
    public partial class ProductDosageForm
    {
        public class ProductDosageFormMetadata
        {
            [Key]
            public int DosageFormId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredDosageFormCode", ErrorMessageResourceType = typeof(PH.Product))]
            [StringLength(50)]
            public string DosageFormCode { get; set; }

            [Required(ErrorMessageResourceName = "RequiredDosageFormName", ErrorMessageResourceType = typeof(PH.Product))]
            public string DosageFormName { get; set; }
        }
    }
}
