﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(DistributorBranchMetadata))]
    public partial class DistributorBranch
    {
        public class DistributorBranchMetadata
        {
            [Key]
            public int BranchId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredBranchName", ErrorMessageResourceType = typeof(PH.Distributor))]
            public string BranchName { get; set; }

            [StringLength(50)]
            public string BranchCode { get; set; }
        }
    }
}
