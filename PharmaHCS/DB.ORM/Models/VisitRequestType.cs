﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(VisitRequestTypeMetadata))]
    public partial class VisitRequestType
    {
        public class VisitRequestTypeMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredVisitRequestTypeName", ErrorMessageResourceType = typeof(PH.VisitCommentType))]
            public string VisitRequestTypeName { get; set; }
        }
    }
}
