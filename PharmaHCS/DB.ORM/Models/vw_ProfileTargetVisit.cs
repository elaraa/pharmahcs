﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_ProfileTargetVisitMetadata))]
    public partial class vw_ProfileTargetVisit
    {
        public class vw_ProfileTargetVisitMetadata
        {
            [Key]
            public int ProfileTargetId { get; set; }

            [Key]
            public int TimeDefinitionId { get; set; }

            [Key]
            public string PeriodName { get; set; }

            [Key]
            public int Year { get; set; }

            [Key]
            public int ProfileId { get; set; }

            [StringLength(50)]
            public string ProfileCode { get; set; }

            [Key]
            public int EmployeeId { get; set; }

            [StringLength(103)]
            public string EmployeeName { get; set; }

            [StringLength(16)]
            public string TargetStatus { get; set; }
            
            [StringLength(103)]
            public string ManagerName { get; set; }

            [Key]
            public decimal EstimatedCallRate { get; set; }

            [Key]
            public int CountryId { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
