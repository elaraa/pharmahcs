﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ProfileTargetVisitMetadata))]
    public partial class ProfileTargetVisit
    {
        public class ProfileTargetVisitMetadata
        {
            [Key]
            public int ProfileTargetId { get; set; }
            

            [StringLength(1)]
            public string ManagerResponse { get; set; }

        }
    }
}
