﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(DoctorMetadata))]
    public partial class Doctor
    {
        public class DoctorMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredDoctorName", ErrorMessageResourceType = typeof(PH.Docotor))]
            public string DoctorName { get; set; }

            [Required(ErrorMessageResourceName = "RequiredGender", ErrorMessageResourceType = typeof(PH.Docotor))]
            [StringLength(1)]
            public string Gender { get; set; }

            [StringLength(11)]
            [RegularExpression(@"^([0-9]{11})$", ErrorMessageResourceName = "InvalidMobileNumber", ErrorMessageResourceType = typeof(PH.Docotor))]
            public string Mobile { get; set; }
        }
    }
}
