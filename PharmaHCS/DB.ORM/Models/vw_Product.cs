﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_ProductMetadata))]
    public partial class vw_Product
    {
        public class vw_ProductMetadata
        {
            [Key]
            public int ProductId { get; set; }

            [StringLength(50)]
            public string ProductCode { get; set; }

            [Key]
            [StringLength(50)]
            public string ProductName { get; set; }

            [Key]
            public bool Active { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
