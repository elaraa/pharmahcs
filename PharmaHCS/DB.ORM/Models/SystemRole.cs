﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(SystemRoleMetadata))]
    public partial class SystemRole
    {
        public class SystemRoleMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredRoleName", ErrorMessageResourceType = typeof(PH.Role))]
            public string RoleName { get; set; }
        }
    }
}
