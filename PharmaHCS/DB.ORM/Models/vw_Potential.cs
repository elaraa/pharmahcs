﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_PotentialMetadata))]
    public partial class vw_Potential
    {
        public class vw_PotentialMetadata
        {
            [Key]
            public int PotentialId { get; set; }

            [StringLength(50)]
            public string PotentialCode { get; set; }

            [Key]
            public string PotentialName { get; set; }

            [Key]
            public int AccountCount { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
