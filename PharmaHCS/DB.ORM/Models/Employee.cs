﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{ 
   [MetadataType(typeof(EmployeeMetadata))]
    public partial class Employee
    {
        public class EmployeeMetadata 
        {
            [Required(ErrorMessageResourceName = "RequiredEmployeeName", ErrorMessageResourceType = typeof(PH.Employee))]
            [StringLength(50)]
            public string EmployeeName { get; set; }

            [StringLength(50)]
            public string EmployeeCode { get; set; }

            [Required(ErrorMessageResourceName = "RequiredNationalIDNumber", ErrorMessageResourceType = typeof(PH.Employee))]
            [StringLength(50)]
            public string NationalIDNumber { get; set; }

            [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            public short? OrganizationLevel { get; set; }

            [Column(TypeName = "date")]
            public DateTime? BirthDate { get; set; }

            [Required(ErrorMessageResourceName = "RequiredMaritalStatus", ErrorMessageResourceType = typeof(PH.Employee))]
            [StringLength(1)]
            public string MaritalStatus { get; set; }

            [StringLength(1)]
            public string Gender { get; set; }

            [Column(TypeName = "date")]
            public DateTime HireDate { get; set; }

            [DataType(DataType.EmailAddress, ErrorMessageResourceName = "InvalidEmail", ErrorMessageResourceType = typeof(PH.Employee))]
            public string Email { get; set; }

            [StringLength(11)]
            [RegularExpression(@"^([0-9]{11})$", ErrorMessageResourceName = "InvalidMobile", ErrorMessageResourceType = typeof(PH.Employee))]
            public string Mobile { get; set; }
        }
    }
}
