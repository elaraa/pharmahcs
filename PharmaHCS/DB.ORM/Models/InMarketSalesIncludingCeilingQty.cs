﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(InMarketSalesIncludingCeilingQtyMetadata))]
    public partial class InMarketSalesIncludingCeilingQty
    {
        public class InMarketSalesIncludingCeilingQtyMetadata
        {
            [Key]
            [Column(Order = 0)]
            public int DosageFormId { get; set; }

            [Key]
            [Column(Order = 1)]
            public int CountryId { get; set; }
        }
    }
}
