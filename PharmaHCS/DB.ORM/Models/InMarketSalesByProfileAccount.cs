﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(InMarketSalesByProfileAccountMetadata))]
    public partial class InMarketSalesByProfileAccount
    {
        public class InMarketSalesByProfileAccountMetadata
        {
            [Key]
            [Column(Order = 0)]
            public int CountryId { get; set; }
            [Key]
            [Column(Order = 1)]
            public int ProductId { get; set; }

            [Key]
            [Column(Order = 2)]
            public int DosageFormId { get; set; }


            [Key]
            [Column(Order = 3)]
            public int ProfileId { get; set; }

            [Key]
            [Column(Order = 4)]
            public int TeamId { get; set; }

            [Key]
            [Column(Order = 5)]
            public decimal SharePercentage { get; set; }


            [Key]
            [Column(Order = 6)]
            [StringLength(7)]
            public string ShareType { get; set; }
        }
    }
}
