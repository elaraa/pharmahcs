﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(VacationTypeMetadata))]
    public partial class VacationType
    {
        public class VacationTypeMetadata 
        {
            [Required(ErrorMessageResourceName = "RequiredVacationTypeName", ErrorMessageResourceType = typeof(PH.Employee))]
            public string VacationTypeName { get; set; }
        }
    }
}
