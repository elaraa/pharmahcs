﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ProductSpecialtyMetadata))]
    public partial class ProductSpecialty
    {
        public class ProductSpecialtyMetadata
        {
            [Key]
            public int ProductId { get; set; }

            [Key]
            public int SpecialtyId { get; set; }
        }
    }
}
