﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_TeamProfileMetadata))]
    public partial class vw_TeamProfile
    {
        public class vw_TeamProfileMetadata
        {
            [Key]
            public int ProfileId { get; set; }

            [StringLength(50)]
            public string ProfileCode { get; set; }

            [Key]
            public string ProfileName { get; set; }

            [Key]
            public int TeamId { get; set; }
            

            [Key]
            public bool Active { get; set; }

            [Key]
            public int CountryId { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }
            

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
