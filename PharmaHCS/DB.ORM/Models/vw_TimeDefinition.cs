﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_TimeDefinitionMetadata))]
    public partial class vw_TimeDefinition
    {
        public class vw_TimeDefinitionMetadata
        {
            [Key]
            public int TimeDefinitionId { get; set; }

            [Key]
            public string PeriodName { get; set; }

            [Key]
            public DateTime FromDate { get; set; }

            [Key]
            public DateTime ToDate { get; set; }
            

            [Key]
            public int Year { get; set; }
            

            [StringLength(50)]
            public string CountryCode { get; set; }
            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
