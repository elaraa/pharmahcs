﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_VisitRequestTypeMetadata))]
    public partial class vw_VisitRequestType
    {
        public class vw_VisitRequestTypeMetadata
        {
            [Key]
            public int VisitRequestTypeId { get; set; }

            [Key]
            public string VisitRequestTypeName { get; set; }

            [Key]
            public bool Active { get; set; }
            

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
