﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(InMarketSaleMetadata))]
    public partial class InMarketSale
    {
        public class InMarketSaleMetadata
        {
            [Key]
            public int InMarketSalesId { get; set; }
        }
    }
}
