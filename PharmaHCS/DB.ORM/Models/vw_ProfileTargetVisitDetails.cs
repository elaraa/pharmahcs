﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_ProfileTargetVisitDetailsMetadata))]
    public partial class vw_ProfileTargetVisitDetails
    {
        public class vw_ProfileTargetVisitDetailsMetadata
        {
            [Key]
            public int ProfileTargetDetailId { get; set; }

            [Key]
            public int ProfileTargetId { get; set; }

            [Key]
            public int AccountId { get; set; }
            

            [Key]
            public int AccountTypeId { get; set; }
            

            [StringLength(50)]
            public string SpecialtyCode { get; set; }
            

            [Key]
            public int TerritoryId { get; set; }

            [Key]
            public string TerritoryName { get; set; }

            [Key]
            public int PotentialId { get; set; }

            [StringLength(50)]
            public string PotentialCode { get; set; }

            [Key]
            public int Frequency { get; set; }
        }
    }
}
