﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(DepartmentMetadata))]
    public partial class Department
    {
        public class DepartmentMetadata
        {

            [Required(ErrorMessageResourceName = "RequiredDepartmentName", ErrorMessageResourceType = typeof(PH.Employee))]
            public string DepartmentName { get; set; }

            [StringLength(50)]
            public string DepartmentCode { get; set; }
        }
    }
}
