﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_VisitCommentTypeMetadata))]
    public partial class vw_VisitCommentType
    {
        public class vw_VisitCommentTypeMetadata
        {
            [Key]
            public int CommentTypeId { get; set; }

            [Key]
            public string CommentType { get; set; }

            [Key]
            public bool Active { get; set; }
            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
