﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(APotentialMetadata))]
    public partial class Potential
    {
        public class APotentialMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredPotentialName", ErrorMessageResourceType = typeof(PH.Potential))]
            public string PotentialName { get; set; }

            [StringLength(50)]
            public string PotentialCode { get; set; }
        }
    }
}
