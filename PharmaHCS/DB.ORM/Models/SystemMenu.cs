﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(SystemMenuMetadata))]
    public partial class SystemMenu
    {
        public class SystemMenuMetadata
        {
            [Key]
            public int MenuId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredMenuName", ErrorMessageResourceType = typeof(PH.Role))]
            public string MenuName { get; set; }
        }
    }
}
