﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_targetPhasing_DetailsMetadata))]
    public partial class vw_targetPhasing_Details
    {
        public class vw_targetPhasing_DetailsMetadata
        {
            [Key]
            public int TargetId { get; set; }

            [Key]
            public int Year { get; set; }

            [StringLength(103)]
            public string ProductName { get; set; }

            [Key]
            public int TimeDefinitionId { get; set; }

            [Key]
            public string PeriodName { get; set; }

            [Key]
            public int CountryId { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
