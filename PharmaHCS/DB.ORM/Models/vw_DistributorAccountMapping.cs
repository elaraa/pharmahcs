﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_DistributorAccountMappingMetadata))]
    public partial class vw_DistributorAccountMapping
    {
        public class vw_DistributorAccountMappingMetadata
        {
            [Key]
            public int DistributorAccountMappingId { get; set; }

            [Key]
            public int DistributorId { get; set; }

            [Key]
            public int AccountId { get; set; }

            [Key]
            public int CountryId { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
