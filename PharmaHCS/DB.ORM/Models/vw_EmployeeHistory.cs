﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_EmployeeHistoryMetadata))]
    public partial class vw_EmployeeHistory
    {
        public class vw_EmployeeHistoryMetadata
        {
            [Key]
            public int EmployeeHistoryId { get; set; }

            [Key]
            public int EmployeeId { get; set; }

            [StringLength(103)]
            public string EmployeeName { get; set; }

            [Key]
            public int JobTitleId { get; set; }

            public string JobTitleName { get; set; }

            [Key]
            public int DepartmentId { get; set; }
            

            [StringLength(103)]
            public string ManagerName { get; set; }

            [Key]
            public DateTime FromDate { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
