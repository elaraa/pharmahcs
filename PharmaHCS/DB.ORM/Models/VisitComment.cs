﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(VisitCommentMetadata))]
    public partial class VisitComment
    {
        public class VisitCommentMetadata
        {
            [Key]
            public Guid VisitId { get; set; }

            [Key]
            public int CommentTypeId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredCommentText", ErrorMessageResourceType = typeof(PH.VisitCommentType))]
            public string CommentText { get; set; }
        }
    }
}
