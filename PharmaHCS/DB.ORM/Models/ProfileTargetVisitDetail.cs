﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ProfileTargetVisitDetailMetadata))]
    public partial class ProfileTargetVisitDetail
    {
        public class ProfileTargetVisitDetailMetadata
        {
            [Key]
            public int ProfileTargetDetailId { get; set; }
        }
    }
}
