﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ImportedFileRecordMetadata))]
    public partial class ImportedFileRecord
    {
        public class ImportedFileRecordMetadata
        {
            [Key]
            public int FileRecordId { get; set; }
        }
    }
}
