﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_TargetProductPhasingMetadata))]
    public partial class vw_TargetProductPhasing
    {
        public class vw_TargetProductPhasingMetadata
        {
            [Key]
            public int TargetId { get; set; }

            [Key]
            public int Year { get; set; }

            public int? Products { get; set; }

            [Key]
            public int CountryId { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }
            

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
