﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_SystemRoleMetadata))]
    public partial class vw_SystemRole
    {
        public class vw_SystemRoleMetadata
        {
            [Key]
            public int SystemRoleId { get; set; }

            [Key]
            public string RoleName { get; set; }

            [Key]
            public bool IsAdmin { get; set; }

            [Key]
            public bool Active { get; set; }

            [Key]
            public int EmployeesCount { get; set; }
            
            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
