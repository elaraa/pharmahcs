﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ImportPluginMetadata))]
    public partial class ImportPlugin
    {
        public class ImportPluginMetadata
        {
            [Key]
            public int PluginId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredPluginName", ErrorMessageResourceType = typeof(PH.SalesImport))]
            public string PluginName { get; set; }

            [Required(ErrorMessageResourceName = "RequiredDllPath", ErrorMessageResourceType = typeof(PH.SalesImport))]
            public string DllPath { get; set; }

            [Required(ErrorMessageResourceName = "RequiredClassName", ErrorMessageResourceType = typeof(PH.SalesImport))]
            public string ClassName { get; set; }

            [Required(ErrorMessageResourceName = "RequiredTemplatePath", ErrorMessageResourceType = typeof(PH.SalesImport))]
            public string TemplatePath { get; set; }
        }
    }
}
