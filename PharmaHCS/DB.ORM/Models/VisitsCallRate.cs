﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(VisitsCallRateMetadata))]
    public partial class VisitsCallRate
    {
        public class VisitsCallRateMetadata
        {
            [Key]
            public int ProfileTargetId { get; set; }

            [Key]
            public int EmployeeId { get; set; }

            [StringLength(103)]
            public string EmployeeName { get; set; }

            [Key]
            public int ProfileId { get; set; }
            [Key]
            public int CountryId { get; set; }

            [Key]
            public int TimeDefinitionId { get; set; }

            [Key]
            public string PeriodName { get; set; }

            [Key]
            public int Year { get; set; }
        }
    }
}
