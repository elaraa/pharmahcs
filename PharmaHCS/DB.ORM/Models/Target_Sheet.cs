﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(Target_SheetMetadata))]
    public partial class Target_Sheet
    {
        public class Target_SheetMetadata
        {
            [Key]
            public int CountryId { get; set; }
        }
    }
}
