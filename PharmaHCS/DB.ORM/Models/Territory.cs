﻿ using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(TerritoryMetadata))]
    public partial class Territory
    {
        public class TerritoryMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredTerritoryCode", ErrorMessageResourceType = typeof(PH.Territory))]
            [StringLength(50)]
            public string TerritoryCode { get; set; }

            [Required(ErrorMessageResourceName = "RequiredTerritoryName", ErrorMessageResourceType = typeof(PH.Territory))]
            public string TerritoryName { get; set; }
        }
    }
}
