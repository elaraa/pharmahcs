﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(VisitDoctorMetadata))]
    public partial class VisitDoctor
    {
        public class VisitDoctorMetadata
        {
            [Key]
            public Guid VisitId { get; set; }

            [Key]
            public int DoctorId { get; set; }
        }
    }
}
