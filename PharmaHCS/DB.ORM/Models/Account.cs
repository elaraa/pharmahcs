﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(AccountMetadata))]
    public partial class Account
    {
        public class AccountMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredAccountName", ErrorMessageResourceType = typeof(PH.Account))]
            public string AccountName { get; set; }

            [StringLength(50)]
            public string AccountCode { get; set; }
        }
    }
}
