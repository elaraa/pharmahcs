﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_MarketShareTerritoryMetadata))]
    public partial class vw_MarketShareTerritory
    {
        public class vw_MarketShareTerritoryMetadata
        {
            [Key]
            public int MarketShareTerritoryId { get; set; }

            [Key]
            public int MarketShareId { get; set; }

            [Key]
            public int TerritoryId { get; set; }
            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
