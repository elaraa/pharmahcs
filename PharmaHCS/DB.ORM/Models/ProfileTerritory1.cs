﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ProfileTerritory1Metadata))]
    public partial class ProfileTerritory1
    {
        public class ProfileTerritory1Metadata
        {
            [Key]
            public int ProfileTerritoryId { get; set; }
        }
    }
}
