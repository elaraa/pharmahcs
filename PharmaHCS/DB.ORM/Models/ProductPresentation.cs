﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ProductPresentationMetadata))]
    public partial class ProductPresentation
    {
        public class ProductPresentationMetadata
        {
            [Key]
            public int PresentationId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredPresentationName", ErrorMessageResourceType = typeof(PH.Product))]
            public string PresentationName { get; set; }

           // [Required(ErrorMessageResourceName = "RequiredFilePath", ErrorMessageResourceType = typeof(PH.Product))]
            public string FilePath { get; set; }

           // [Required(ErrorMessageResourceName = "RequiredExt", ErrorMessageResourceType = typeof(PH.Product))]
            [StringLength(50)]
            public string Ext { get; set; }
        }
        [NotMapped]
        public class ProductPresentationDto : ProductPresentation
        {
            public int Id { get; set; }
           
            public HttpPostedFileBase UploadFile { get; set; }  
        }

    }
}
