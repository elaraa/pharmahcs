﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_ProfileTargetVisit_PotentialSummaryMetadata))]
    public partial class vw_ProfileTargetVisit_PotentialSummary
    {
        public class vw_ProfileTargetVisit_PotentialSummaryMetadata
        {
            [Key]
            public int ProfileTargetId { get; set; }

            [Key]
            public int PotentialId { get; set; }

            [StringLength(50)]
            public string PotentialCode { get; set; }

            [Key]
            public string PotentialName { get; set; }
        }
    }
}
