﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(DistributorAccountMappingMetadata))]
    public partial class DistributorAccountMapping
    {
        public class DistributorAccountMappingMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredAccountCode", ErrorMessageResourceType = typeof(PH.Distributor))]
            public string AccountCode { get; set; }
        }
    }
}
