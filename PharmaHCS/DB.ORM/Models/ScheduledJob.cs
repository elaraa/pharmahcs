﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ScheduledJobMetadata))]
    public partial class ScheduledJob
    {
        public class ScheduledJobMetadata
        {
            [Key]
            public int JobId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredJobName", ErrorMessageResourceType = typeof(PH.ScheduledJob))]
            public string JobName { get; set; }

            [Required(ErrorMessageResourceName = "RequiredModule", ErrorMessageResourceType = typeof(PH.ScheduledJob))]
            [StringLength(50)]
            public string Module { get; set; }

            [Required(ErrorMessageResourceName = "RequiredStoredProcedure", ErrorMessageResourceType = typeof(PH.ScheduledJob))]
            public string StoredProcedure { get; set; }

            [Required(ErrorMessageResourceName = "RequiredScheduleType", ErrorMessageResourceType = typeof(PH.ScheduledJob))]
            [StringLength(1)]
            public string ScheduleType { get; set; }
        }
    }
}
