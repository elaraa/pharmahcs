﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(SpecialtyMetadata))]
    public partial class Specialty
    {
        public class SpecialtyMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredSpecialtyName", ErrorMessageResourceType = typeof(PH.Specialty))]
            public string SpecialtyName { get; set; }

            [StringLength(50)]
            public string SpecialtyCode { get; set; }
        }
    }
}
