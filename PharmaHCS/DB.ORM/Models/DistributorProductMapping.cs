﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(DistributorProductMappingMetadata))]
    public partial class DistributorProductMapping
    {
        public class DistributorProductMappingMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredDosageFormCode", ErrorMessageResourceType = typeof(PH.Distributor))]
            public string DosageFormCode { get; set; }
        }
    }
}
