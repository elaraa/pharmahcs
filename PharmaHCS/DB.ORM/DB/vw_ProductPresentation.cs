namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.vw_ProductPresentation")]
    public partial class vw_ProductPresentation
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PresentationId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string PresentationName { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProductId { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        public string ProductName { get; set; }

        [Key]
        [Column(Order = 4)]
        public bool Active { get; set; }

        [Key]
        [Column(Order = 5)]
        public string FilePath { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(50)]
        public string Ext { get; set; }
    }
}
