namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HR.EmployeeHistory")]
    public partial class EmployeeHistory
    {
        public int EmployeeHistoryId { get; set; }

        public int EmployeeId { get; set; }

        public int JobTitleId { get; set; }

        public int DepartmentId { get; set; }

        public int? ManagerId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public short? OrganizationLevel { get; set; }

        [Column(TypeName = "date")]
        public DateTime FromDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ToDate { get; set; }

        public int? CountryId { get; set; }

        public int? TeamId { get; set; }

        public bool? CanVisit { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Department Department { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Employee Employee2 { get; set; }

        public virtual Employee Employee3 { get; set; }

        public virtual Country Country { get; set; }

        public virtual JobTitle JobTitle { get; set; }

        public virtual Team Team { get; set; }
    }
}
