namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.ImportedFileRecord")]
    public partial class ImportedFileRecord
    {
        [Key]
        public int FileRecordId { get; set; }

        public int FileId { get; set; }

        public int? DistributorBranchId { get; set; }

        public string RawRecord { get; set; }

        public string CustomerCode { get; set; }

        public string CustomerName { get; set; }

        public string CustomerAddress { get; set; }

        public string CustomerTerritory { get; set; }

        public string DosageFormCode { get; set; }

        public string DosageFormName { get; set; }

        public string FileQuantity { get; set; }

        public string FileAmount { get; set; }

        public string InvoiceNumber { get; set; }

        [Column(TypeName = "date")]
        public DateTime? SalesDate { get; set; }

        public int? AccountId { get; set; }

        public int? DosageFormId { get; set; }

        public decimal? Quantity { get; set; }

        public decimal? Amount { get; set; }

        public string DistributorBranchCode { get; set; }

        public string DistributorBranchName { get; set; }

        public bool CustomerMapped { get; set; }

        public bool ProductMapped { get; set; }

        public bool DuplicateRow { get; set; }

        public bool InvalidQty { get; set; }

        public bool InvalidRow { get; set; }

        public bool AccountToBeCreated { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual DistributorBranch DistributorBranch { get; set; }

        public virtual ImportedFile ImportedFile { get; set; }

        public virtual Account Account { get; set; }

        public virtual ProductDosageForm ProductDosageForm { get; set; }
    }
}
