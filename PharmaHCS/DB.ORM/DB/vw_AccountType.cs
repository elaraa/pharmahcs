namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Shared.vw_AccountType")]
    public partial class vw_AccountType
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountTypeId { get; set; }

        [StringLength(50)]
        public string AccountTypeCode { get; set; }

        [Key]
        [Column(Order = 1)]
        public string AccountTypeName { get; set; }

        [Key]
        [Column(Order = 2)]
        public bool MustHaveDoctor { get; set; }

        [Key]
        [Column(Order = 3)]
        public bool TargetAndPlanWithDoctor { get; set; }

        [Key]
        [Column(Order = 4)]
        public bool IncludeInSales { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountsCount { get; set; }

        public int? CountryId { get; set; }

        [StringLength(50)]
        public string CountryCode { get; set; }

        public DateTime? LastUpdate { get; set; }

        public int? UpdatedBy { get; set; }

        [StringLength(50)]
        public string UpdatedByLogonName { get; set; }
    }
}
