namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.vw_targetPhasing_Details")]
    public partial class vw_targetPhasing_Details
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TargetId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Year { get; set; }

        public int? ProductId { get; set; }

        [StringLength(103)]
        public string ProductName { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TimeDefinitionId { get; set; }

        [Key]
        [Column(Order = 3)]
        public string PeriodName { get; set; }

        public decimal? Percentage { get; set; }

        public decimal? PercentageGrowth { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CountryId { get; set; }

        [StringLength(50)]
        public string CountryCode { get; set; }

        public DateTime? LastUpdate { get; set; }

        public int? UpdatedBy { get; set; }

        [StringLength(50)]
        public string UpdatedByLogonName { get; set; }
    }
}
