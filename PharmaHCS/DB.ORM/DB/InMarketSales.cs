namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.InMarketSales")]
    public partial class InMarketSales
    {
        public int InMarketSalesId { get; set; }

        public int? FileId { get; set; }

        public int CountryId { get; set; }

        public int DistributorId { get; set; }

        public int? DistributorBranchId { get; set; }

        public int? AccountId { get; set; }

        public int DosageFormId { get; set; }

        public decimal Qty { get; set; }

        public decimal? Amount { get; set; }

        [Column(TypeName = "date")]
        public DateTime? SalesDate { get; set; }

        public string InvoiceNumber { get; set; }

        public int? TimeDefinitionId { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Distributor Distributor { get; set; }

        public virtual DistributorBranch DistributorBranch { get; set; }

        public virtual ImportedFile ImportedFile { get; set; }

        public virtual Account Account { get; set; }

        public virtual Country Country { get; set; }

        public virtual ProductDosageForm ProductDosageForm { get; set; }

        public virtual TimeDefinition TimeDefinition { get; set; }
    }
}
