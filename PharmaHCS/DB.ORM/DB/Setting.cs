namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("App.Setting")]
    public partial class Setting
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SettingId { get; set; }

        [Required]
        public string SettingLabel { get; set; }

        public string SettingValue { get; set; }

        [Required]
        [StringLength(10)]
        public string SettingType { get; set; }

        public string SettingAvailableValues { get; set; }

        [Required]
        [StringLength(50)]
        public string Module { get; set; }

        public int? CountryId { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }
    }
}
