namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.VisitRequest")]
    public partial class VisitRequest
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int VisitRequestId { get; set; }

        public Guid VisitId { get; set; }

        public int VisitRequestTypeId { get; set; }

        public string RequestDescription { get; set; }

        public int RequestStatus { get; set; }

        public bool Delivered { get; set; }

        public int? FullfilledById { get; set; }

        public DateTime? FullfillmentDate { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Visit Visit { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Employee Employee2 { get; set; }

        public virtual VisitRequestType VisitRequestType { get; set; }
    }
}
