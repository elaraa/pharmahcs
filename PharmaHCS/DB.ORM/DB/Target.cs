namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.Target")]
    public partial class Target
    {
        public int TargetId { get; set; }

        public int TargetTypeId { get; set; }

        public int Year { get; set; }

        public int? TimeDefinitionId { get; set; }

        public int? ProductId { get; set; }

        public int? DosageFormId { get; set; }

        public int? AccountId { get; set; }

        public int? TerritoryId { get; set; }

        public int? TeamId { get; set; }

        public int? ProfileId { get; set; }

        public int? EmployeeId { get; set; }

        public decimal? Qty { get; set; }

        public decimal? Amount { get; set; }

        public decimal? Percentage { get; set; }

        public int CountryId { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Employee Employee2 { get; set; }

        public virtual Account Account { get; set; }

        public virtual Country Country { get; set; }

        public virtual Product Product { get; set; }

        public virtual ProductDosageForm ProductDosageForm { get; set; }

        public virtual TargetType TargetType { get; set; }

        public virtual Team Team { get; set; }

        public virtual TeamProfile TeamProfile { get; set; }

        public virtual Territory Territory { get; set; }

        public virtual TimeDefinition TimeDefinition { get; set; }
    }
}
