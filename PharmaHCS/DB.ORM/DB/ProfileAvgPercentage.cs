namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Temp.ProfileAvgPercentage")]

    public partial class ProfileAvgPercentage
    {
      
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProfileId { get; set; }

       
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProductId { get; set; }

        public int? TimeDefinitionId { get; set; }

        public decimal? salesPercentage { get; set; }

        public decimal? targetPercentage { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? AvgPercenatege { get; set; }
    }
}
