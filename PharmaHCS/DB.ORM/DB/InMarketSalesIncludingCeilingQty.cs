namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Staging.InMarketSalesIncludingCeilingQty")]
    public partial class InMarketSalesIncludingCeilingQty
    {
        public int? AccountId { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DosageFormId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CountryId { get; set; }

        public int? TimeDefinitionId { get; set; }

        public decimal? Qty { get; set; }

        public decimal? RemainingQty { get; set; }
    }
}
