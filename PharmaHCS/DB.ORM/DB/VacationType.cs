namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HR.VacationType")]
    public partial class VacationType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public VacationType()
        {
            EmployeeVacations = new HashSet<EmployeeVacation>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int VacationTypeId { get; set; }

        [Required]
        public string VacationTypeName { get; set; }

        public int NoticePeriodPerDay { get; set; }

        public int? CountryId { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EmployeeVacation> EmployeeVacations { get; set; }

        public virtual Country Country { get; set; }
    }
}
