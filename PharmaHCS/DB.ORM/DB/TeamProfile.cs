namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Shared.TeamProfile")]
    public partial class TeamProfile
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TeamProfile()
        {
            ProfileAccounts = new HashSet<ProfileAccount>();
            ProfileTargetVisits = new HashSet<ProfileTargetVisit>();
            ProfileTerritories = new HashSet<ProfileTerritory>();
            ProfileAccount1 = new HashSet<ProfileAccount1>();
            ProfileTerritory1 = new HashSet<ProfileTerritory1>();
            Targets = new HashSet<Target>();
            EmployeeProfiles = new HashSet<EmployeeProfile>();
        }

        [Key]
        public int ProfileId { get; set; }

        public int TeamId { get; set; }

        [StringLength(50)]
        public string ProfileCode { get; set; }

        [Required]
        public string ProfileName { get; set; }

        public bool Active { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public decimal? AvgCallRate { get; set; }

        public int? ListTargetDoctorsCount { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProfileAccount> ProfileAccounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProfileTargetVisit> ProfileTargetVisits { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProfileTerritory> ProfileTerritories { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProfileAccount1> ProfileAccount1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProfileTerritory1> ProfileTerritory1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Target> Targets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EmployeeProfile> EmployeeProfiles { get; set; }

        public virtual Team Team { get; set; }
    }
}
