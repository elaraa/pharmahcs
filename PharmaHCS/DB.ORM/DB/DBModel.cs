namespace DB.ORM.DB
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBModel : DbContext
    {
        public DBModel()
            : base("name=DBModel")
        {
        }

        public virtual DbSet<ScheduledJob> ScheduledJobs { get; set; }
        public virtual DbSet<Setting> Settings { get; set; }
        public virtual DbSet<SystemMenu> SystemMenus { get; set; }
        public virtual DbSet<SystemRole> SystemRoles { get; set; }
        public virtual DbSet<SystemRoleMenu> SystemRoleMenus { get; set; }
        public virtual DbSet<AccountDoctor> AccountDoctors { get; set; }
        public virtual DbSet<Doctor> Doctors { get; set; }
        public virtual DbSet<Potential> Potentials { get; set; }
        public virtual DbSet<ProductPresentation> ProductPresentations { get; set; }
        public virtual DbSet<ProductSpecialty> ProductSpecialties { get; set; }
        public virtual DbSet<ProfileAccount> ProfileAccounts { get; set; }
        public virtual DbSet<ProfileTargetNewDoctorApproval> ProfileTargetNewDoctorApprovals { get; set; }
        public virtual DbSet<ProfileTargetVisit> ProfileTargetVisits { get; set; }
        public virtual DbSet<ProfileTargetVisitDetail> ProfileTargetVisitDetails { get; set; }
        public virtual DbSet<ProfileTerritory> ProfileTerritories { get; set; }
        public virtual DbSet<Specialty> Specialties { get; set; }
        public virtual DbSet<TeamProduct> TeamProducts { get; set; }
        public virtual DbSet<Visit> Visits { get; set; }
        public virtual DbSet<VisitCoach> VisitCoaches { get; set; }
        public virtual DbSet<VisitComment> VisitComments { get; set; }
        public virtual DbSet<VisitCommentType> VisitCommentTypes { get; set; }
        public virtual DbSet<VisitDoctor> VisitDoctors { get; set; }
        public virtual DbSet<VisitPresentation> VisitPresentations { get; set; }
        public virtual DbSet<VisitProduct> VisitProducts { get; set; }
        public virtual DbSet<VisitRequest> VisitRequests { get; set; }
        public virtual DbSet<VisitRequestType> VisitRequestTypes { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<EmployeeHistory> EmployeeHistories { get; set; }
        public virtual DbSet<EmployeeVacation> EmployeeVacations { get; set; }
        public virtual DbSet<JobTitle> JobTitles { get; set; }
        public virtual DbSet<VacationType> VacationTypes { get; set; }
        public virtual DbSet<CeilingAccountException> CeilingAccountExceptions { get; set; }
        public virtual DbSet<CeilingDosageAccount> CeilingDosageAccounts { get; set; }
        public virtual DbSet<Distributor> Distributors { get; set; }
        public virtual DbSet<DistributorAccountMapping> DistributorAccountMappings { get; set; }
        public virtual DbSet<DistributorBranch> DistributorBranches { get; set; }
        public virtual DbSet<DistributorProductMapping> DistributorProductMappings { get; set; }
        public virtual DbSet<ImportedFile> ImportedFiles { get; set; }
        public virtual DbSet<ImportedFileRecord> ImportedFileRecords { get; set; }
        public virtual DbSet<ImportedFileStatu> ImportedFileStatus { get; set; }
        public virtual DbSet<ImportPlugin> ImportPlugins { get; set; }
        public virtual DbSet<InMarketSale> InMarketSales { get; set; }
        public virtual DbSet<MarketShare> MarketShares { get; set; }
        public virtual DbSet<MarketShareTerritory> MarketShareTerritories { get; set; }
        public virtual DbSet<ProductPrice> ProductPrices { get; set; }
        public virtual DbSet<ProfileAccount1> ProfileAccount1 { get; set; }
        public virtual DbSet<ProfileTerritory1> ProfileTerritory1 { get; set; }
        public virtual DbSet<StoreAccount> StoreAccounts { get; set; }
        public virtual DbSet<Target> Targets { get; set; }
        public virtual DbSet<TargetType> TargetTypes { get; set; }
        public virtual DbSet<TeamSalesProduct> TeamSalesProducts { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<AccountCategory> AccountCategories { get; set; }
        public virtual DbSet<AccountType> AccountTypes { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<DosageFormUnit> DosageFormUnits { get; set; }
        public virtual DbSet<EmployeeProfile> EmployeeProfiles { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductDosageForm> ProductDosageForms { get; set; }
        public virtual DbSet<ProductGroup> ProductGroups { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<TeamProfile> TeamProfiles { get; set; }
        public virtual DbSet<Territory> Territories { get; set; }
        public virtual DbSet<TimeDefinition> TimeDefinitions { get; set; }
        public virtual DbSet<tb_EmployeeStructurewProfile> tb_EmployeeStructurewProfile { get; set; }
        public virtual DbSet<InMarketSalesByEmployeeByProfile> InMarketSalesByEmployeeByProfiles { get; set; }
        public virtual DbSet<DistributorAccountDosagePercentage> DistributorAccountDosagePercentages { get; set; }
        public virtual DbSet<InMarketSalesByDistributorIncludingCeiledQty> InMarketSalesByDistributorIncludingCeiledQties { get; set; }
        public virtual DbSet<InMarketSalesByProfileAccount> InMarketSalesByProfileAccounts { get; set; }
        public virtual DbSet<InMarketSalesByProfileAccount_CeilingProduct> InMarketSalesByProfileAccount_CeilingProduct { get; set; }
        public virtual DbSet<InMarketSalesFact> InMarketSalesFacts { get; set; }
        public virtual DbSet<InMarketSalesIncludingCeilingQty> InMarketSalesIncludingCeilingQties { get; set; }
        public virtual DbSet<EmployeeProfileAccountProductShare> EmployeeProfileAccountProductShares { get; set; }
        public virtual DbSet<EmployeeProfileTerritoryProductShare> EmployeeProfileTerritoryProductShares { get; set; }
        public virtual DbSet<profileaccount2> profileaccount2 { get; set; }
        public virtual DbSet<ProfileAccountProductShare> ProfileAccountProductShares { get; set; }
        public virtual DbSet<ProfileSales_For_Store> ProfileSales_For_Store { get; set; }
        public virtual DbSet<storeException> storeExceptions { get; set; }
        public virtual DbSet<Target_AvgPercentage> Target_AvgPercentage { get; set; }
        public virtual DbSet<Target_InMarketSales> Target_InMarketSales { get; set; }
        public virtual DbSet<Target_Sheet> Target_Sheet { get; set; }
        public virtual DbSet<TeamSalesProduct1> TeamSalesProduct1 { get; set; }
        public virtual DbSet<vw_EmployeeStructurewProfile> vw_EmployeeStructurewProfile { get; set; }
        public virtual DbSet<vw_SystemRole> vw_SystemRole { get; set; }
        public virtual DbSet<vw_User> vw_User { get; set; }
        public virtual DbSet<VisitsCallRate> VisitsCallRates { get; set; }
        public virtual DbSet<VisitsCoverage> VisitsCoverages { get; set; }
        public virtual DbSet<vw_Potential> vw_Potential { get; set; }
        public virtual DbSet<vw_ProductPresentation> vw_ProductPresentation { get; set; }
        public virtual DbSet<vw_ProfileTargetVisit> vw_ProfileTargetVisit { get; set; }
        public virtual DbSet<vw_ProfileTargetVisit_PotentialSummary> vw_ProfileTargetVisit_PotentialSummary { get; set; }
        public virtual DbSet<vw_ProfileTargetVisitDetails> vw_ProfileTargetVisitDetails { get; set; }
        public virtual DbSet<vw_RepCovarageTypeCategoryAnalyzer> vw_RepCovarageTypeCategoryAnalyzer { get; set; }
        public virtual DbSet<vw_Specialty> vw_Specialty { get; set; }
        public virtual DbSet<vw_VisitCommentType> vw_VisitCommentType { get; set; }
        public virtual DbSet<vw_VisitRequestType> vw_VisitRequestType { get; set; }
        public virtual DbSet<vw_Visits> vw_Visits { get; set; }
        public virtual DbSet<vw_VisitsList> vw_VisitsList { get; set; }
        public virtual DbSet<vw_VisitsPerDate> vw_VisitsPerDate { get; set; }
        public virtual DbSet<vw_Employee> vw_Employee { get; set; }
        public virtual DbSet<vw_EmployeeHistory> vw_EmployeeHistory { get; set; }
        public virtual DbSet<vw_EmployeeAchievement> vw_EmployeeAchievement { get; set; }
        public virtual DbSet<vw_EmployeeStructurewProfile1> vw_EmployeeStructurewProfile1 { get; set; }
        public virtual DbSet<vw_CeilingAccount> vw_CeilingAccount { get; set; }
        public virtual DbSet<vw_CeilingDosageAccount> vw_CeilingDosageAccount { get; set; }
        public virtual DbSet<vw_Distributor> vw_Distributor { get; set; }
        public virtual DbSet<vw_DistributorAccountMapping> vw_DistributorAccountMapping { get; set; }
        public virtual DbSet<vw_DistributorBranch> vw_DistributorBranch { get; set; }
        public virtual DbSet<vw_DistributorProductMapping> vw_DistributorProductMapping { get; set; }
        public virtual DbSet<vw_EmployeeAccount> vw_EmployeeAccount { get; set; }
        public virtual DbSet<vw_EmployeeTerritory> vw_EmployeeTerritory { get; set; }
        public virtual DbSet<vw_ImportedFile> vw_ImportedFile { get; set; }
        public virtual DbSet<vw_ImportedFile_AccountsToCreate> vw_ImportedFile_AccountsToCreate { get; set; }
        public virtual DbSet<vw_ImportedFile_UnmappedAccount> vw_ImportedFile_UnmappedAccount { get; set; }
        public virtual DbSet<vw_ImportedFile_UnmappedProduct> vw_ImportedFile_UnmappedProduct { get; set; }
        public virtual DbSet<vw_MarketShare> vw_MarketShare { get; set; }
        public virtual DbSet<vw_MarketShareTerritory> vw_MarketShareTerritory { get; set; }
        public virtual DbSet<vw_ProductPrice> vw_ProductPrice { get; set; }
        public virtual DbSet<vw_ProfileAccount> vw_ProfileAccount { get; set; }
        public virtual DbSet<vw_ProfileAccountValidation> vw_ProfileAccountValidation { get; set; }
        public virtual DbSet<vw_ProfileTerritory> vw_ProfileTerritory { get; set; }
        public virtual DbSet<vw_target_ProductPrice> vw_target_ProductPrice { get; set; }
        public virtual DbSet<vw_targetPhasing_Details> vw_targetPhasing_Details { get; set; }
        public virtual DbSet<vw_TargetProduct> vw_TargetProduct { get; set; }
        public virtual DbSet<vw_TargetProduct_Details> vw_TargetProduct_Details { get; set; }
        public virtual DbSet<vw_TargetProductPhasing> vw_TargetProductPhasing { get; set; }
        public virtual DbSet<vw_TargetProfile> vw_TargetProfile { get; set; }
        public virtual DbSet<vw_TargetProfile_Details> vw_TargetProfile_Details { get; set; }
        public virtual DbSet<vw_TeamSalesProduct> vw_TeamSalesProduct { get; set; }
        public virtual DbSet<vw_Account> vw_Account { get; set; }
        public virtual DbSet<vw_AccountCategory> vw_AccountCategory { get; set; }
        public virtual DbSet<vw_AccountType> vw_AccountType { get; set; }
        public virtual DbSet<vw_Country> vw_Country { get; set; }
        public virtual DbSet<vw_DosageForm> vw_DosageForm { get; set; }
        public virtual DbSet<vw_DosageFormUnit> vw_DosageFormUnit { get; set; }
        public virtual DbSet<vw_EmployeeProfile> vw_EmployeeProfile { get; set; }
        public virtual DbSet<vw_Product> vw_Product { get; set; }
        public virtual DbSet<vw_ProductGroup> vw_ProductGroup { get; set; }
        public virtual DbSet<vw_Team> vw_Team { get; set; }
        public virtual DbSet<vw_TeamProfile> vw_TeamProfile { get; set; }
        public virtual DbSet<vw_Territory> vw_Territory { get; set; }
        public virtual DbSet<vw_TimeDefinition> vw_TimeDefinition { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ScheduledJob>()
                .Property(e => e.ScheduleType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ScheduledJob>()
                .Property(e => e.LastRunSpan)
                .HasPrecision(18, 6);

            modelBuilder.Entity<Setting>()
                .Property(e => e.SettingType)
                .IsUnicode(false);

            modelBuilder.Entity<SystemMenu>()
                .HasMany(e => e.SystemMenu1)
                .WithOptional(e => e.SystemMenu2)
                .HasForeignKey(e => e.ParentMenuId);

            modelBuilder.Entity<SystemMenu>()
                .HasMany(e => e.SystemRoleMenus)
                .WithRequired(e => e.SystemMenu)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SystemRole>()
                .HasMany(e => e.Employees)
                .WithOptional(e => e.SystemRole)
                .HasForeignKey(e => e.RoleId);

            modelBuilder.Entity<SystemRole>()
                .HasMany(e => e.SystemRoleMenus)
                .WithRequired(e => e.SystemRole)
                .HasForeignKey(e => e.RoleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Doctor>()
                .Property(e => e.Gender)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Doctor>()
                .HasMany(e => e.AccountDoctors)
                .WithRequired(e => e.Doctor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Doctor>()
                .HasMany(e => e.VisitDoctors)
                .WithRequired(e => e.Doctor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Potential>()
                .HasMany(e => e.Doctors)
                .WithRequired(e => e.Potential)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Potential>()
                .HasMany(e => e.ProfileTargetNewDoctorApprovals)
                .WithOptional(e => e.Potential)
                .HasForeignKey(e => e.AccountPotentialId);

            modelBuilder.Entity<Potential>()
                .HasMany(e => e.ProfileTargetNewDoctorApprovals1)
                .WithRequired(e => e.Potential1)
                .HasForeignKey(e => e.PotentialId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Potential>()
                .HasMany(e => e.ProfileTargetVisitDetails)
                .WithRequired(e => e.Potential)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductPresentation>()
                .HasMany(e => e.VisitPresentations)
                .WithRequired(e => e.ProductPresentation)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProfileTargetNewDoctorApproval>()
                .Property(e => e.Gender)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ProfileTargetNewDoctorApproval>()
                .Property(e => e.Address)
                .IsFixedLength();

            modelBuilder.Entity<ProfileTargetNewDoctorApproval>()
                .Property(e => e.Latitude)
                .HasPrecision(18, 10);

            modelBuilder.Entity<ProfileTargetNewDoctorApproval>()
                .Property(e => e.Longitude)
                .HasPrecision(18, 10);

            modelBuilder.Entity<ProfileTargetNewDoctorApproval>()
                .Property(e => e.ApprovalStatus)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ProfileTargetVisit>()
                .Property(e => e.ManagerResponse)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ProfileTargetVisit>()
                .HasMany(e => e.ProfileTargetNewDoctorApprovals)
                .WithRequired(e => e.ProfileTargetVisit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProfileTargetVisit>()
                .HasMany(e => e.ProfileTargetVisitDetails)
                .WithRequired(e => e.ProfileTargetVisit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Specialty>()
                .HasMany(e => e.Doctors)
                .WithRequired(e => e.Specialty)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Specialty>()
                .HasMany(e => e.ProductSpecialties)
                .WithRequired(e => e.Specialty)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Visit>()
                .Property(e => e.Latitude)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Visit>()
                .Property(e => e.Longitude)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Visit>()
                .HasMany(e => e.VisitCoaches)
                .WithRequired(e => e.Visit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Visit>()
                .HasMany(e => e.VisitComments)
                .WithRequired(e => e.Visit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Visit>()
                .HasMany(e => e.VisitDoctors)
                .WithRequired(e => e.Visit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Visit>()
                .HasMany(e => e.VisitPresentations)
                .WithRequired(e => e.Visit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Visit>()
                .HasMany(e => e.VisitProducts)
                .WithRequired(e => e.Visit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Visit>()
                .HasMany(e => e.VisitRequests)
                .WithRequired(e => e.Visit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VisitCommentType>()
                .HasMany(e => e.VisitComments)
                .WithRequired(e => e.VisitCommentType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VisitRequestType>()
                .HasMany(e => e.VisitRequests)
                .WithRequired(e => e.VisitRequestType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Department>()
                .HasMany(e => e.Employees)
                .WithOptional(e => e.Department)
                .HasForeignKey(e => e.DepartmentId);

            modelBuilder.Entity<Department>()
                .HasMany(e => e.EmployeeHistories)
                .WithRequired(e => e.Department)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.MaritalStatus)
                .IsFixedLength();

            modelBuilder.Entity<Employee>()
                .Property(e => e.Gender)
                .IsFixedLength();

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Settings)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Settings1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.SystemMenus)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.SystemMenus1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.SystemRoles)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.SystemRoles1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.SystemRoleMenus)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.SystemRoleMenus1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.AccountDoctors)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.AccountDoctors1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Doctors)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Doctors1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Potentials)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Potentials1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProductPresentations)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProductPresentations1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProductSpecialties)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProductSpecialties1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProfileAccounts)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProfileAccounts1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProfileTargetNewDoctorApprovals)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CategoryId);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProfileTargetNewDoctorApprovals1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProfileTargetVisits)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProfileTargetVisits1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.ManagerId);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProfileTargetVisits2)
                .WithOptional(e => e.Employee2)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProfileTargetVisitDetails)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProfileTargetVisitDetails1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProfileTerritories)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProfileTerritories1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Specialties)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Specialties1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.TeamProducts)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.TeamProducts1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Visits)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Visits1)
                .WithRequired(e => e.Employee1)
                .HasForeignKey(e => e.EmployeeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Visits2)
                .WithOptional(e => e.Employee2)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Visits3)
                .WithOptional(e => e.Employee3)
                .HasForeignKey(e => e.MarkedFalseById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Visits4)
                .WithOptional(e => e.Employee4)
                .HasForeignKey(e => e.ReviewedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitCoaches)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitCoaches1)
                .WithRequired(e => e.Employee1)
                .HasForeignKey(e => e.CoachId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitCoaches2)
                .WithOptional(e => e.Employee2)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitComments)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitComments1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitCommentTypes)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitCommentTypes1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitDoctors)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitDoctors1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitPresentations)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitPresentations1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitProducts)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitProducts1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitRequests)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitRequests1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.FullfilledById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitRequests2)
                .WithOptional(e => e.Employee2)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitRequestTypes)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VisitRequestTypes1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Departments)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Departments1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.AccountCategories)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.AccountCategories1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.AccountTypes)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.AccountTypes1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.CeilingAccountExceptions)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.CeilingAccountExceptions1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.CeilingDosageAccounts)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.CeilingDosageAccounts1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Countries)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Countries1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Distributors)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Distributors1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.DistributorAccountMappings)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.DistributorAccountMappings1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.DistributorBranches)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.DistributorBranches1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.DistributorProductMappings)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.DistributorProductMappings1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.DosageFormUnits)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.DosageFormUnits1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Employee1)
                .WithOptional(e => e.Employee2)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Employee11)
                .WithOptional(e => e.Employee3)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.EmployeeHistories)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.EmployeeHistories1)
                .WithRequired(e => e.Employee1)
                .HasForeignKey(e => e.EmployeeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.EmployeeHistories2)
                .WithOptional(e => e.Employee2)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.EmployeeHistories3)
                .WithOptional(e => e.Employee3)
                .HasForeignKey(e => e.ManagerId);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.EmployeeProfiles)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.EmployeeProfiles1)
                .WithRequired(e => e.Employee1)
                .HasForeignKey(e => e.EmployeeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.EmployeeProfiles2)
                .WithOptional(e => e.Employee2)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.EmployeeVacations)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.EmployeeVacations1)
                .WithRequired(e => e.Employee1)
                .HasForeignKey(e => e.EmployeeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.EmployeeVacations2)
                .WithOptional(e => e.Employee2)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.EmployeeVacations3)
                .WithOptional(e => e.Employee3)
                .HasForeignKey(e => e.ManagerId);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ImportedFiles)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ImportedFiles1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ImportedFileRecords)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ImportedFileRecords1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ImportedFileStatus)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ImportedFileStatus1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ImportPlugins)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ImportPlugins1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.InMarketSales)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.InMarketSales1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.JobTitles)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.JobTitles1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.MarketShares)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.MarketShares1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.MarketShareTerritories)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.MarketShareTerritories1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Products)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Products1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProductDosageForms)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProductDosageForms1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProductGroups)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProductGroups1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProductPrices)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProductPrices1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProfileAccount1)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProfileAccount11)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProfileTerritory1)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ProfileTerritory11)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.StoreAccounts)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.StoreAccounts1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Targets)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Targets1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.EmployeeId);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Targets2)
                .WithOptional(e => e.Employee2)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.TargetTypes)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.TargetTypes1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Teams)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Teams1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.TeamSalesProducts)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.TeamProfiles)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.TeamProfiles1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.TeamSalesProducts1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Territories)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Territories1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.TimeDefinitions)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.TimeDefinitions1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VacationTypes)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.VacationTypes1)
                .WithOptional(e => e.Employee1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<EmployeeVacation>()
                .Property(e => e.NumberOfDays)
                .HasPrecision(6, 2);

            modelBuilder.Entity<EmployeeVacation>()
                .Property(e => e.ManagerResponse)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<JobTitle>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.JobTitle)
                .HasForeignKey(e => e.JobTitleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobTitle>()
                .HasMany(e => e.EmployeeHistories)
                .WithRequired(e => e.JobTitle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VacationType>()
                .HasMany(e => e.EmployeeVacations)
                .WithRequired(e => e.VacationType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CeilingDosageAccount>()
                .Property(e => e.CeilingQty)
                .HasPrecision(18, 6);

            modelBuilder.Entity<Distributor>()
                .HasMany(e => e.DistributorAccountMappings)
                .WithRequired(e => e.Distributor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Distributor>()
                .HasMany(e => e.DistributorBranches)
                .WithRequired(e => e.Distributor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Distributor>()
                .HasMany(e => e.DistributorProductMappings)
                .WithRequired(e => e.Distributor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Distributor>()
                .HasMany(e => e.ImportedFiles)
                .WithRequired(e => e.Distributor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Distributor>()
                .HasMany(e => e.InMarketSales)
                .WithRequired(e => e.Distributor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DistributorBranch>()
                .HasMany(e => e.ImportedFileRecords)
                .WithOptional(e => e.DistributorBranch)
                .HasForeignKey(e => e.DistributorBranchId);

            modelBuilder.Entity<DistributorBranch>()
                .HasMany(e => e.InMarketSales)
                .WithOptional(e => e.DistributorBranch)
                .HasForeignKey(e => e.DistributorBranchId);

            modelBuilder.Entity<ImportedFile>()
                .Property(e => e.ImportMode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ImportedFile>()
                .HasMany(e => e.ImportedFileRecords)
                .WithRequired(e => e.ImportedFile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ImportedFileRecord>()
                .Property(e => e.Quantity)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ImportedFileRecord>()
                .Property(e => e.Amount)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ImportedFileStatu>()
                .HasMany(e => e.ImportedFiles)
                .WithRequired(e => e.ImportedFileStatu)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ImportPlugin>()
                .HasMany(e => e.Distributors)
                .WithOptional(e => e.ImportPlugin)
                .HasForeignKey(e => e.SalesPluginId);

            modelBuilder.Entity<ImportPlugin>()
                .HasMany(e => e.ImportedFiles)
                .WithRequired(e => e.ImportPlugin)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InMarketSale>()
                .Property(e => e.Qty)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InMarketSale>()
                .Property(e => e.Amount)
                .HasPrecision(18, 4);

            modelBuilder.Entity<MarketShare>()
                .HasMany(e => e.MarketShareTerritories)
                .WithRequired(e => e.MarketShare1)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MarketShareTerritory>()
                .Property(e => e.MarketShare)
                .HasPrecision(8, 5);

            modelBuilder.Entity<ProductPrice>()
                .Property(e => e.Price)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ProfileAccount1>()
                .Property(e => e.Perccentage)
                .HasPrecision(18, 6);

            modelBuilder.Entity<ProfileTerritory1>()
                .Property(e => e.Percentage)
                .HasPrecision(18, 6);

            modelBuilder.Entity<Target>()
                .Property(e => e.Qty)
                .HasPrecision(18, 4);

            modelBuilder.Entity<Target>()
                .Property(e => e.Amount)
                .HasPrecision(18, 4);

            modelBuilder.Entity<Target>()
                .Property(e => e.Percentage)
                .HasPrecision(18, 6);

            modelBuilder.Entity<TargetType>()
                .HasMany(e => e.Targets)
                .WithRequired(e => e.TargetType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .Property(e => e.Latitude)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Account>()
                .Property(e => e.Longitude)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.AccountDoctors)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.ProfileAccounts)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.ProfileTargetVisitDetails)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Visits)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.CeilingAccountExceptions)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.DistributorAccountMappings)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.ProfileAccount1)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.StoreAccounts)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AccountCategory>()
                .HasMany(e => e.ProductPrices)
                .WithOptional(e => e.AccountCategory)
                .HasForeignKey(e => e.AccountCategoryId);

            modelBuilder.Entity<AccountCategory>()
                .HasMany(e => e.AccountCategory1)
                .WithOptional(e => e.AccountCategory2)
                .HasForeignKey(e => e.ParentCategoryId);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Doctors)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.ProfileTargetVisits)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Visits)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Employees)
                .WithOptional(e => e.Country)
                .HasForeignKey(e => e.CountryId);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Distributors)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.ImportedFiles)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.InMarketSales)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.MarketShares)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Targets)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Teams)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Territories)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.ProductPresentations)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.ProductSpecialties)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.TeamProducts)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.VisitProducts)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.ProductPrices)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.TeamSalesProducts)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.Product1)
                .WithOptional(e => e.Product2)
                .HasForeignKey(e => e.ParentProductId);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.ProductDosageForms)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductDosageForm>()
                .Property(e => e.PackFactor)
                .HasPrecision(18, 6);

            modelBuilder.Entity<ProductDosageForm>()
                .HasMany(e => e.CeilingDosageAccounts)
                .WithRequired(e => e.ProductDosageForm)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductDosageForm>()
                .HasMany(e => e.DistributorProductMappings)
                .WithRequired(e => e.ProductDosageForm)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductDosageForm>()
                .HasMany(e => e.InMarketSales)
                .WithRequired(e => e.ProductDosageForm)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductDosageForm>()
                .HasMany(e => e.ProfileAccount1)
                .WithOptional(e => e.ProductDosageForm)
                .HasForeignKey(e => e.DosageFromId);

            modelBuilder.Entity<ProductGroup>()
                .HasMany(e => e.ProductGroup1)
                .WithOptional(e => e.ProductGroup2)
                .HasForeignKey(e => e.ParentGroupId);

            modelBuilder.Entity<Team>()
                .HasMany(e => e.TeamProducts)
                .WithRequired(e => e.Team)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Team>()
                .HasMany(e => e.TeamSalesProducts)
                .WithRequired(e => e.Team)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Team>()
                .HasMany(e => e.TeamProfiles)
                .WithRequired(e => e.Team)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TeamProfile>()
                .Property(e => e.AvgCallRate)
                .HasPrecision(18, 6);

            modelBuilder.Entity<TeamProfile>()
                .HasMany(e => e.ProfileAccounts)
                .WithRequired(e => e.TeamProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TeamProfile>()
                .HasMany(e => e.ProfileTargetVisits)
                .WithRequired(e => e.TeamProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TeamProfile>()
                .HasMany(e => e.ProfileTerritories)
                .WithRequired(e => e.TeamProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TeamProfile>()
                .HasMany(e => e.ProfileAccount1)
                .WithRequired(e => e.TeamProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TeamProfile>()
                .HasMany(e => e.ProfileTerritory1)
                .WithRequired(e => e.TeamProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TeamProfile>()
                .HasMany(e => e.EmployeeProfiles)
                .WithRequired(e => e.TeamProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Territory>()
                .HasMany(e => e.ProfileTerritories)
                .WithRequired(e => e.Territory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Territory>()
                .HasMany(e => e.MarketShareTerritories)
                .WithRequired(e => e.Territory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Territory>()
                .HasMany(e => e.ProfileTerritory1)
                .WithRequired(e => e.Territory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Territory>()
                .HasMany(e => e.Territory1)
                .WithOptional(e => e.Territory2)
                .HasForeignKey(e => e.ParentTerritoryId);

            modelBuilder.Entity<TimeDefinition>()
                .HasMany(e => e.ProfileTargetVisits)
                .WithRequired(e => e.TimeDefinition)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TimeDefinition>()
                .HasMany(e => e.CeilingAccountExceptions)
                .WithRequired(e => e.TimeDefinition)
                .HasForeignKey(e => e.FromTimeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TimeDefinition>()
                .HasMany(e => e.CeilingAccountExceptions1)
                .WithOptional(e => e.TimeDefinition1)
                .HasForeignKey(e => e.ToTimeId);

            modelBuilder.Entity<TimeDefinition>()
                .HasMany(e => e.CeilingDosageAccounts)
                .WithRequired(e => e.TimeDefinition)
                .HasForeignKey(e => e.FromTimeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TimeDefinition>()
                .HasMany(e => e.CeilingDosageAccounts1)
                .WithOptional(e => e.TimeDefinition1)
                .HasForeignKey(e => e.ToTimeId);

            modelBuilder.Entity<TimeDefinition>()
                .HasMany(e => e.StoreAccounts)
                .WithRequired(e => e.TimeDefinition)
                .HasForeignKey(e => e.FromTimeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TimeDefinition>()
                .HasMany(e => e.StoreAccounts1)
                .WithOptional(e => e.TimeDefinition1)
                .HasForeignKey(e => e.ToTimeId);

            modelBuilder.Entity<InMarketSalesByEmployeeByProfile>()
                .Property(e => e.Perccentage)
                .HasPrecision(18, 6);

            modelBuilder.Entity<InMarketSalesByEmployeeByProfile>()
                .Property(e => e.Qty)
                .HasPrecision(37, 10);

            modelBuilder.Entity<InMarketSalesByEmployeeByProfile>()
                .Property(e => e.FileAmount)
                .HasPrecision(18, 4);

            modelBuilder.Entity<DistributorAccountDosagePercentage>()
                .Property(e => e.DistributorSalesPercentage)
                .HasPrecision(38, 6);

            modelBuilder.Entity<InMarketSalesByDistributorIncludingCeiledQty>()
                .Property(e => e.Qty)
                .HasPrecision(38, 6);

            modelBuilder.Entity<InMarketSalesByProfileAccount>()
                .Property(e => e.SharePercentage)
                .HasPrecision(18, 6);

            modelBuilder.Entity<InMarketSalesByProfileAccount>()
                .Property(e => e.Qty)
                .HasPrecision(38, 10);

            modelBuilder.Entity<InMarketSalesByProfileAccount>()
                .Property(e => e.Amount)
                .HasPrecision(38, 10);

            modelBuilder.Entity<InMarketSalesByProfileAccount>()
                .Property(e => e.ShareType)
                .IsUnicode(false);

            modelBuilder.Entity<InMarketSalesByProfileAccount_CeilingProduct>()
                .Property(e => e.Qty)
                .HasPrecision(38, 6);

            modelBuilder.Entity<InMarketSalesByProfileAccount_CeilingProduct>()
                .Property(e => e.RemainingQty)
                .HasPrecision(38, 6);

            modelBuilder.Entity<InMarketSalesFact>()
                .Property(e => e.SharePercentage)
                .HasPrecision(18, 6);

            modelBuilder.Entity<InMarketSalesFact>()
                .Property(e => e.Qty)
                .HasPrecision(38, 6);

            modelBuilder.Entity<InMarketSalesFact>()
                .Property(e => e.Amount)
                .HasPrecision(18, 6);

            modelBuilder.Entity<InMarketSalesIncludingCeilingQty>()
                .Property(e => e.Qty)
                .HasPrecision(38, 4);

            modelBuilder.Entity<InMarketSalesIncludingCeilingQty>()
                .Property(e => e.RemainingQty)
                .HasPrecision(38, 4);

            modelBuilder.Entity<EmployeeProfileAccountProductShare>()
                .Property(e => e.Perccentage)
                .HasPrecision(18, 6);

            modelBuilder.Entity<EmployeeProfileTerritoryProductShare>()
                .Property(e => e.Percentage)
                .HasPrecision(18, 6);

            modelBuilder.Entity<profileaccount2>()
                .Property(e => e.Perccentage)
                .HasPrecision(38, 6);

            modelBuilder.Entity<profileaccount2>()
                .Property(e => e.ProfileAccountType)
                .IsUnicode(false);

            modelBuilder.Entity<ProfileAccountProductShare>()
                .Property(e => e.Perccentage)
                .HasPrecision(38, 6);

            modelBuilder.Entity<ProfileAccountProductShare>()
                .Property(e => e.PackFactor)
                .HasPrecision(18, 6);

            modelBuilder.Entity<ProfileSales_For_Store>()
                .Property(e => e.Qty)
                .HasPrecision(38, 10);

            modelBuilder.Entity<Target_AvgPercentage>()
                .Property(e => e.SalesPecentage)
                .HasPrecision(38, 6);

            modelBuilder.Entity<Target_AvgPercentage>()
                .Property(e => e.TargetPecentage)
                .HasPrecision(38, 6);

            modelBuilder.Entity<Target_AvgPercentage>()
                .Property(e => e.AvgPercentage)
                .HasPrecision(38, 6);

            modelBuilder.Entity<Target_InMarketSales>()
                .Property(e => e.Qty)
                .HasPrecision(38, 6);

            modelBuilder.Entity<Target_InMarketSales>()
                .Property(e => e.Amount)
                .HasPrecision(38, 6);

            modelBuilder.Entity<Target_InMarketSales>()
                .Property(e => e.Perccentage)
                .HasPrecision(38, 6);

            modelBuilder.Entity<Target_InMarketSales>()
                .Property(e => e.SalesType)
                .IsUnicode(false);

            modelBuilder.Entity<Target_Sheet>()
                .Property(e => e.price)
                .HasPrecision(18, 6);

            modelBuilder.Entity<Target_Sheet>()
                .Property(e => e.Qty_Lastyear2)
                .HasPrecision(18, 6);

            modelBuilder.Entity<Target_Sheet>()
                .Property(e => e.Amount_Lastyear2)
                .HasPrecision(18, 6);

            modelBuilder.Entity<Target_Sheet>()
                .Property(e => e.Qty_Lastyear1)
                .HasPrecision(18, 6);

            modelBuilder.Entity<Target_Sheet>()
                .Property(e => e.Amount_Lastyear1)
                .HasPrecision(18, 6);

            modelBuilder.Entity<Target_Sheet>()
                .Property(e => e.Qty_Currentyear)
                .HasPrecision(18, 6);

            modelBuilder.Entity<Target_Sheet>()
                .Property(e => e.Amount_Currentyear)
                .HasPrecision(18, 6);

            modelBuilder.Entity<Target_Sheet>()
                .Property(e => e.Qty_Currentyear_estimate)
                .HasPrecision(18, 6);

            modelBuilder.Entity<Target_Sheet>()
                .Property(e => e.Amount_Currentyear_estimate)
                .HasPrecision(18, 6);

            modelBuilder.Entity<Target_Sheet>()
                .Property(e => e.Sales_Office)
                .HasPrecision(18, 6);

            modelBuilder.Entity<Target_Sheet>()
                .Property(e => e.MarketShare)
                .HasPrecision(18, 6);

            modelBuilder.Entity<Target_Sheet>()
                .Property(e => e.Line_Sales)
                .HasPrecision(18, 6);

            modelBuilder.Entity<Target_Sheet>()
                .Property(e => e.Product_Sales)
                .HasPrecision(18, 6);

            modelBuilder.Entity<VisitsCallRate>()
                .Property(e => e.AccountsCallRate)
                .HasPrecision(38, 20);

            modelBuilder.Entity<VisitsCoverage>()
                .Property(e => e.AccountsCoverage)
                .HasPrecision(38, 20);

            modelBuilder.Entity<vw_ProfileTargetVisit>()
                .Property(e => e.TargetStatus)
                .IsUnicode(false);

            modelBuilder.Entity<vw_ProfileTargetVisit>()
                .Property(e => e.EstimatedCallRate)
                .HasPrecision(1, 1);

            modelBuilder.Entity<vw_ProfileTargetVisit>()
                .Property(e => e.AvgCallRate)
                .HasPrecision(18, 6);

            modelBuilder.Entity<vw_Visits>()
                .Property(e => e.Latitude)
                .HasPrecision(18, 10);

            modelBuilder.Entity<vw_Visits>()
                .Property(e => e.Longitude)
                .HasPrecision(18, 10);

            modelBuilder.Entity<vw_VisitsList>()
                .Property(e => e.Longitude)
                .HasPrecision(18, 10);

            modelBuilder.Entity<vw_VisitsList>()
                .Property(e => e.Latitude)
                .HasPrecision(18, 10);

            modelBuilder.Entity<vw_EmployeeAchievement>()
                .Property(e => e.SalesQty)
                .HasPrecision(38, 10);

            modelBuilder.Entity<vw_EmployeeAchievement>()
                .Property(e => e.SalesAmount)
                .HasPrecision(38, 10);

            modelBuilder.Entity<vw_EmployeeAchievement>()
                .Property(e => e.TargetQty)
                .HasPrecision(38, 4);

            modelBuilder.Entity<vw_EmployeeAchievement>()
                .Property(e => e.TargetAmount)
                .HasPrecision(38, 4);

            modelBuilder.Entity<vw_CeilingDosageAccount>()
                .Property(e => e.CeilingQty)
                .HasPrecision(18, 6);

            modelBuilder.Entity<vw_ImportedFile>()
                .Property(e => e.ImportMode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_MarketShareTerritory>()
                .Property(e => e.MarketShare)
                .HasPrecision(8, 5);

            modelBuilder.Entity<vw_ProductPrice>()
                .Property(e => e.PriceType)
                .IsUnicode(false);

            modelBuilder.Entity<vw_ProductPrice>()
                .Property(e => e.Price)
                .HasPrecision(18, 4);

            modelBuilder.Entity<vw_ProfileAccount>()
                .Property(e => e.Perccentage)
                .HasPrecision(18, 6);

            modelBuilder.Entity<vw_ProfileAccountValidation>()
                .Property(e => e.Perccentage)
                .HasPrecision(18, 6);

            modelBuilder.Entity<vw_ProfileTerritory>()
                .Property(e => e.Percentage)
                .HasPrecision(18, 6);

            modelBuilder.Entity<vw_target_ProductPrice>()
                .Property(e => e.Price)
                .HasPrecision(18, 4);

            modelBuilder.Entity<vw_targetPhasing_Details>()
                .Property(e => e.Percentage)
                .HasPrecision(18, 6);

            modelBuilder.Entity<vw_targetPhasing_Details>()
                .Property(e => e.PercentageGrowth)
                .HasPrecision(38, 20);

            modelBuilder.Entity<vw_TargetProduct>()
                .Property(e => e.Qty)
                .HasPrecision(38, 4);

            modelBuilder.Entity<vw_TargetProduct>()
                .Property(e => e.Amount)
                .HasPrecision(38, 4);

            modelBuilder.Entity<vw_TargetProduct>()
                .Property(e => e.QtyGrowth)
                .HasPrecision(38, 6);

            modelBuilder.Entity<vw_TargetProduct>()
                .Property(e => e.AmountGrowth)
                .HasPrecision(38, 6);

            modelBuilder.Entity<vw_TargetProduct_Details>()
                .Property(e => e.Qty)
                .HasPrecision(18, 4);

            modelBuilder.Entity<vw_TargetProduct_Details>()
                .Property(e => e.Amount)
                .HasPrecision(18, 4);

            modelBuilder.Entity<vw_TargetProduct_Details>()
                .Property(e => e.QtyGrowth)
                .HasPrecision(38, 20);

            modelBuilder.Entity<vw_TargetProduct_Details>()
                .Property(e => e.AmountGrowth)
                .HasPrecision(38, 20);

            modelBuilder.Entity<vw_TargetProfile>()
                .Property(e => e.Qty)
                .HasPrecision(38, 4);

            modelBuilder.Entity<vw_TargetProfile>()
                .Property(e => e.Amount)
                .HasPrecision(38, 4);

            modelBuilder.Entity<vw_TargetProfile_Details>()
                .Property(e => e.Qty)
                .HasPrecision(18, 4);

            modelBuilder.Entity<vw_TargetProfile_Details>()
                .Property(e => e.Amount)
                .HasPrecision(18, 4);

            modelBuilder.Entity<vw_DosageForm>()
                .Property(e => e.PackFactor)
                .HasPrecision(18, 6);
        }
    }
}
